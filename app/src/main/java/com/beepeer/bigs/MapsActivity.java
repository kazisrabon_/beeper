/*
 * Copyright (c) to srabon
 * IIT, University of Dhaka
 * kaziiit@gmail.com
 */

package com.beepeer.bigs;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.app.UiModeManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beepeer.bigs.adapter.ContactAdapter;
import com.beepeer.bigs.adapter.MapChatItemAdapter;
import com.beepeer.bigs.model.Beep;
import com.beepeer.bigs.model.Contact;
import com.beepeer.bigs.model.MapChat;
import com.beepeer.bigs.model.User;
import com.beepeer.bigs.todo.ToDoItemAdapter;
import com.facebook.FacebookSdk;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.AppInviteDialog;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;
import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.MobileServiceList;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.notifications.NotificationsManager;
import com.mikepenz.iconics.typeface.FontAwesome;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeader;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.Nameable;
import com.mikepenz.materialdrawer.util.KeyboardUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;

import static com.microsoft.windowsazure.mobileservices.table.query.QueryOperations.val;

public class MapsActivity extends ActionBarActivity implements View.OnClickListener {

    public static boolean login;
    public static MobileServiceTable<MapChat> mMapChatTable;
    public static Beep item;
    public static List<MapChat> mapChats = new ArrayList<MapChat>();
    public static android.support.v4.app.FragmentManager fragmentManager;
    Bitmap myBitmap;
    ProgressBar mProgressBar;
    SharedPreferences sharedpreferences, latlongSF;
    private String sharedData, msg, name, phone, id, color, sosNumber, previewImageUrl, appLinkUrl, sos = "tel:+8801684397844";
    ImageButton greenIB, yellowIB, redIB;
    public static MobileServiceClient mClient;
    private MobileServiceTable<Beep> mBeepTable;
    private double latitude, longitude;
    private Drawer.Result result;
    private AccountHeader.Result headerResult;
    private long delaytime = 1000 * 30;
    public static RelativeLayout relativeLayout;
    private Dialog dialog;
    EditText editedAge;
    Button btnOK;
    MapChat mapChat = new MapChat();
    public static String cityName = "";
    public static String stateName = "";
    public static String countryName = "";
    public static String beepeerID;
    private Dialog dialogContact;
    private ListView listViewContacts;
    private ImageButton btnSOS, btnShare, btnCamera, btnMSG, btnChat;
    ContactAdapter contactAdapter;
    Contact contact;
    List<String> phones = new ArrayList<String>();
    List<String> names = new ArrayList<String>();
    private List<Contact> contacts = new ArrayList<Contact>();
    private Context context;
    //    321468801697
    public static final String SENDER_ID = "101219809405";
    private ToDoItemAdapter mAdapter;
    public static Toolbar toolbar;
    private Intent loginIntent;
    public static LinearLayout bottomLayout;
    private boolean isOdd;
    private EditText mTextNewToDo;
    public static boolean isFnF;
    private List<String> putReceiver;
    private ProgressDialog progressDialogFNF;
    private User user;
    private SharedPreferences values;
    public static Bundle bundle;
    public static String beepeerName;
    private RelativeLayout outterRelativeLayout, innerRelativeLayout;
    private View normalView;
    private LinearLayout linearLayoutTitle;
    private TextView textViewTitle;
    private ImageButton buttonInvitation;
    private Bitmap bitmap;
    private ShareDialog shareDialog;
    private SharedPreferences picSF;
    private Contact selectedContact;
    private ProgressDialog progressDialog;
    public static List<Beep> refreshResult;
    public static MobileServiceTable<User> mUserTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        bundle = savedInstanceState;
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+1:00"));
        Date currentLocalTime = cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("dd:MM:yyyy HH:mm:ss");
        Date lastLoginDate = cal.getTime();
        Log.e("date", lastLoginDate.getTime()+"");
//        Date dt2 =
        int hours = currentLocalTime.getHours();
        Log.e("hours", hours + "");
        if (hours >= 0 && hours < 8) {
            UiModeManager uiManager = (UiModeManager) getSystemService(Context.UI_MODE_SERVICE);
            uiManager.setNightMode(UiModeManager.MODE_NIGHT_YES);
        }
        FacebookSdk.sdkInitialize(getApplicationContext());
        isOdd = true;
        loginIntent = new Intent(MapsActivity.this, MainActivity.class);
        Log.e("MapsActivity", "Google: " + MainActivity.isGoogleSignIn);
        Log.e("MapsActivity", "Facebook: " + MainActivity.isFacebookSignIn);
        Log.e("MapsActivity", "System: " + MainActivity.isUserLoggedIn);
        context = MapsActivity.this;
        login = true;
        values = getSharedPreferences("app", Context.MODE_PRIVATE);
        values.edit().putLong("lastLoginDate", lastLoginDate.getTime()).commit();
        color = values.getString("color", "blue");
        sosNumber = values.getString("sos", "01684397844");
        sharedpreferences = getSharedPreferences(getResources().getString(R.string.id), Context.MODE_PRIVATE);
        picSF = getSharedPreferences("pictures", Context.MODE_PRIVATE);
        beepeerID = sharedpreferences.getString(getString(R.string.beepeerID), "1234567");
        beepeerName = sharedpreferences.getString(getString(R.string.beepeerName), "Srabon");
        latlongSF = getSharedPreferences(getResources().getString(R.string.latlon), Context.MODE_PRIVATE);
        sharedData = latlongSF.getString(getResources().getString(R.string.lat), "") + " " + latlongSF.getString(getResources().getString(R.string.lon), "");
        // For making call
        PhoneCallListener phoneListener = new PhoneCallListener();
        TelephonyManager telephonyManager = (TelephonyManager) this
                .getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(phoneListener,
                PhoneStateListener.LISTEN_CALL_STATE);
        fragmentManager = getSupportFragmentManager();

        try {

            mClient =  new MobileServiceClient(
                    "https://beepeer.azure-mobile.net/",
                    "TBpzCbTsUcdYjPiGBdZunJfjRnhEiR93",
                    this);
            mBeepTable = mClient.getTable(Beep.class);
            mUserTable = mClient.getTable(User.class);
            NotificationsManager.handleNotifications(this, SENDER_ID, MyHandler.class);
        } catch (Exception e) {
            //createAndShowDialog(new Exception("There was an error creating the Mobile Service. Verify the URL"), "Error");
        }
        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
        List<Address> addresses = null;
        latitude = Double.parseDouble(latlongSF.getString(getResources().getString(R.string.lat), "23.7245072"));
        longitude = Double.parseDouble(latlongSF.getString(getResources().getString(R.string.lon), "90.4030656"));
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (addresses != null) {
            cityName = addresses.get(0).getAddressLine(0);
            stateName = addresses.get(0).getAddressLine(1);
            countryName = addresses.get(0).getAddressLine(2);
        }

        Log.e("MapsActivity", cityName + " " + stateName + " " + countryName);
        item = new Beep();
        item.setmBeeper(sharedpreferences.getString(getResources().getString(R.string.mail), "1234567"));
        item.setmAreaName(cityName);
        item.setmLatitude(latlongSF.getString(getResources().getString(R.string.lat), "23.7245072"));
        item.setmLongitude(latlongSF.getString(getResources().getString(R.string.lon), "90.4030656"));

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(getResources().getColor(R.color.selected));

        menuBarHide();
        initImageButton();
        initDrawer(savedInstanceState);
//        Log.e("ColorMap", color);
        if (color.equals("red")) {
            toolbar.setBackgroundColor(getResources().getColor(R.color.spring_header));
            bottomLayout.setBackgroundColor(getResources().getColor(R.color.spring_header));
        } else if (color.equals("yellow")) {
            toolbar.setBackgroundColor(getResources().getColor(R.color.summer_header));
            bottomLayout.setBackgroundColor(getResources().getColor(R.color.summer_header));
        } else if (color.equals("blue")) {
            toolbar.setBackgroundColor(getResources().getColor(R.color.autumn_header));
            bottomLayout.setBackgroundColor(getResources().getColor(R.color.autumn_header));
        } else {
            toolbar.setBackgroundColor(getResources().getColor(R.color.winter_header));
            bottomLayout.setBackgroundColor(getResources().getColor(R.color.winter_header));
        }

        new BeepChat().execute();
    }

    private void initImageButton() {
//        btnCamera = (ImageButton)findViewById(R.id.btnCamera);
        btnChat = (ImageButton) findViewById(R.id.btnMChat);
        btnMSG = (ImageButton) findViewById(R.id.btnMessage);
        btnShare = (ImageButton) findViewById(R.id.btnShare);
        btnSOS = (ImageButton) findViewById(R.id.btnCall);

        btnSOS.setOnClickListener(this);
        btnShare.setOnClickListener(this);
        btnMSG.setOnClickListener(this);
        btnChat.setOnClickListener(this);
//        btnCamera.setOnClickListener(this);
    }

    private void initDrawer(Bundle savedInstanceState) {
        String email = sharedpreferences.getString(getString(R.string.mail), "kaziiit@gmail.com");
        String personName = sharedpreferences.getString(getString(R.string.beepeerName), "Srabon");
        String url = MainActivity.url;
        String cover = MainActivity.cover;
        BitmapDrawable bitmapDrawable = null;
        Drawable drawable = null;
        Bitmap bit=null;
        try{
            bit = BitmapFactory.decodeStream((InputStream) new URL(url).getContent());
            drawable = new BitmapDrawable(getResources(), bit);

        } catch (Exception e){}
        Log.e("pictures", url+"/n"+cover);
        if (MainActivity.personName != null && MainActivity.email != null && MainActivity.drawable != null && MainActivity.coverPic != null) {
            headerResult = new AccountHeader()
                    .withActivity(this)
                    .withHeaderBackground(MainActivity.coverPic)
                    .addProfiles(
                            new ProfileDrawerItem().withName(MainActivity.personName)
                                    .withEmail(MainActivity.email)
                                    .withIcon(MainActivity.drawable)
                    )
                    .build();
        }
        else if(!url.equals("") && drawable!=null){
            headerResult = new AccountHeader()
                    .withActivity(this)
                    .withHeaderBackground(drawable)
                    .addProfiles(
                            new ProfileDrawerItem().withName(personName)
                                    .withEmail(email)
                                    .withIcon(url)
                    )
                    .build();
        }
        else if(drawable!=null){
            headerResult = new AccountHeader()
                    .withActivity(this)
                    .withHeaderBackground(drawable)
                    .addProfiles(
                            new ProfileDrawerItem().withName(personName)
                                    .withEmail(email)
                                    .withIcon(getResources().getDrawable(R.drawable.avatar))
                    )
                    .build();
        }
        else if(!url.equals("")){
            headerResult = new AccountHeader()
                    .withActivity(this)
                    .withHeaderBackground(R.drawable.header)
                    .addProfiles(
                            new ProfileDrawerItem().withName(personName)
                                    .withEmail(email)
                                    .withIcon(url)
                    )
                    .build();
        }
        else {
            headerResult = new AccountHeader()
                    .withActivity(this)
                    .withHeaderBackground(R.drawable.header)
                    .addProfiles(
                            new ProfileDrawerItem().withName(personName)
                                    .withEmail(email)
                                    .withIcon(getResources().getDrawable(R.drawable.avatar))
                    )
                    .build();

        }

        //Create the drawer
        result = new Drawer()
                .withActivity(this)
                .withHeader(R.layout.header)
                .withAccountHeader(headerResult)
                .withDisplayBelowToolbar(true)
                .withToolbar(toolbar)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.drawer_item_traffic).withIcon(FontAwesome.Icon.faw_map_marker).withIdentifier(0),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_profile).withIcon(FontAwesome.Icon.faw_user).withIdentifier(1),
//                        new PrimaryDrawerItem().withName(R.string.drawer_item_profile).withIcon(getResources().getDrawable(R.drawable.ic_people)).withSelectedColor(Color.GREEN).withBadge("6").withIdentifier(1),
//                        new PrimaryDrawerItem().withName(R.string.drawer_item_chat).withIcon(FontAwesome.Icon.faw_wechat).withIdentifier(2),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_rating).withIcon(FontAwesome.Icon.faw_star_o).withIdentifier(3),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_club).withIcon(FontAwesome.Icon.faw_trophy).withIdentifier(4),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_aboutus).withIcon(FontAwesome.Icon.faw_book).withIdentifier(5),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_settings).withIcon(FontAwesome.Icon.faw_cog).withIdentifier(6),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_help).withIcon(FontAwesome.Icon.faw_question).withIdentifier(7),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_faq).withIcon(FontAwesome.Icon.faw_question_circle).withIdentifier(8),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_logout).withIcon(FontAwesome.Icon.faw_sign_out).withIdentifier(9)
                )
                .withOnDrawerListener(new Drawer.OnDrawerListener() {
                    @Override
                    public void onDrawerOpened(View drawerView) {
//                        Toast.makeText(MapsActivity.this, "onDrawerOpened", Toast.LENGTH_SHORT).show();
                        KeyboardUtil.hideKeyboard(MapsActivity.this);
                    }

                    @Override
                    public void onDrawerClosed(View drawerView) {
//                        Toast.makeText(MapsActivity.this, "onDrawerClosed", Toast.LENGTH_SHORT).show();
                    }
                })
                        // add the items we want to use with our Drawer
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id, IDrawerItem drawerItem) {

                        if (drawerItem != null && drawerItem instanceof Nameable) {
                            int drawerItemNumber = ((Nameable) drawerItem).getNameRes();
                            Log.e("drawer Number", drawerItemNumber+"");
                            if (((Nameable) drawerItem).getNameRes() == 2131165284) {
                                getSupportActionBar().setTitle("Settings");
                            } else {
                                getSupportActionBar().setTitle("Traffic");
                            }
//                            getSupportActionBar().setTitle(((Nameable) drawerItem).getNameRes());
                            Fragment fragment = null;
                            DialogFragment dialogFragment = null;
                            //ignore the DemoFragment and it's layout it's just to showcase the handle with an keyboard
                            if (drawerItem.getIdentifier() == 0) {
                                fragment = new HomeFragment();
                            } else if (drawerItem.getIdentifier() == 1) {
                                dialogFragment = Profile.newInstance();
                            }
//                            else if (drawerItem.getIdentifier() == 2) {
//                                fragment = new Friends();
//                            }
                            else if (drawerItem.getIdentifier() == 3) {
                                Intent viewIntent =
                                        new Intent("android.intent.action.VIEW",
                                                Uri.parse("https://play.google.com/store/apps/details?id=com.beepeer.bigs&ah=WxDX-Z_sYJuUD58H7O4C4WCaCOM"));
                                startActivity(viewIntent);

                            } else if (drawerItem.getIdentifier() == 4) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(MapsActivity.this);   //never use application context to show dialog
                                LayoutInflater inflater = getLayoutInflater();
                                View layout = inflater.inflate(R.layout.beepeer_club, null);
                                builder.setView(layout);
                                builder.setTitle("BEEPEER CLUB");
                                builder.setNeutralButton("Ok. Got it!!!", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        getSupportActionBar().setTitle("Traffic");
                                    }
                                });
                                builder.show();

                            } else if (drawerItem.getIdentifier() == 5) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(MapsActivity.this);   //never use application context to show dialog
                                LayoutInflater inflater = getLayoutInflater();
                                View layout = inflater.inflate(R.layout.about_us, null);
                                builder.setView(layout);
                                builder.setTitle("About us");
                                builder.setNeutralButton("Ok. Got it!!!", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        getSupportActionBar().setTitle("Traffic");
                                    }
                                });
                                builder.show();

                            } else if (drawerItem.getIdentifier() == 6) {
                                fragment = new SettingsFragment();

                            } else if (drawerItem.getIdentifier() == 7) {

                            } else if (drawerItem.getIdentifier() == 8) {

                            } else if (drawerItem.getIdentifier() == 9) {
                                signOut();
                            }
                            if (fragment != null) {
                                FragmentManager fragmentManager = getFragmentManager();
                                fragmentManager.beginTransaction()
                                        .replace(R.id.fragment_container, fragment)
                                        .addToBackStack(null)
                                        .commit();
                            } else if (dialogFragment != null) {
                                dialogFragment.show(getFragmentManager(), "Profile");
                                dialogFragment.setCancelable(true);
                            }
                        }
                    }
                })
                .withFireOnInitialOnClick(true)
                .withSavedInstance(savedInstanceState)
                .withSelectedItem(0)
                .build();
        result.getActionBarDrawerToggle().setDrawerIndicatorEnabled(true);
    }

    private void signOut() {
//        SharedPreferences.Editor editor = sharedpreferences.edit();
//        editor.clear();
//        editor.commit();
//        login = false;
//        MainActivity.isUserLoggedIn = false;
//        MainActivity.isGoogleSignIn = false;
//        MainActivity.isFacebookSignIn = false;
//        revokeGplusAccess();
//        callFacebookLogout(MapsActivity.this);
        showLogoutMessage();
//        signOutFromSystem();
    }

    private void showLogoutMessage() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage(getResources().getString(R.string.logoutmsg))
                .setCancelable(true)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        signOutFromSystem();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void signOutFromSystem() {
//        startActivity(loginIntent);
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

        finish();
//        System.exit(0);
    }

    private void signOutFromGplus() {
        if (MainActivity.mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(MainActivity.mGoogleApiClient);
            MainActivity.mGoogleApiClient.disconnect();
            MainActivity.mGoogleApiClient.connect();
//            MapsActivity.updateUI(false);
        }
    }

    private void menuBarHide() {
        bottomLayout = (LinearLayout) findViewById(R.id.bottomLayout);
        relativeLayout = (RelativeLayout) findViewById(R.id.mapsActivity);
        relativeLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                getSupportActionBar().show();
                bottomLayout.setVisibility(View.VISIBLE);
//                if (isOdd) {
//                    isOdd = false;
//                    bottomLayout.setVisibility(View.VISIBLE);
//                } else {
//                    isOdd = true;
//                    bottomLayout.setVisibility(View.INVISIBLE);
//                }
                return false;
            }
        });
        relativeLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                bottomLayout.setVisibility(View.VISIBLE);
                return false;
            }
        });
        Handler h = new Handler();

        h.postDelayed(new Runnable() {

            @Override
            public void run() {
                // DO DELAYED STUFF
//                getSupportActionBar().hide();
//                bottomLayout.setVisibility(View.INVISIBLE);
            }
        }, delaytime);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.action_refresh:

                return false;
//            case R.id.action_search:
//                DialogFragment dialogFragmentSearch = GooglePlacesAutocompleteFragment.newInstance();
//                dialogFragmentSearch.setCancelable(true);
//                dialogFragmentSearch.show(getFragmentManager(), "Search nearby places");
//                break;
//
//            case R.id.action_voice_search:
//                DialogFragment dialogFragmentVoiceSearch = SpeechToTextFragment.newInstance();
//                dialogFragmentVoiceSearch.setCancelable(true);
//                dialogFragmentVoiceSearch.show(getFragmentManager(), "Voice Search");
//                break;
        }
        return false;
    }

//    message for specific person
    private void writeAndGetMessage(String reciverId) {
        final Dialog dialog1 = new Dialog(MapsActivity.this, R.style.Dialog);
//        final ProgressBar mProgressBar = (ProgressBar) dialog1.findViewById(R.id.loadingProgressBar);
        dialog1.setContentView(R.layout.activity_to_do);
//        dialog1.setTitle("Map Chat");
        dialog1.setCancelable(true);
        outterRelativeLayout = (RelativeLayout) dialog1.findViewById(R.id.relativeLayoutMapChatOutter);
        innerRelativeLayout = (RelativeLayout) dialog1.findViewById(R.id.relativeLayoutMapChatInner);
        normalView = dialog1.findViewById(R.id.normalView);
        linearLayoutTitle = (LinearLayout)dialog1.findViewById(R.id.linearLayoutTitle);
        textViewTitle = (TextView)dialog1.findViewById(R.id.title);
        checkThemeColor();
        final MapChatItemAdapter mAdapter = new MapChatItemAdapter(context, R.layout.row_list_to_do);
        final String receiver = reciverId;
        mTextNewToDo = (EditText) dialog1.findViewById(R.id.textNewToDo);
        Button add = (Button) dialog1.findViewById(R.id.buttonAddToDo);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MapsActivity.this, "Successfully send your message", Toast.LENGTH_SHORT).show();
                final MapChat item = new MapChat();

                item.setmMessage(mTextNewToDo.getText().toString());
                item.setmAreaName(cityName);
                item.setmBeepeer(beepeerID);
                item.setmLatitude(String.valueOf(latitude));
                item.setmLongitude(String.valueOf(longitude));
                item.setmBeepeerName(beepeerName);
                item.setmReceiver(receiver);
                Log.e("Activity to do", beepeerID);
                // Insert the new item
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... params) {
                        try {
                            mMapChatTable.insert(item).get();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
//                                    mProgressBar.setVisibility(ProgressBar.VISIBLE);
                                }
                            });
                        } catch (Exception e) {
//                    createAndShowDialog(e, "Error");

                        }

                        return null;
                    }
                }.execute();

                mTextNewToDo.setText("");
                dialog1.dismiss();
            }
        });

//        mProgressBar.setVisibility(ProgressBar.GONE);
        ListView listViewToDo = (ListView) dialog1.findViewById(R.id.listViewToDo);
        listViewToDo.setAdapter(mAdapter);
        dialog1.show();
//        NotificationsManager.handleNotifications(this, SENDER_ID, MyHandler.class);
        // Load the items from the Mobile Service
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    final List<MapChat> results =
                            mMapChatTable.where().field("receiver").
                                    eq(val(beepeerID)).execute().get();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mAdapter.clear();

                            for (MapChat item : results) {
                                mAdapter.add(item);
                            }
                        }
                    });
                } catch (Exception e) {
//                    createAndShowDialog(e, "Error");

                }

                return null;
            }
        }.execute();
    }

//    message for specific person
    private void showList() {
        dialogContact = new Dialog(context, R.style.Dialog);
        dialogContact.setContentView(R.layout.friends_with_invitation);

        outterRelativeLayout = (RelativeLayout) dialogContact.findViewById(R.id.relativeLayoutMapChatOutter);
//        innerRelativeLayout = (RelativeLayout) dialogContact.findViewById(R.id.relativeLayoutInviteFriend);
        linearLayoutTitle = (LinearLayout)dialogContact.findViewById(R.id.linearLayoutTitle);
        textViewTitle = (TextView)dialogContact.findViewById(R.id.title);
        checkThemeColor2();

        dialogContact.setCanceledOnTouchOutside(true);
        listViewContacts = (ListView) dialogContact.findViewById(R.id.friends);
        buttonInvitation = (ImageButton) dialogContact.findViewById(R.id.buttonInvitation);
        buttonInvitation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inviteFriends();
            }
        });
        readContacts();
        new checkFriend().execute();
//        dialogContact.set
//        generateList();
    }
//    message for specific person
    private void generateList(final List<String> strings) {
        context = MapsActivity.this;
//        progressDialog.dismiss();
        contactAdapter = new ContactAdapter(context, contacts);
        listViewContacts.setAdapter(contactAdapter);
        dialogContact.show();
        listViewContacts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dialogContact.dismiss();
                contacts.clear();
//                Log.e("receiver id", strings.get(position));
//                storeData();
                writeAndGetMessage(strings.get(position));
            }
        });
    }

//    message for specific person
    public void readContacts() {
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);

        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
//                    Log.e("Friends" , name + " " + id);
                    // get the phone number
                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        phone = pCur.getString(
                                pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        phone = phone.replace("-", "");
                        phone = phone.replace(" ", "");
                        phone = phone.replace("+88", "");
                        phone = phone.replace("(", "");
                        phone = phone.replace(")", "");
                        phone = phone.replace("+1", "");
//                        contact = new Contact(name,phone);
//                        contacts.add(contact);
//                        Log.e("phone&Name" ,name+ " " + phone);
                    }
                    phones.add(phone);
                    names.add(name);
                    pCur.close();
                }
            }
        }
    }

//    message for specific person
    public class checkFriend extends AsyncTask<Void, Void, List<String>> {

        @Override
        protected void onPreExecute() {
//            progressDialog = ProgressDialog.show(getActivity(), "Data Loading", "Please wait ...", true, false);
            for(String phone : phones){
//                Log.e("Phone Number", phone);
            }
        }

        @Override
        protected List<String> doInBackground(Void... params) {
            try {

                MobileServiceList<User> resultUT = mUserTable.where().startsWith("MSISDN", "0").execute().get();
                Log.e("resultUT", resultUT.size()+"");
                for (int i = 0; i < resultUT.size(); i++) {
                    user = resultUT.get(i);
                    Log.e("server phone no ", user.getmMsisdn());
                    for (int j = 0; j < phones.size(); j++) {
                        if (phones.get(j).equals(user.getmMsisdn())) {
//                            Log.e("MSIDN & Phone", user.getmMsisdn() + " " + phones.get(j));
                            putReceiver = new ArrayList<String>();
                            contact = new Contact(names.get(j), phones.get(j));
                            contacts.add(contact);
                            putReceiver.add(user.getmId());
                        }
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

            return putReceiver;
        }

        @Override
        protected void onPostExecute(List<String> strings) {
//            progressDialog.dismiss();
//            phones.clear();
            generateList(strings);
        }
    }

//    map chat (for all within 1km)
    private void storeData() {
        Log.e("storeData", "ok");
        dialog = new Dialog(context, R.style.Dialog);
        dialog.setContentView(R.layout.map_chat);
//        dialog.setTitle("Message");
        outterRelativeLayout = (RelativeLayout) dialog.findViewById(R.id.relativeLayoutMapChatOutter);
        innerRelativeLayout = (RelativeLayout) dialog.findViewById(R.id.relativeLayoutMapChatInner);
        normalView = dialog.findViewById(R.id.normalView);
        linearLayoutTitle = (LinearLayout)dialog.findViewById(R.id.linearLayoutTitle);
        textViewTitle = (TextView)dialog.findViewById(R.id.title);
        checkThemeColor();
        editedAge = (EditText) dialog.findViewById(R.id.editedAge);
        TextView message = (TextView) dialog.findViewById(R.id.tvMessage);
        if (HomeFragment.newMSG.isEmpty()) {
            message.setText("No Message");
        } else {
            message.setText(HomeFragment.newMSG);
        }

//        editedAge.setHint("Write message...");
        btnOK = (Button) dialog.findViewById(R.id.btnOK);
//        btnOK.setText("Share");
        dialog.show();
        dialog.setCancelable(true);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                msg = String.valueOf(editedAge.getText());
                mapChat.setmAreaName(cityName);
                mapChat.setmBeepeer(beepeerID);
                mapChat.setmLatitude(String.valueOf(latitude));
                mapChat.setmLongitude(String.valueOf(longitude));
                mapChat.setmMessage(msg);
                mapChat.setmBeepeerName(beepeerName);
                Log.e("Map chat", beepeerID);
                // Insert the new item
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... params) {
                        try {
                            mMapChatTable.insert(mapChat).get();
                        } catch (Exception e) {
                        }

                        return null;
                    }
                }.execute();
//                dialog.dismiss();
                editedAge.setText("");
                dialog.dismiss();
            }
        });
    }

    private void checkThemeColor() {
        color = values.getString("color", "");
        if(color.equals("red")){
            outterRelativeLayout.setBackgroundColor(getResources().getColor(R.color.spring_light));
            innerRelativeLayout.setBackgroundColor(getResources().getColor(R.color.spring));
            normalView.setBackgroundColor(getResources().getColor(R.color.spring));
            linearLayoutTitle.setBackgroundColor(getResources().getColor(R.color.spring));
            textViewTitle.setBackgroundColor(getResources().getColor(R.color.spring));
        }
        else if(color.equals("yellow")){
            outterRelativeLayout.setBackgroundColor(getResources().getColor(R.color.summer_light));
            innerRelativeLayout.setBackgroundColor(getResources().getColor(R.color.summer));
            normalView.setBackgroundColor(getResources().getColor(R.color.summer));
            linearLayoutTitle.setBackgroundColor(getResources().getColor(R.color.summer));
            textViewTitle.setBackgroundColor(getResources().getColor(R.color.summer));
        }
        else if(color.equals("blue")){
            outterRelativeLayout.setBackgroundColor(getResources().getColor(R.color.autumn_light));
            innerRelativeLayout.setBackgroundColor(getResources().getColor(R.color.autumn));
            normalView.setBackgroundColor(getResources().getColor(R.color.autumn));
            linearLayoutTitle.setBackgroundColor(getResources().getColor(R.color.autumn));
            textViewTitle.setBackgroundColor(getResources().getColor(R.color.autumn));
        }
        else {
            outterRelativeLayout.setBackgroundColor(getResources().getColor(R.color.winter_light));
            innerRelativeLayout.setBackgroundColor(getResources().getColor(R.color.winter));
            normalView.setBackgroundColor(getResources().getColor(R.color.winter));
            linearLayoutTitle.setBackgroundColor(getResources().getColor(R.color.winter));
            textViewTitle.setBackgroundColor(getResources().getColor(R.color.winter));
        }
    }

    private void checkThemeColor2() {
        color = values.getString("color", "");
        if(color.equals("red")){
            outterRelativeLayout.setBackgroundColor(getResources().getColor(R.color.spring_light));
//            innerRelativeLayout.setBackgroundColor(getResources().getColor(R.color.spring));
            linearLayoutTitle.setBackgroundColor(getResources().getColor(R.color.spring));
            textViewTitle.setBackgroundColor(getResources().getColor(R.color.spring));
        }
        else if(color.equals("yellow")){
            outterRelativeLayout.setBackgroundColor(getResources().getColor(R.color.summer_light));
//            innerRelativeLayout.setBackgroundColor(getResources().getColor(R.color.summer));
            linearLayoutTitle.setBackgroundColor(getResources().getColor(R.color.summer));
            textViewTitle.setBackgroundColor(getResources().getColor(R.color.summer));
        }
        else if(color.equals("blue")){
            outterRelativeLayout.setBackgroundColor(getResources().getColor(R.color.autumn_light));
//            innerRelativeLayout.setBackgroundColor(getResources().getColor(R.color.autumn));
            linearLayoutTitle.setBackgroundColor(getResources().getColor(R.color.autumn));
            textViewTitle.setBackgroundColor(getResources().getColor(R.color.autumn));
        }
        else {
            outterRelativeLayout.setBackgroundColor(getResources().getColor(R.color.winter_light));
//            innerRelativeLayout.setBackgroundColor(getResources().getColor(R.color.winter));
            linearLayoutTitle.setBackgroundColor(getResources().getColor(R.color.winter));
            textViewTitle.setBackgroundColor(getResources().getColor(R.color.winter));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("onPause", "onPause MapsActivity");
//        HomeFragment.hasNew = false;
//        finish();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //add the values which need to be saved from the drawer to the bundle
        outState = result.saveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {

//        if (getFragmentManager().getBackStackEntryCount() > 0) {
//            getFragmentManager().popBackStack();
//        }
//        else {
            new AlertDialog.Builder(this)
                    .setTitle("Really Exit?")
                    .setMessage("Are you sure you want to exit?")
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface arg0, int arg1) {
                            signOutFromSystem();
                        }
                    }).create().show();

//        }
        //handle the back press :D close the drawer first and if the drawer is closed close the activity
//        if (result != null && result.isDrawerOpen()) {
//            result.closeDrawer();
//        } else {
//            super.onBackPressed();
//        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCall:
                call();
                break;
            case R.id.btnShare:
                share();
                break;
//            case R.id.btnCamera:
//                picShare();
//                break;
            case R.id.btnMessage:
//                writeAndGetMessage();
                storeData();
                break;
            case R.id.btnMChat:
                showList();
                break;
        }
    }

    private void picShare() {
//        if(HomeFragment.captureMapScreen() != null){
        myBitmap = HomeFragment.captureMapScreen();
//        }
//        else {
//            View v1 = getWindow().getDecorView().getRootView();
//            v1.setDrawingCacheEnabled(true);
//            myBitmap = v1.getDrawingCache();
//        }
//        String s = saveBitmap(myBitmap);
//        Intent share = new Intent();
//        share.setAction(Intent.ACTION_SEND);
//        share.putExtra(android.content.Intent.EXTRA_SUBJECT, "My Place");
//        share.putExtra(Intent.EXTRA_TEXT, "Beeper");
//        share.setType("image/png");
//        Uri myUri = Uri.parse("file://" + s);
//        share.putExtra(Intent.EXTRA_STREAM, myUri);
//        Log.e("Capture", "file://" + s);
//        startActivity(Intent.createChooser(share, "Beep"));
    }

    private void share() {
//        Intent sendIntent = new Intent();
//        sendIntent.setAction(Intent.ACTION_SEND);
//        sendIntent.putExtra(Intent.EXTRA_TEXT, sharedData);
//        sendIntent.setType("text/plain");
//        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));
        
//        SharePhoto photo;
        progressDialog = ProgressDialog.show(MapsActivity.this, "Please wait", "Loading...", true);
        picShare();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 5s = 5000ms
                progressDialog.dismiss();
                if(myBitmap != null){

                    Bitmap image = myBitmap;
                    SharePhoto photo = new SharePhoto.Builder()
                            .setBitmap(image)
                            .build();

                    SharePhotoContent content = new SharePhotoContent.Builder()
                            .addPhoto(photo)
                            .build();

//            shareDialog.show(content);

//            photo = new SharePhoto.Builder()
//                    .setBitmap(myBitmap)
//                    .build();
//
//            SharePhotoContent content = new SharePhotoContent.Builder()
//                    .setContentUrl(Uri.parse("https://www.beepeer.com"))
//                    .addPhoto(photo)
//                    .build();
//
                    ShareDialog.show(MapsActivity.this, content);
                }
                else
                    Toast.makeText(MapsActivity.this, "Sorry, please try again", Toast.LENGTH_LONG).show();
            }
        }, 5000);

//        ShareLinkContent content = new ShareLinkContent.Builder()
//                .setContentUrl(Uri.parse("https://www.beepeer.com"))
//                .build();
//        https://developers.facebook.com

    }

    private void call() {

        if (!sosNumber.equals("")) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            sos = "tel:" + sosNumber;
            callIntent.setData(Uri.parse(sos));
            startActivity(callIntent);
        }
        else {
            AlertDialog.Builder builder = new AlertDialog.Builder(MapsActivity.this);   //never use application context to show dialog
            LayoutInflater inflater=getLayoutInflater();
//            View layout=inflater.inflate(R.layout.pre_sos,null);
            View layout=inflater.inflate(R.layout.sos,null);
            builder.setView(layout);
//            builder.setTitle("SOS");
            builder.setTitle("SOS Call Settings");

            final EditText editTextSOSNumber = (EditText)layout.findViewById(R.id.editTextSOSNumber);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    sosNumber = editTextSOSNumber.getText().toString().trim();
                    values.edit().putString("sos", sosNumber).commit();
                    dialog.cancel();
//                    view.findViewById(R.id.textviewSOS).setBackgroundColor(getResources().getColor(R.color.white));
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
//                    view.findViewById(R.id.textviewSOS).setBackgroundColor(getResources().getColor(R.color.white));
                }
            });
            builder.setNeutralButton("Phonebook", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    showPhoneNumber();
                    if(!sosNumber.equals("")){
                        editTextSOSNumber.setText(sosNumber);
                    }
                }
            });
            builder.show();

//            builder.setNeutralButton("Ok. Got it!!!", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int id) {
//                    dialog.cancel();
//                }
//            });
//            builder.show();
        }
    }

    private void showPhoneNumber() {
        progressDialog = ProgressDialog.show(MapsActivity.this, "Please wait", "Loading...", true);
        dialogContact = new Dialog(MapsActivity.this);
        dialogContact.setContentView(R.layout.friends);
        dialogContact.setTitle("Phone Number");
        dialogContact.setCanceledOnTouchOutside(true);
        listViewContacts = (ListView) dialogContact.findViewById(R.id.friends);
        readContactsForSOS();
        generateListForSOS();
    }

    private void generateListForSOS() {
        progressDialog.dismiss();
        dialogContact.show();
        listViewContacts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dialogContact.dismiss();
                selectedContact = contacts.get(position);
                sosNumber = selectedContact.phone;
                contacts.clear();
            }
        });
    }

    private void readContactsForSOS() {
        phones.clear();
        names.clear();
        contacts = new ArrayList<Contact>();
        contacts.clear();
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);

        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
//                    Log.e("Friends" , name + " " + id);
                    // get the phone number
                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        phone = pCur.getString(
                                pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        phone = phone.replace("-", "");
                        phone = phone.replace(" ", "");
                        phone = phone.replace("+88", "");
                        phone = phone.replace("(", "");
                        phone = phone.replace(")", "");
                        phone = phone.replace("+1", "");
//                        phones.add(phone);
//                        names.add(name);
                    }
                    Log.e("name&number", name +" "+ phone);
                    contact = new Contact(name, phone);
                    contacts.add(contact);
                    pCur.close();
                }
            }
        }
        contactAdapter = new ContactAdapter(MapsActivity.this, contacts);
        listViewContacts.setAdapter(contactAdapter);
    }

    private String saveBitmap(Bitmap bitmap) {

        String filePath = Environment.getExternalStorageDirectory()
                + File.separator + "screenshot.png";
        File imagePath = new File(filePath);
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(imagePath);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            Log.e("GREC", e.getMessage(), e);
        } catch (IOException e) {
            Log.e("GREC", e.getMessage(), e);
        }
        return filePath;
    }

    //monitor phone call activities
    private class PhoneCallListener extends PhoneStateListener {

        String LOG_TAG = "LOGGING 123";
        private boolean isPhoneCalling = false;

        @Override
        public void onCallStateChanged(int state, String incomingNumber) {

            if (TelephonyManager.CALL_STATE_RINGING == state) {
                // phone ringing
                Log.i(LOG_TAG, "RINGING, number: " + incomingNumber);
            }

            if (TelephonyManager.CALL_STATE_OFFHOOK == state) {
                // active
                Log.i(LOG_TAG, "OFFHOOK");

                isPhoneCalling = true;
            }

            if (TelephonyManager.CALL_STATE_IDLE == state) {
                // run when class initial and phone call ended,
                // need detect flag from CALL_STATE_OFFHOOK
                Log.i(LOG_TAG, "IDLE");

                if (isPhoneCalling) {

                    Log.i(LOG_TAG, "restart app");

                    // restart app
                    Intent i = getBaseContext().getPackageManager()
                            .getLaunchIntentForPackage(
                                    getBaseContext().getPackageName());
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);

                    isPhoneCalling = false;
                }

            }
        }
    }

    public class BeepChat extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            mMapChatTable = mClient.getTable(MapChat.class);
            try {
                mapChats = mMapChatTable.where().field("receiver").
                        eq(val(beepeerID)).execute().get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    private void revokeGplusAccess() {
        if (MainActivity.mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(MainActivity.mGoogleApiClient);
            Plus.AccountApi.revokeAccessAndDisconnect(MainActivity.mGoogleApiClient)
                    .setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status arg0) {
                            MainActivity.mGoogleApiClient.connect();
                            MainActivity.isGoogleSignIn = false;
                        }

                    });
        }
    }

    private void inviteFriends(){
        appLinkUrl = "https://fb.me/993588140673786";

        if (AppInviteDialog.canShow()) {
            AppInviteContent content = new AppInviteContent.Builder()
                    .setApplinkUrl(appLinkUrl)
//                    .setPreviewImageUrl(previewImageUrl)
                    .build();
            AppInviteDialog.show(MapsActivity.this, content);
        }
    }
}