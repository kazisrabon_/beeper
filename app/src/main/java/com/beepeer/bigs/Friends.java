/*
 * Copyright (c) to srabon
 * IIT, University of Dhaka
 * kaziiit@gmail.com
 */

package com.beepeer.bigs;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.beepeer.bigs.adapter.ContactAdapter;
import com.beepeer.bigs.model.Contact;
import com.beepeer.bigs.model.User;
import com.microsoft.windowsazure.mobileservices.MobileServiceException;
import com.microsoft.windowsazure.mobileservices.MobileServiceList;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class Friends extends Fragment {

    View view;
    ListView listViewContacts;
    Contact contact;
    Context context;
    String name, phone, id;
    String[] projection;
    User user;
    ContactAdapter contactAdapter;
    List<String> phones = new ArrayList<String>();
    List<String> names = new ArrayList<String>();
    private List<Contact> contacts = new ArrayList<Contact>();
    private MobileServiceList<User> result;
    private ProgressDialog progressDialog;
    private List<String> putReceiver = new ArrayList<String>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.friends, container, false);
        } catch (InflateException e) {
        /* friend is already there, just return view as it is */
        }
        init();
        readContacts();
        new checkFriend().execute();

//        checkFriend();

        return view;
    }

    private void generateList() {
        context = getActivity().getApplicationContext();
        contactAdapter = new ContactAdapter(context, contacts);
        listViewContacts.setAdapter(contactAdapter);
        listViewContacts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Fragment fragmentChat = new Chat();
                Bundle bundle = new Bundle();
                bundle.putString("receiverID", putReceiver.get(position));
                fragmentChat.setArguments(bundle);
                getActivity().getFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, fragmentChat)
                        .commit();
            }
        });
    }

    private void init() {
        listViewContacts = (ListView) view.findViewById(R.id.friends);
    }

    public void readContacts() {
        ContentResolver cr = getActivity().getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);

        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
//                    Log.e("Friends" , name + " " + id);
                    // get the phone number
                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        phone = pCur.getString(
                                pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        phone = phone.replace("-", "");
                        phone = phone.replace(" ", "");
                        phone = phone.replace("+88", "");
                        phones.add(phone);
                        names.add(name);

//                        contact = new Contact(name,phone);
//                        contacts.add(contact);
//                        Log.e("phone&Name" ,name+ " " + phone);
                    }
                    pCur.close();
                }
            }
        }
    }

    public class checkFriend extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(getActivity(), "Data Loading", "Please wait ...", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {

                result = MainActivity.mUserTable.execute().get();
                for (int i = 0; i < result.size(); i++) {
                    user = result.get(i);
                    for (int j = 0; j < phones.size(); j++) {
                        if (phones.get(j).equals(user.getmMsisdn())) {
                            Log.e("MSIDN&Phone", user.getmMsisdn() + " " + phone);
                            contact = new Contact(names.get(j), phones.get(j));
                            contacts.add(contact);
                            putReceiver.add(user.getmId());
                        }
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (MobileServiceException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            progressDialog.dismiss();
            generateList();
        }
    }
}
