/*
 * Copyright (c) to srabon
 * IIT, University of Dhaka
 * kaziiit@gmail.com
 */

package com.beepeer.bigs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.beepeer.bigs.model.User;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import static com.microsoft.windowsazure.mobileservices.table.query.QueryOperations.val;


public class Profile extends DialogFragment {

    TextView tvName, tvAge, tvPhone, tvMail, tvGender, tvLocation;
    ImageButton ib1, ib2, ib3, ib5, ib6, ib7;
    EditText editedValue;
    Button btnOK;
    User user;
    View view;
    String prefs;
    private ImageButton ib4;
    private Dialog dialog;
    private String name="";
    private SharedPreferences sharedpreferences;
    private ArrayList<User> results;
    private ProgressDialog progressDialog;
    private ImageView circleView;
    private SharedPreferences app, profile;
    private String themeColor;
    private ScrollView scrollView;
    private View normalView;
    private SharedPreferences.Editor editor;
    private boolean TRUE = true;


    static Profile newInstance(){
        return new Profile();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.activity_profile, container, false);
            getDialog().setTitle("Profile");
        } catch (InflateException e) {
        /* profile is already there, just return view as it is */
        }
        DisplayMetrics metrics = getActivity().getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        Log.e("height&width", height + " " + width);
        getDialog().getWindow().setLayout(width, (height * 2) / 3);
        sharedpreferences = getActivity().getSharedPreferences(getResources().getString(R.string.id), Context.MODE_PRIVATE);
        prefs = sharedpreferences.getString(getResources().getString(R.string.mail), "");
//        app = getActivity().getSharedPreferences("app", Context.MODE_PRIVATE);
        profile = getActivity().getSharedPreferences("app", Context.MODE_PRIVATE);
        editor = profile.edit();
        themeColor = profile.getString("color", "blue");

        init();
        new Asy().execute();
//        initValue();
        editValue();

        return view;
    }

    //    image button click listener
    private void editValue() {
        ib1.setVisibility(View.VISIBLE);

        ib2.setVisibility(View.VISIBLE);
        ib2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.dialog);
                dialog.setTitle("Name");
                editedValue = (EditText) dialog.findViewById(R.id.editedAge);
                editedValue.setHint("Your Name.....");
                editedValue.setInputType(InputType.TYPE_CLASS_TEXT);
                btnOK = (Button) dialog.findViewById(R.id.btnOK);
                btnOK.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        name = String.valueOf(editedValue.getText());
                        tvName.setText(name);
//                        ib3.setVisibility(View.GONE);
                        editor.putString("name", name);
                        editor.commit();
                        dialog.dismiss();
//                        new EditAge().execute();
                    }
                });
                dialog.show();
            }
        });

        ib4.setVisibility(View.VISIBLE);
        ib4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.dialog);
                dialog.setTitle("Phone Number");
                editedValue = (EditText) dialog.findViewById(R.id.editedAge);
                editedValue.setHint("Your Phone Number.....");
                editedValue.setInputType(InputType.TYPE_CLASS_PHONE);
                btnOK = (Button) dialog.findViewById(R.id.btnOK);
                btnOK.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        name = String.valueOf(editedValue.getText());
                        tvPhone.setText(name);
//                        ib3.setVisibility(View.GONE);
                        editor.putString("phone", name);
                        editor.commit();
                        dialog.dismiss();
//                        new EditAge().execute();
                    }
                });
                dialog.show();
            }
        });

        ib5.setVisibility(View.VISIBLE);
        ib5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.dialog);
                dialog.setTitle("Email");
                editedValue = (EditText) dialog.findViewById(R.id.editedAge);
                editedValue.setHint("Your Email.....");
                editedValue.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                btnOK = (Button) dialog.findViewById(R.id.btnOK);
                btnOK.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        name = String.valueOf(editedValue.getText());
                        tvMail.setText(name);
//                        ib3.setVisibility(View.GONE);
                        editor.putString("email", name);
                        editor.commit();
                        dialog.dismiss();
//                        new EditAge().execute();
                    }
                });
                dialog.show();
            }
        });

        ib6.setVisibility(View.VISIBLE);
        ib6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.dialog);
                dialog.setTitle("Gender");
                editedValue = (EditText) dialog.findViewById(R.id.editedAge);
                editedValue.setHint("Your Gender.....");
                editedValue.setInputType(InputType.TYPE_CLASS_TEXT);
                btnOK = (Button) dialog.findViewById(R.id.btnOK);
                btnOK.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        name = String.valueOf(editedValue.getText());
                        tvGender.setText(name);
//                        ib3.setVisibility(View.GONE);
                        editor.putString("gender", name);
                        editor.commit();
                        dialog.dismiss();
//                        new EditAge().execute();
                    }
                });
                dialog.show();
            }
        });

        ib7.setVisibility(View.VISIBLE);
        ib7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.dialog);
                dialog.setTitle("Location");
                editedValue = (EditText) dialog.findViewById(R.id.editedAge);
                editedValue.setHint("Your Location.....");
                editedValue.setInputType(InputType.TYPE_CLASS_TEXT);
                btnOK = (Button) dialog.findViewById(R.id.btnOK);
                btnOK.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        name = String.valueOf(editedValue.getText());
                        tvLocation.setText(name);
//                        ib3.setVisibility(View.GONE);
                        editor.putString("location", name);
                        editor.commit();
                        dialog.dismiss();
//                        new EditAge().execute();
                    }
                });
                dialog.show();
            }
        });

        ib3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.dialog);
                dialog.setTitle("Age");
                editedValue = (EditText) dialog.findViewById(R.id.editedAge);
                editedValue.setHint("Your Age.....");
                btnOK = (Button) dialog.findViewById(R.id.btnOK);
                btnOK.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        name = String.valueOf(editedValue.getText());
                        tvAge.setText(name);
//                        ib3.setVisibility(View.GONE);
                        editor.putString("age", name);
                        editor.commit();
                        dialog.dismiss();
//                        new EditAge().execute();
                    }
                });
                dialog.show();
            }
        });
        if(MainActivity.drawable != null){
         circleView.setImageDrawable(MainActivity.drawable);
        }
    }

    //     put data from DB
    private void initValue() {

        if (!results.isEmpty()) {
            user = results.get(0);
            tvName.setText(user.getmName());
            tvAge.setText(user.getmAge());
            tvPhone.setText(user.getmMsisdn());
        }
        else{
            tvName.setText("Srabon");
            tvAge.setText("23");
            tvPhone.setText("01684397844");
        }
    }

    //    initialize views
    private void init() {
        tvName = (TextView) view.findViewById(R.id.textView1);
        tvName.setText(profile.getString("name", "Name"));

        tvAge = (TextView) view.findViewById(R.id.textView2);
        tvAge.setText(profile.getString("age", "Age"));

        tvPhone = (TextView) view.findViewById(R.id.textView3);
        tvPhone.setText(profile.getString("phone", "Phone"));

        tvMail = (TextView) view.findViewById(R.id.textView4);
        tvMail.setText(profile.getString("email", "Email"));

        tvGender = (TextView) view.findViewById(R.id.textView5);
        tvGender.setText(profile.getString("gender", "Gender"));

        tvLocation = (TextView) view.findViewById(R.id.textView6);
        tvLocation.setText(profile.getString("location", "Location"));

        withoutService(TRUE);

        ib1 = (ImageButton) view.findViewById(R.id.ib1);
        ib2 = (ImageButton) view.findViewById(R.id.ib2);
        ib3 = (ImageButton) view.findViewById(R.id.ib3);
        ib4 = (ImageButton) view.findViewById(R.id.ib4);
        ib5 = (ImageButton) view.findViewById(R.id.ib5);
        ib6 = (ImageButton) view.findViewById(R.id.ib6);
        ib7 = (ImageButton) view.findViewById(R.id.ib7);
        circleView = (ImageView)view.findViewById(R.id.circleView);
        changeUIAccordingToTheme();
    }

    private void withoutService(boolean b) {
        tvName.setText(profile.getString("name", "Srabon"));
        tvAge.setText(profile.getString("age", "23"));
        tvPhone.setText(profile.getString("phone", "01684397844"));
        tvMail.setText(profile.getString("email", "kaziiit@gmail.com"));
        tvGender.setText(profile.getString("gender", "Male"));
        tvLocation.setText(profile.getString("location", "Dhaka"));
    }

    private void changeUIAccordingToTheme() {
        scrollView = (ScrollView) view.findViewById(R.id.scrollViewProfile);
        normalView = view.findViewById(R.id.viewProfileDivider);

        if(themeColor.equals("red")){
            scrollView.setBackgroundColor(getResources().getColor(R.color.spring_light));
            normalView.setBackgroundColor(getResources().getColor(R.color.spring));
        }
        else if(themeColor.equals("yellow")){
            scrollView.setBackgroundColor(getResources().getColor(R.color.summer_light));
            normalView.setBackgroundColor(getResources().getColor(R.color.summer));
        }
        else if(themeColor.equals("blue")){
            scrollView.setBackgroundColor(getResources().getColor(R.color.autumn_light));
            normalView.setBackgroundColor(getResources().getColor(R.color.autumn));
        }
        else {
            scrollView.setBackgroundColor(getResources().getColor(R.color.winter_light));
            normalView.setBackgroundColor(getResources().getColor(R.color.winter));
        }
    }

    //    get data from DB
    public class Asy extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(getActivity(), "Data Loading", "Please wait ...", true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                results = MainActivity.mUserTable.where().field("email").
                        eq(val(prefs)).execute().get();
                if (results.isEmpty()) {
                    Log.e("Profile", results.toString());
                } else {
                    Log.e("Profile", results.toString());
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            progressDialog.dismiss();
            initValue();
        }

    }

    //    Modify DB
    public class EditAge extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            user.setmAge(name);
            try {
                MapsActivity.mUserTable.update(user).get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}