/*
 * Copyright (c) to srabon
 * IIT, University of Dhaka
 * kaziiit@gmail.com
 */

package com.beepeer.bigs;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.beepeer.bigs.model.Beep;
import com.google.android.gms.maps.model.LatLng;
import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.MobileServiceException;
import com.microsoft.windowsazure.mobileservices.MobileServiceList;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilterResponse;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.table.TableOperationCallback;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;

public class BeepService extends Service {

    public static final String EASY = "0";
    public static final String MEDIUM = "1";
    public static final String BUSY = "2";
    private static final String TAG = "IAmHereService";
    public static double latitude = 0;
    public static double longitude = 0;
    public static String cityName = "";
    LocationService locationService;
    double lastLatitude = 0;
    double lastLongitude = 0;
    SharedPreferences idSharedpreferences;
    private String address = "", city, country;
    private MobileServiceClient mClient;
    private MobileServiceTable<Beep> mUserTable;
    private MobileServiceList<Beep> results;
    private double markLatitude;
    private double markLongitude;
    private int sound, hasExtra=0, statusId;
    private SharedPreferences values, sharedpreferences;
    private String hasNotification = "", hasSound = "", beepeerName;
    private long lastLoginDate, currentDate;
    private long unusedTime;
    private int unusedDays;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {

        try {

            LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );

                if ( manager.isProviderEnabled( LocationManager.NETWORK_PROVIDER ) ) {
                    Log.e("location at Service", "Enabled" );
                    mClient = new MobileServiceClient("https://beepeer.azure-mobile.net/", "TBpzCbTsUcdYjPiGBdZunJfjRnhEiR93", this);
                    mUserTable = mClient.getTable(Beep.class);

                    SharedPreferences sharedPreferences = getSharedPreferences("com.beepeer.beepeer.LastLocation", MODE_PRIVATE);
                    idSharedpreferences = getSharedPreferences(getResources().getString(R.string.id), Context.MODE_PRIVATE);
                    lastLatitude = Double.parseDouble(sharedPreferences.getString("LastLatitude", "").trim());
                    lastLongitude = Double.parseDouble(sharedPreferences.getString("LastLongitude", "").trim());
                    sharedpreferences = getSharedPreferences(getResources().getString(R.string.id), Context.MODE_PRIVATE);
                    beepeerName = sharedpreferences.getString(getString(R.string.beepeerName), "");
                    values = getSharedPreferences("app", Context.MODE_PRIVATE);
                    hasNotification = values.getString("notification", "");
                    hasSound = values.getString("sound", "");
                    unusedDays = values.getInt("unusedDays", 0);
                    lastLoginDate = values.getLong("lastLoginDate", new Date().getTime());
                    Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+1:00"));
                    Date serviceDate = cal.getTime();
                    currentDate = serviceDate.getTime();
                    sendUnusedNotification(lastLoginDate,currentDate);
                }
            else {
                    Log.e("location at Service", "Disabled" );
//                    stopSelf();
//                    showSettingsAlert("NETWORK");
                }
//            Log.e("NotificationService", hasNotification+" "+hasSound);

        } catch (Exception e) {
            //createAndShowDialog(new Exception("There was an error creating the Mobile Service. Verify the URL"), "Error");
        }

//        Log.d(TAG, "onCreate");
    }

    private void sendUnusedNotification(long lastLoginDate, long currentDate) {
        unusedTime = (currentDate - lastLoginDate)/ (60 * 60 * 1000);
        Log.e("unusedTime", unusedTime+"");
        if(unusedTime <48 ){
            unusedDays = 0;
        }
        if(unusedDays == 0){
            if(unusedTime >= 48 && unusedTime < 96){
                sendNotification("Hey "+beepeerName+"! Where are you? Peers are missing you on the road", R.mipmap.ic_launcher, "Beepeer");
                unusedDays = 2;
            }
        }
        if(unusedDays == 2){
            if(unusedTime >= 96 && unusedTime < 168){
                sendNotification("Hey "+beepeerName+"! We are really missing you. There are a lot going on the road.", R.mipmap.ic_launcher, "Beepeer");
                unusedDays = 4;
            }
        }
        if(unusedDays == 4){
            if(unusedTime >= 168){
                sendNotification("Earth to "+beepeerName+"! Earth to "+beepeerName+"! BEEP back if you copy!!!!", R.mipmap.ic_launcher, "Beepeer");
                unusedDays = 7;
            }
        }
        Log.e("unusedDays", unusedDays+"");
        values.edit().putInt("unusedDays", unusedDays).commit();
    }

    @Override
    public void onStart(Intent intent, int startId) {

        LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );

        if ( manager.isProviderEnabled( LocationManager.NETWORK_PROVIDER ) ) {
            locationService = new LocationService(this);
            Location nwLocation = locationService.getLocation(LocationManager.NETWORK_PROVIDER);

            if (nwLocation != null) {
                latitude = nwLocation.getLatitude();
                longitude = nwLocation.getLongitude();
            } else {
//            showSettingsAlert("NETWORK");
                //Toast.makeText(getApplicationContext(), "Network Provider not enabled.", Toast.LENGTH_LONG).show();
            }

            try {
                Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                List<Address> addresses;
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
                cityName = addresses.get(0).getAddressLine(0);
                String stateName = addresses.get(0).getAddressLine(1);
                String countryName = addresses.get(0).getAddressLine(2);
//            Log.e("BeepService", cityName + " " + stateName + " " + countryName);
                String prefsId = idSharedpreferences.getString(getResources().getString(R.string.mail), "");
                double distance = getDistance();
                final Beep item = new Beep();

                item.setmBeeper(prefsId);
                item.setmAreaName(cityName);
                item.setmLongitude(String.valueOf(longitude));
                item.setmLatitude(String.valueOf(latitude));

//            Log.e("Service", prefsId);

                if (distance > 1) {
                    Log.e("Service", EASY+"");
                    item.setmAreaStatusId(EASY);
                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... params) {
                            insertBeep(item);
                            return null;
                        }
                    }.execute();
                }
                if (distance <= 1 && distance > .5) {
                    Log.e("Service", MEDIUM+"");
                    item.setmAreaStatusId(MEDIUM);
                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... params) {
                            insertBeep(item);
                            return null;
                        }
                    }.execute();

                }
                else if (distance <= .5 && distance >= .2) {
                    Log.e("Service", BUSY+"");
                    item.setmAreaStatusId(BUSY);
                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... params) {
                            insertBeep(item);
                            return null;
                        }
                    }.execute();

                } else {

                }

//            Toast.makeText(getApplicationContext(), "Location is - \nLat: " + latitude + "\nLong: " + longitude + "\nAddress: " + stateName + "\nCity: " + cityName + "\nCountry: " + countryName, Toast.LENGTH_SHORT).show();
//            Toast.makeText(getApplicationContext(), String.valueOf(getDistance()) + " KM", Toast.LENGTH_LONG).show();

            } catch (IOException e) {
                e.printStackTrace();
            }

//        Log.d(TAG, "onStart");
            getData();
            stopSelf();
        }


    }

    private void getData() {
        new AsyncTask<Void, Void, List<Beep>>() {
            @Override
            protected List<Beep> doInBackground(Void... params) {

                try {
                    results = mUserTable.execute().get();

                    for (Beep result : results) {

                        markLatitude = Double.parseDouble(result.getmLatitude());
                        markLongitude = Double.parseDouble(result.getmLongitude());
                        double distance = getDistance();
                        if(distance > .2){
                            if(hasNotification.equals("yes")){
                                if (Integer.parseInt(result.getmAreaStatusId()) == 4) {
                                    if (calculateDistance(new LatLng(latitude, longitude), new LatLng(markLatitude, markLongitude)) < 1) {
                                        sendNotification("There has been an accident around, please be cautious", R.mipmap.ic_winter_4, "Road Accident");
                                        sound = 0;
                                        hasExtra = 1;
                                        statusId = 4;
                                    }
                                }
                                else if (Integer.parseInt(result.getmAreaStatusId()) == 5) {
                                    if (calculateDistance(new LatLng(latitude, longitude), new LatLng(markLatitude, markLongitude)) < 1) {
                                        sendNotification("Heavy footers pilling up, might hit up traffic", R.mipmap.ic_winter_14, "Construction");
                                        sound = 0;
                                        hasExtra = 1;
                                        statusId = 5;
                                    }
                                }
                                else if (Integer.parseInt(result.getmAreaStatusId()) == 6) {
                                    if (calculateDistance(new LatLng(latitude, longitude), new LatLng(markLatitude, markLongitude)) < 1) {
                                        sendNotification("Water Water everywhere, not a road to ride", R.mipmap.ic_winter_5, "Water Log");
                                        sound = 0;
                                        hasExtra = 1;
                                        statusId = 6;
                                    }
                                }
                                else if (Integer.parseInt(result.getmAreaStatusId()) == 7) {
                                    if (calculateDistance(new LatLng(latitude, longitude), new LatLng(markLatitude, markLongitude)) < 1) {
                                        sendNotification("Got your license? Brothers in Blue are looking at you +_*", R.mipmap.ic_winter_12, "Police");
                                        sound = 0;
                                        hasExtra = 1;
                                        statusId = 7;
                                    }
                                }
                                else if (Integer.parseInt(result.getmAreaStatusId()) == 8) {
                                    if (calculateDistance(new LatLng(latitude, longitude), new LatLng(markLatitude, markLongitude)) < 1) {
                                        sendNotification("There's a road block. Let's look for a better route", R.mipmap.ic_winter_15, "Road Block");
                                        sound = 0;
                                        hasExtra = 1;
                                        statusId = 6;
                                    }
                                }
                                if(hasExtra == 0){
                                    if (Integer.parseInt(result.getmAreaStatusId()) == 0) {
                                        if (calculateDistance(new LatLng(latitude, longitude), new LatLng(markLatitude, markLongitude)) < 1) {
                                            sendNotification("Road is clear. Time to ride!", R.drawable.green_marker, "Go");
                                            sound = 0;
                                            statusId = 0;
                                        }
                                    }
                                    else if (Integer.parseInt(result.getmAreaStatusId()) == 1) {
                                        if (calculateDistance(new LatLng(latitude, longitude), new LatLng(markLatitude, markLongitude)) < 1) {
                                            sendNotification("Slow traffic, move carefully", R.drawable.yellow_marker, "Easy");
                                            sound = 1;
                                            statusId = 1;
                                        }
                                    }
                                    else if (Integer.parseInt(result.getmAreaStatusId()) == 2) {
                                        if (calculateDistance(new LatLng(latitude, longitude), new LatLng(markLatitude, markLongitude)) < 1) {
                                            sendNotification("CODE RED! CODE RED! Looks like you need an escape", R.drawable.red_marker, "Busy");
                                            sound = 3;
                                            statusId = 2;
                                        }
                                    }
                                }
                            }
                        }

                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (MobileServiceException e) {
                    e.printStackTrace();
                }

                return results;
            }

            @Override
            protected void onPostExecute(List<Beep> aVoid) {
//                markerSetUp(aVoid);
            }
        }.execute();

    }

    private void insertBeep(Beep item) {
        mClient.getTable(Beep.class).insert(item, new TableOperationCallback<Beep>() {
            public void onCompleted(Beep entity, Exception exception, ServiceFilterResponse response) {
                if (exception == null) {
                    // Insert succeeded
                } else {
                    // Insert failed
                }
            }
        });
    }

    private int calculateDistance(LatLng ll1, LatLng ll2) {
        Location locationA = new Location("Last Location");
        locationA.setLatitude(ll1.latitude);
        locationA.setLongitude(ll1.longitude);
        Location locationB = new Location("Get Location");
        locationB.setLatitude(ll2.latitude);
        locationB.setLongitude(ll2.longitude);
        float distance = locationA.distanceTo(locationB) / 1000;

        return (int) distance;
    }

    private double getDistance() {
        Location locationA = new Location("Last Location");
        locationA.setLatitude(lastLatitude);
        locationA.setLongitude(lastLongitude);
        Location locationB = new Location("Current Location");
        locationB.setLatitude(latitude);
        locationB.setLongitude(longitude);
        float distance = locationA.distanceTo(locationB) / 1000;

        return distance;
    }

    public void showSettingsAlert(String provider) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                getApplicationContext());

        alertDialog.setTitle(provider + " SETTINGS");

        alertDialog
                .setMessage(provider + " is not enabled! Want to go to settings menu?");

        alertDialog.setPositiveButton("Settings",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        getApplicationContext().startActivity(intent);
                    }
                });

        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

//        alertDialog.show();
    }

    @Override
    public void onDestroy() {
        SharedPreferences lastLocation;
        lastLocation = this.getSharedPreferences("com.beepeer.beepeer.LastLocation", MODE_PRIVATE);

        SharedPreferences.Editor prefEditor = lastLocation.edit();
        prefEditor.putString("LastLatitude", String.valueOf(latitude));
        prefEditor.putString("LastLongitude", String.valueOf(longitude));
        prefEditor.commit();

//        Toast.makeText(this, "MyService Stopped", Toast.LENGTH_LONG).show();
        AlarmManager alarm = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarm.set(
                AlarmManager.RTC_WAKEUP,
                System.currentTimeMillis() + (1000 * 60 * 5),
                PendingIntent.getService(this, 0, new Intent(this, BeepService.class), 0)
        );
//        Log.d(TAG, "onDestroy");
    }

    private void sendNotification(String alert, int icon, String msg) {
        Intent intent = new Intent(this, Splash.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, 0);
        long[] vibrate = { 0, 100, 200, 300 };

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setAutoCancel(true);
        builder.setContentTitle("Beepeer");
        builder.setContentText(alert);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.addAction(icon, msg, pIntent);
        builder.setVibrate(vibrate);
        builder.setAutoCancel(true);

//Sound effect for notification
        if(hasSound.equals("yes")){
            if (sound == 0) {
                builder.setSound(Uri.parse("android.resource://"
                        + getApplicationContext().getPackageName() + "/" + R.raw.notification));
            }
            else if(sound == 1){
                builder.setSound(Uri.parse("android.resource://"
                        + getApplicationContext().getPackageName() + "/" + R.raw.notification));
//                Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//                builder.setSound(alarmSound);
            }
            else if(sound ==3) {
                builder.setSound(Uri.parse("android.resource://"
                        + getApplicationContext().getPackageName() + "/" + R.raw.notification));
//                builder.setSound(Uri.parse("android.resource://"
//                        + getApplicationContext().getPackageName() + "/" + R.raw.beepeer));
            }
        }
        Notification notification = builder.build();
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager.notify(8, notification);
//        notification.sound = Uri.parse("android.resource://"
//                + getApplicationContext().getPackageName() + "/" + R.raw.beepeer);
//        notification.defaults |= Notification.DEFAULT_SOUND;
//        notification.defaults |= Notification.DEFAULT_VIBRATE;
//        long[] vibrate = { 0, 100, 200, 300 };
//        notification.vibrate = vibrate;
    }
}
