/*
 * Copyright (c) to srabon
 * IIT, University of Dhaka
 * kaziiit@gmail.com
 */

package com.beepeer.bigs;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.beepeer.bigs.model.User;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;
import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.http.NextServiceFilterCallback;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilter;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilterRequest;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilterResponse;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.table.TableOperationCallback;
import com.microsoft.windowsazure.mobileservices.table.query.QueryOrder;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static com.microsoft.windowsazure.mobileservices.table.query.QueryOperations.val;

public class RegisterActivity extends ActionBarActivity{
    TextView registerErrorMsg;
//    ProgressBar mProgressBar;
    Button btnRegister;
    Button btnLinkToLogin;
    private EditText name;
    private EditText phoneNumber;
    private EditText email;
    private EditText age, location;
    private MobileServiceClient mClient;
    private MobileServiceTable<User> mUserTable;
    private String txtlocation = "Dhaka", txtname = "Srabon", txtemail = "kaziiit@gmail.com",
            txtphone = "01684397844", txtage = "23", txtgender = "Male", txtId = "1234567";
    private SharedPreferences sharedpreferences, profile;
    private ProgressDialog progressDialog;
    private Spinner gender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration);

        name = (EditText) findViewById(R.id.registerName);
        phoneNumber = (EditText) findViewById(R.id.registerPhoneNumber);
        email = (EditText) findViewById(R.id.registerEmail);
        age = (EditText) findViewById(R.id.registerAge);
        gender = (Spinner) findViewById(R.id.registerGender);
        gender.setPrompt("Gender");
        addItemsOnSpinner();
        addListenerOnSpinnerItemSelection();
        location = (EditText) findViewById(R.id.registerLocation);

        sharedpreferences = getSharedPreferences(getResources().getString(R.string.id), Context.MODE_PRIVATE);
        profile = getSharedPreferences("app", Context.MODE_PRIVATE);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnLinkToLogin = (Button) findViewById(R.id.btnLinkToLoginScreen);
        registerErrorMsg = (TextView) findViewById(R.id.register_error);
//        mProgressBar = (ProgressBar) findViewById(R.id.loadingProgressBar);
//        mProgressBar.setVisibility(ProgressBar.GONE);
        try {
            mClient = new MobileServiceClient(
                    "https://beepeer.azure-mobile.net/",
                    "TBpzCbTsUcdYjPiGBdZunJfjRnhEiR93",
                    this).withFilter(new ProgressFilter());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        btnRegister.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                // Get the Mobile Service Table instance to use
                mUserTable = mClient.getTable(User.class);
                Log.e("RegisterActivity", mUserTable.toString());
                if(isValidEmail(email.getText().toString().trim())){
                    progressDialog = ProgressDialog.show(RegisterActivity.this, "Please wait", "Processing...", true);
                    addUser();
                }
                else {
                    registerErrorMsg.setVisibility(View.VISIBLE);
                    registerErrorMsg.setText("Invalid data!!! You must give a valid email address");
                }
            }
        });

        // Link to Login Screen
        btnLinkToLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),
                        MainActivity.class);
                startActivity(i);
                // Close Registration View
                finish();
            }
        });
    }

    private void addListenerOnSpinnerItemSelection() {
        gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position > 0){
                    ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
                    txtgender = parent.getItemAtPosition(position).toString().trim();
                    Toast.makeText(RegisterActivity.this, parent.getItemAtPosition(position).toString().trim(),
                            Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                gender.setSelection(1);
                txtgender = "Male";
            }
        });
    }

    private void addItemsOnSpinner() {
        List<String> list = new ArrayList<>();
//        list.add("Gender");
        list.add("Male");
        list.add("Female");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                R.layout.my_spinner_style, list){
            public View getView(int position, View convertView,
                                ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(16);
                ((TextView) v).setTextColor(
                        getResources()
                                .getColorStateList(R.color.white));
                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView,
                        parent);
                v.setBackgroundResource(R.drawable.edittextwhite_textfield_activated_holo_light);

                ((TextView) v).setTextColor(getResources().getColorStateList(
                        R.color.black));
//                ((TextView) v).setTypeface(fontStyle);
                ((TextView) v).setGravity(Gravity.CENTER);

                return v;
            }
        };
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        gender.setAdapter(dataAdapter);
    }

    private boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public void addUser() {
//        this code is muted for service
//        if (mClient == null) {
//            return;
//        }

        // Create a new item
        final User item = new User();

        txtname = name.getText().toString().trim();
        txtemail = email.getText().toString().trim();
        txtphone = phoneNumber.getText().toString().trim();
        txtage = age.getText().toString().trim();
//        txtgender = gender.getText().toString().trim();
        txtlocation = location.getText().toString().trim();
        Log.e("values", txtemail);

        item.setmName(name.getText().toString());
        item.setmEmail(email.getText().toString());
        item.setmMsisdn(phoneNumber.getText().toString());
        item.setmAge(age.getText().toString());
        Log.e(RegisterActivity.this + "", item.getmName());

        txtId = "1234567";
        savetoSF(txtId, txtname, txtage, txtemail, txtphone, txtgender, txtlocation);
        // Insert the new item
//        this code is muted for service

       /* new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {

                mClient.getTable(User.class).insert(item, new TableOperationCallback<User>() {
                    public void onCompleted(User entity, Exception exception, ServiceFilterResponse response) {
                        if (exception == null) {
                            // Insert succeeded
                            Log.e(RegisterActivity.this+"", mClient.toString());
                        } else {
                            // Insert failed
                        }
                    }
                });
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                            if(!entity.isComplete()){
//                                mAdapter.add(entity);
//                            }
                    }
                });
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                new checkUser().execute();
            }
        }.execute();    */
    }

    public class checkUser extends AsyncTask<Void, Void, List<User>> {
        List<User> result = new ArrayList<User>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    // Do something after 5s = 5000ms

                }
            }, 5000);
        }

        @Override
        protected List<User> doInBackground(Void... params) {

            try {
                Log.e("new user data1", txtemail);
                result = mUserTable.where().field("email").eq(val(txtemail))
                        .orderBy("createdAt", QueryOrder.Descending).top(1).execute().get();

            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(List<User> users) {
            super.onPostExecute(users);
            for(User user : users){
                txtId = "";
                txtId = user.getmId();
            }
            Log.e("new user data2", txtId);
            savetoSF(txtId, txtname, txtage, txtemail, txtphone, txtgender, txtlocation);
        }
    }

    private void savetoSF(String txtId, String txtname, String txtage, String txtemail,
                          String txtphone, String txtgender, String txtlocation) {
        sharedpreferences.edit().putString(getResources().getString(R.string.mail), txtemail).commit();
        sharedpreferences.edit().putString(getResources().getString(R.string.beepeerID), txtId).commit();
        sharedpreferences.edit().putString(getResources().getString(R.string.beepeerName), txtname).commit();
        sharedpreferences.edit().putString(getResources().getString(R.string.age), txtage).commit();

        profile.edit().putString("email", txtemail).commit();
        profile.edit().putString(getResources().getString(R.string.beepeerID), txtId).commit();
        profile.edit().putString("name", txtname).commit();
        profile.edit().putString("age", txtage).commit();
        profile.edit().putString("phone", txtphone).commit();
        profile.edit().putString("gender", txtgender).commit();
        profile.edit().putString("location", txtlocation).commit();

        gotoMap();
    }

    private void gotoMap() {
        HomeFragment.hasNew = false;
        if (mClient == null) {
            MainActivity.isUserLoggedIn = false;
            MainActivity.isFacebookSignIn = false;
            MainActivity.isGoogleSignIn = false;
            return;
        }else{
            progressDialog.dismiss();
            MainActivity.isUserLoggedIn = true;
            MainActivity.isFacebookSignIn = false;
            MainActivity.isGoogleSignIn = false;

            nullifyValue();
            startService(new Intent(RegisterActivity.this, BeepService.class));
            startActivity(new Intent(RegisterActivity.this, MapsActivity.class));
        }
    }

    private void nullifyValue() {
        name.setText("");
        email.setText("");
        phoneNumber.setText("");
        age.setText("");
//        gender.setText("");
        location.setText("");
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    /**
     * Creates a dialog and shows it
     *
     * @param exception The exception to show in the dialog
     * @param title     The dialog title
     */
    private void createAndShowDialog(Exception exception, String title) {
        Throwable ex = exception;
        if (exception.getCause() != null) {
            ex = exception.getCause();
        }
        createAndShowDialog(ex.getMessage(), title);
    }

    /**
     * Creates a dialog and shows it
     *
     * @param message The dialog message
     * @param title   The dialog title
     */
    private void createAndShowDialog(final String message, final String title) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage(message);
        builder.setTitle(title);
        builder.create().show();
    }

    private class ProgressFilter implements ServiceFilter {

        @Override
        public ListenableFuture<ServiceFilterResponse> handleRequest(ServiceFilterRequest request, NextServiceFilterCallback nextServiceFilterCallback) {

            final SettableFuture<ServiceFilterResponse> resultFuture = SettableFuture.create();


            runOnUiThread(new Runnable() {

                @Override
                public void run() {
//                    if (mProgressBar != null) mProgressBar.setVisibility(ProgressBar.VISIBLE);
                }
            });

            ListenableFuture<ServiceFilterResponse> future = nextServiceFilterCallback.onNext(request);

            Futures.addCallback(future, new FutureCallback<ServiceFilterResponse>() {
                @Override
                public void onFailure(Throwable e) {
                    resultFuture.setException(e);
                }

                @Override
                public void onSuccess(ServiceFilterResponse response) {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
//                            if (mProgressBar != null) mProgressBar.setVisibility(ProgressBar.GONE);
                        }
                    });

                    resultFuture.set(response);
                }
            });

            return resultFuture;
        }
    }
}
