/*
 * Copyright (c) to srabon
 * IIT, University of Dhaka
 * kaziiit@gmail.com
 */

package com.beepeer.bigs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beepeer.bigs.adapter.PlaceAdapter;
import com.beepeer.bigs.maprotation.BearingToNorthProvider;
import com.beepeer.bigs.model.Beep;
import com.beepeer.bigs.model.GooglePlaces;
import com.beepeer.bigs.model.MapChat;
import com.beepeer.bigs.model.NearBy;
import com.beepeer.bigs.model.PlaceDetails;
import com.beepeer.bigs.model.Places;
import com.beepeer.bigs.model.PlacesList;
import com.beepeer.bigs.model.Results;
import com.facebook.FacebookSdk;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.widget.AppInviteDialog;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;
import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.http.NextServiceFilterCallback;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilter;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilterRequest;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilterResponse;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.table.TableOperationCallback;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class HomeFragment extends Fragment implements SensorEventListener, BearingToNorthProvider.ChangeEventListener, LocationListener {

    private static final String KEY_TITLE = "title";
    public static final String APIKEY = "AIzaSyA0J75-4LgS8Df8ZsP1KHPG5AqXmYw7Cow";
    public static final int RADIUS_METER = 1000;
    public static List<Beep> results;
    private static View view;
    public static Bitmap bitmapNew;
    private static Bitmap sharedBitmap;
    private static String path;
    Bitmap bitmap;
    public static GoogleMap mMap; // Might be null if Google Play services APK is not available.
    LocationService LocationService;
    double latitude = 0;
    double longitude = 0;
    //    SharedPreferences idPrefs, latlongPrefs;
    double markLatitude = 0;
    double markLongitude = 0;
    String areaName;
    int areaId;
    String sos = "tel:+8801684397844";
    LatLng latLng;
    String beepeerID;
    private RelativeLayout map, rel1;
    private MobileServiceClient mClient;
    public static MobileServiceTable<Beep> mBeepTable;
    private Bitmap myBitmap;
    private boolean login;
    private SharedPreferences sharedpreferences;
    private SharedPreferences latlongSF;
    private ProgressDialog progressDialog;
    private List<MapChat> mMapChats = new ArrayList<MapChat>();
    private String receiver, previewImageUrl, appLinkUrl;
    private MobileServiceTable<MapChat> mMapChatTable;
    public int zoom = 17;
    private Dialog dialog;
    EditText editedAge;
    Button btnOK;
    ProgressDialog pDialog;
//    private SensorManager mSensorManager;
//    private Sensor mAccelerometer;
    private Location myLoc;
    AlertDialogManager alert = new AlertDialogManager();
    GooglePlaces googlePlaces;
    PlacesList nearPlaces;
    ListView lv;
    ArrayList<HashMap<String, String>> placesListItems = new ArrayList<HashMap<String, String>>();
    PlaceDetails placeDetails;
    String reference;
    // KEY Strings
    public static String KEY_REFERENCE = "reference"; // id of the place
    public static String KEY_NAME = "name"; // name of the place
    public static String KEY_VICINITY = "vicinity"; // Place area name
    MapChat mapChat = new MapChat();
    private Button button_show_map;
    public static String types;
    private boolean isFabClicked = true;
    public static double newLat;
    public static double newLon;
    public static boolean hasNew = false;
    public static String newMSG = "";
    public static String senderBeepeerName = "";
//    private MediaRouteButton mProgressBar;
    ProgressBar mProgressBar;
    private CameraPosition cameraPosition;
    private StringBuilder query = new StringBuilder();
    private InputStream inputStream = null;
    private String json = "";
    public static ArrayList<NearBy> nearbyPlaces;
//    private ArrayList<NearBy> mNearbyPlaces = null;
    private List<Results> nearBy = new ArrayList<Results>();
    public static PlaceAdapter placeAdapter;
    public static BitmapDescriptor nearByPlaceIcon;
    FloatingActionButton hospital, school, restaurant, atm, shopping, hotel, water, police, road_accident, construction, road_block;
    FloatingActionsMenu head1, head2, head3;
    private SharedPreferences app;
    private String myMarker;
    private MarkerOptions myselfMarker;
    private Map<String, MarkerOptions> mMarkers = new ConcurrentHashMap<String, MarkerOptions>();
    private RelativeLayout shadow;
    private Toolbar toolbar;
    private BitmapDescriptor policeIcon;
    private BitmapDescriptor road_accidentIcon, road_blockIcon;
    private BitmapDescriptor waterIcon;
    private BitmapDescriptor constructionIcon;
    private float mDeclination;
    private float[] mRotationMatrix = new float[16];
    private BearingToNorthProvider mBearingProvider;
    private float bearing = 0;
    private int i=1;
    private boolean isDragged = false;
    private LocationManager locationManager;
    private Criteria criteria;
    private String provider;
    private Marker marker;
    private final boolean[] isStopped = {false};
    private String themeColor;
    private FloatingActionButton actionD, actionE, actionF;
    private RelativeLayout outterRelativeLayout, innerRelativeLayout;
    private View normalView;
    private LinearLayout linearLayoutTitle;
    private TextView textViewTitle;
    public static  Bitmap backBitmap = null, bmOverlay = null;
//    private LocationService locationListener;
//    private SensorManager sensorManager;

    public HomeFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.map_fragment, container, false);
//            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (InflateException e) {
        /* map is already there, just return view as it is */
        }
        toolbar = MapsActivity.toolbar;
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        setHasOptionsMenu(true);
//        mSensorManager = (SensorManager)getActivity().getSystemService(Context.SENSOR_SERVICE);
//        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
//        mMegnaticField = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        sharedpreferences = getActivity().getSharedPreferences(getResources().getString(R.string.id), Context.MODE_PRIVATE);
        beepeerID = sharedpreferences.getString("beepeer", "");
        latlongSF = getActivity().getSharedPreferences(getResources().getString(R.string.latlon), Context.MODE_PRIVATE);
        app = getActivity().getSharedPreferences("app", Context.MODE_PRIVATE);
        myMarker = app.getString("marker", "");
        themeColor = app.getString("color", "");
        latLng = new LatLng(Double.parseDouble(latlongSF.getString(getResources().getString(R.string.lat), "23.7245072")), Double.parseDouble(latlongSF.getString(getResources().getString(R.string.lon), "90.4030656")));
        shadow = (RelativeLayout) view.findViewById(R.id.shadow);
        mBearingProvider = new BearingToNorthProvider(getActivity().getApplicationContext());
        mBearingProvider.setChangeEventListener(this);
        LocationService = new LocationService(getActivity());

        delayProvider();
        isStopped[0] = false;

        bringTOFront();
        initHead();
        initTrafficUpdate();
        initZoom();
        initPlaces();
        initExtraService();
        initMobileService();
        return view;
    }

    private void delayProvider() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 5s = 5000ms
                mBearingProvider.start();
            }
        }, 5000);
    }

    private void initHead() {
        head1 = (FloatingActionsMenu) view.findViewById(R.id.head1);
        head2 = (FloatingActionsMenu) view.findViewById(R.id.head2);
        head3 = (FloatingActionsMenu) view.findViewById(R.id.head3);

        head1.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                shadow.setBackgroundColor(0x4DFFFFFF);
                head2.collapse();
                head3.collapse();
                head2.setVisibility(View.GONE);
                head3.setVisibility(View.GONE);
                if(actionD != null || actionE != null || actionF != null){
                    actionD.setVisibility(View.GONE);
                    actionE.setVisibility(View.GONE);
                    actionF.setVisibility(View.GONE);
                }
            }

            @Override
            public void onMenuCollapsed() {
                shadow.setBackgroundColor(0x00FFFFFF);
                head2.setVisibility(View.VISIBLE);
                head3.setVisibility(View.VISIBLE);
                actionD.setVisibility(View.VISIBLE);
                actionE.setVisibility(View.VISIBLE);
                actionF.setVisibility(View.VISIBLE);
            }
        });

        head2.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                shadow.setBackgroundColor(0x4DFFFFFF);
                head1.collapse();
                head3.collapse();
                head1.setVisibility(View.GONE);
                head3.setVisibility(View.GONE);
                if(actionD != null || actionE != null || actionF != null){
                    actionD.setVisibility(View.GONE);
                    actionE.setVisibility(View.GONE);
                    actionF.setVisibility(View.GONE);
                }
            }

            @Override
            public void onMenuCollapsed() {
                shadow.setBackgroundColor(0x00FFFFFF);
                head1.setVisibility(View.VISIBLE);
                head3.setVisibility(View.VISIBLE);
                actionD.setVisibility(View.VISIBLE);
                actionE.setVisibility(View.VISIBLE);
                actionF.setVisibility(View.VISIBLE);
            }
        });

        head3.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                mBearingProvider.stop();
                shadow.setBackgroundColor(0x4DFFFFFF);
                head2.collapse();
                head1.collapse();
                head2.setVisibility(View.GONE);
                head1.setVisibility(View.GONE);
                if(actionD != null || actionE != null || actionF != null){
                    actionD.setVisibility(View.GONE);
                    actionE.setVisibility(View.GONE);
                    actionF.setVisibility(View.GONE);
                }
                if(mMap != null){
                    if(latitude != 0 && longitude != 0){
                        cameraPosition = new CameraPosition.Builder()
                                .target(new LatLng(latitude, longitude)) // Sets the center of the map to
                                .zoom(zoom)                   // Sets the zoom
                                .bearing(bearing) // Sets the orientation of the camera to east
                                .build();    // Creates a CameraPosition from the builder
                        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    }
                }
//                toolbar.animate().translationY(-toolbar.getBottom()).setInterpolator(new AccelerateInterpolator()).start();
            }

            @Override
            public void onMenuCollapsed() {
//                rel1.setBackgroundColor(0x00FFFFFF);
//                toolbar.animate().translationY(0).setInterpolator(new DecelerateInterpolator()).start();
                head2.setVisibility(View.VISIBLE);
                head1.setVisibility(View.VISIBLE);
                actionD.setVisibility(View.VISIBLE);
                actionE.setVisibility(View.VISIBLE);
                actionF.setVisibility(View.VISIBLE);
            }
        });
    }

    private void initExtraService() {

        police = (FloatingActionButton) view.findViewById(R.id.police);
        police.setTitle("Police");
        policeIcon = BitmapDescriptorFactory.fromResource(R.mipmap.ic_winter_12);
        police.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                collapseFab();
                if (MapsActivity.item != null) {
                    MapsActivity.item.setmAreaStatusId("7");
                    insertBeep(MapsActivity.item);
                }
                Toast.makeText(getActivity().getApplicationContext(), "Police update sent successfully", Toast.LENGTH_SHORT).show();
                getLatLong();
                if(mMap != null)
                    mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title("Warning").snippet("Police").icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_winter_12))).showInfoWindow();
            }
        });

        water  = (FloatingActionButton) view.findViewById(R.id.water);
        water.setTitle("Water log");
        waterIcon = BitmapDescriptorFactory.fromResource(R.mipmap.ic_winter_5);
        water.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                collapseFab();
                if (MapsActivity.item != null) {
                    MapsActivity.item.setmAreaStatusId("6");
                    insertBeep(MapsActivity.item);
                }
                Toast.makeText(getActivity().getApplicationContext(), "Water lock update sent successfully", Toast.LENGTH_SHORT).show();
                getLatLong();
                if(mMap != null)
                    mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title("Warning").snippet("Water Lock").icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_winter_5))).showInfoWindow();
            }
        });

        construction = (FloatingActionButton) view.findViewById(R.id.construction);
        construction.setTitle("Construction");
        constructionIcon = BitmapDescriptorFactory.fromResource(R.mipmap.ic_winter_14);
        construction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                collapseFab();
                if (MapsActivity.item != null) {
                    MapsActivity.item.setmAreaStatusId("5");
                    insertBeep(MapsActivity.item);
                }
                Toast.makeText(getActivity().getApplicationContext(), "Construction update sent successfully", Toast.LENGTH_SHORT).show();
                getLatLong();
                if(mMap != null)
                    mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title("Warning").snippet("Construction").icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_winter_14))).showInfoWindow();
            }
        });

        road_accident = (FloatingActionButton) view.findViewById(R.id.road_accident);
        road_accident.setTitle("Road Accident");
        road_accidentIcon = BitmapDescriptorFactory.fromResource(R.mipmap.ic_winter_4);
        road_accident.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                collapseFab();
                if (MapsActivity.item != null) {
                    MapsActivity.item.setmAreaStatusId("4");
                    insertBeep(MapsActivity.item);
                }
                getLatLong();
                Toast.makeText(getActivity().getApplicationContext(), "Road accident update sent successfully", Toast.LENGTH_SHORT).show();
                if(mMap != null)
                    mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title("Warning").snippet("Road Accident").icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_winter_4))).showInfoWindow();
            }
        });

        road_block = (FloatingActionButton) view.findViewById(R.id.road_block);
        road_block.setTitle("Road Block");
        road_blockIcon = BitmapDescriptorFactory.fromResource(R.mipmap.ic_winter_15);
        road_block.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                collapseFab();
                if (MapsActivity.item != null) {
                    MapsActivity.item.setmAreaStatusId("8");
                    insertBeep(MapsActivity.item);
                }
                Toast.makeText(getActivity().getApplicationContext(), "Road block update sent successfully", Toast.LENGTH_SHORT).show();
                getLatLong();
                if(mMap != null)
                    mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title("Warning").snippet("Road Block").icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_winter_15))).showInfoWindow();
            }
        });
    }

    private void initMobileService() {
        try {
//            mClient = new MobileServiceClient("https://beepeer.azure-mobile.net/","TBpzCbTsUcdYjPiGBdZunJfjRnhEiR93",this).withFilter(new ProgressFilter());
//            mClient = new MobileServiceClient("https://beepeer.azure-mobile.net/",
//                    "TBpzCbTsUcdYjPiGBdZunJfjRnhEiR93", getActivity());
            mClient = new MobileServiceClient(
                    "https://beepeer.azure-mobile.net/",
                    "TBpzCbTsUcdYjPiGBdZunJfjRnhEiR93",
                    getActivity()).withFilter(new ProgressFilter());
            mBeepTable = mClient.getTable(Beep.class);
            mMapChatTable = mClient.getTable(MapChat.class);
//            new setMarker().execute();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private void initTrafficUpdate() {
        FloatingActionButton actionB = (FloatingActionButton) view.findViewById(R.id.action_b);
        actionB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                collapseFab();
                if (MapsActivity.item != null) {
                    MapsActivity.item.setmAreaStatusId("1");
                    insertBeep(MapsActivity.item);
                }
                getLatLong();
                if(mMap != null){
                    mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude))
                            .title("Beepeer").snippet("Easy").icon(BitmapDescriptorFactory.fromResource
                            (R.mipmap.ic_yellow_with_arrow))).showInfoWindow();
//                    mMap.addCircle(new CircleOptions().center(new LatLng(latitude, longitude)).radius(10f).fillColor(getResources().getColor(R.color.yellow)).strokeColor(getResources().getColor(R.color.yellowDrak)));
                }

                Toast.makeText(getActivity().getApplicationContext(), "Traffic update sent successfully", Toast.LENGTH_SHORT).show();
//                changeShade();
            }
        });

        FloatingActionButton actionA = (FloatingActionButton) view.findViewById(R.id.action_a);
        actionA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                collapseFab();
                if (MapsActivity.item != null) {
                    MapsActivity.item.setmAreaStatusId("0");
                    insertBeep(MapsActivity.item);
                }
                getLatLong();
                if(mMap != null){
                    mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude))
                            .title("Beepeer").snippet("Go").icon(BitmapDescriptorFactory.fromResource
                            (R.mipmap.ic_green_with_arrow))).showInfoWindow();
//                    mMap.addCircle(new CircleOptions().center(new LatLng(latitude, longitude)).radius(10f).fillColor(getResources().getColor(R.color.green)).strokeColor(getResources().getColor(R.color.greenDark)));
                }
                Toast.makeText(getActivity().getApplicationContext(), "Traffic update sent successfully", Toast.LENGTH_SHORT).show();
            }
        });

        FloatingActionButton actionC = (FloatingActionButton) view.findViewById(R.id.action_c);
        actionC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                collapseFab();
                if (MapsActivity.item != null) {
                    MapsActivity.item.setmAreaStatusId("2");
                    insertBeep(MapsActivity.item);
                }
                getLatLong();
                if(mMap != null){
                    mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude))
                            .title("Beepeer").snippet("Busy").icon(BitmapDescriptorFactory.fromResource
                            (R.mipmap.ic_red_with_arrow))).showInfoWindow();
//                    mMap.addCircle(new CircleOptions().center(new LatLng(latitude, longitude)).radius(10f).fillColor(getResources().getColor(R.color.red)).strokeColor(getResources().getColor(R.color.redDrak)));
                }
                Toast.makeText(getActivity().getApplicationContext(), "Traffic update sent successfully", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initZoom() {
        actionD = (FloatingActionButton) view.findViewById(R.id.action_d);
        actionD.setTitle("Zoom In");
        actionD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mMap != null) {
                    mBearingProvider.stop();
                    mMap.animateCamera(CameraUpdateFactory.zoomIn());
                }
            }
        });

        actionE = (FloatingActionButton) view.findViewById(R.id.action_e);
        actionE.setTitle("Zoom Out");
        actionE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mMap != null) {
                    mBearingProvider.stop();
                    mMap.animateCamera(CameraUpdateFactory.zoomOut());
                }
            }
        });

        actionF = (FloatingActionButton) view.findViewById(R.id.action_f);
        actionF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getLatLong();
                if (mMap != null) {
                    zoom = 17;
                    cameraPosition = new CameraPosition.Builder()
                            .target(new LatLng(latitude, longitude)) // Sets the center of the map to
                            .zoom(zoom)                   // Sets the zoom
                            .bearing(0) // Sets the orientation of the camera to east
                            .tilt(90)    // Sets the tilt of the camera to 30 degrees
                            .build();    // Creates a CameraPosition from the builder
                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    delayProvider();
                    isStopped[0] = false;
                }
            }
        });
    }

    private void initPlaces() {
        types = "";
        hospital = (FloatingActionButton) view.findViewById(R.id.hospital);
        hospital.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                collapseFab();
                types = "hospital";
                nearByPlaceIcon = BitmapDescriptorFactory.fromResource(R.mipmap.ic_winter_16);
                new LoadNearBy().execute(types);
            }
        });

        school= (FloatingActionButton) view.findViewById(R.id.school);
        school.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                collapseFab();
                nearByPlaceIcon = BitmapDescriptorFactory.fromResource(R.mipmap.ic_winter_11);
                types = "school";
                new LoadNearBy().execute(types);
            }
        });

        restaurant = (FloatingActionButton) view.findViewById(R.id.restaurant);
        restaurant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                collapseFab();
                nearByPlaceIcon = BitmapDescriptorFactory.fromResource(R.mipmap.ic_winter_9);
                types = "restaurant";
                new LoadNearBy().execute(types);
            }
        });

//        bank= (FloatingActionButton) view.findViewById(R.id.bank);
//        bank.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                collapseFab();
//                nearByPlaceIcon = BitmapDescriptorFactory.fromResource(R.mipmap.ic_autumn_7);
//                types = "bank";
//                new LoadNearBy().execute(types);
//            }
//        });

        atm= (FloatingActionButton) view.findViewById(R.id.atm);
        atm.setTitle("ATMs");
        atm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                collapseFab();
                nearByPlaceIcon = BitmapDescriptorFactory.fromResource(R.mipmap.ic_winter_10);
                types = "atm";
                new LoadNearBy().execute(types);
            }
        });

        shopping= (FloatingActionButton) view.findViewById(R.id.shopping);
        shopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                collapseFab();
                nearByPlaceIcon = BitmapDescriptorFactory.fromResource(R.mipmap.ic_winter_2);
                types = "shopping_mall";
                new LoadNearBy().execute(types);
            }
        });

        hotel= (FloatingActionButton) view.findViewById(R.id.hotel);
        hotel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                collapseFab();
                nearByPlaceIcon = BitmapDescriptorFactory.fromResource(R.mipmap.ic_winter_9);
                types = "food";
                new LoadNearBy().execute(types);
            }
        });

    }

    private void collapseFab() {
        head1.collapse();
        head2.collapse();
        head3.collapse();
    }

    @Override
    public void onBearingChanged(double bearing) {
        this.bearing = (float) bearing;

        if(NearByPlacesFragment.selectedItemLatitude != 0.0 && NearByPlacesFragment.selectedItemLongitude != 0.0){
            Log.e("NearbyInOnBearing", "yes");
            mBearingProvider.stop();
            NearByPlacesFragment.selectedItemLatitude = 0;
            NearByPlacesFragment.selectedItemLongitude = 0;
            isStopped[0] = true;
        }
        if(hasNew){
            mBearingProvider.stop();
            isStopped[0] = true;
        }

        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
//                Log.e("camerachanged", "yes");
                if(isItFar()){
                    mBearingProvider.stop();
                    isDragged = true;
                    isStopped[0] = true;
                }
            }
        });
//        Log.e("isDragged", isDragged+"");
        if(mMap != null){
            if(!isStopped[0]){
//            Log.e("myLocatiton", "myLocatiton");
                setUpCamera();
            }
        }
    }

    private boolean isItFar() {
//        cameraPosition.target.latitude  latitude && cameraPosition.target.longitude != longitude

        Location locationA = new Location("new Location");
        locationA.setLatitude(cameraPosition.target.latitude);
        locationA.setLongitude(cameraPosition.target.longitude);
        Location locationB = new Location("old Location");
        locationB.setLatitude(latitude);
        locationB.setLongitude(longitude);
        Log.e("locations","new:"+ locationA.getLatitude()+" "+locationA.getLongitude()+" old:"+locationB.getLatitude()+" "+locationA.getLongitude());
        float distance = locationA.distanceTo(locationB) / 1000;
        Log.e("isItFar", distance+"");
        if(distance >= 0.5f)
            return true;
        return false;
    }

    private void locationData(){
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, true);
        Location location = locationManager
                .getLastKnownLocation(provider);

        if (location != null) {
            onLocationChanged(location);
        }

        locationManager
                .requestLocationUpdates(provider, 20000, 0, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        if(marker != null){
            marker.remove();

        }
        getLatLong();
        LatLng latLng = new LatLng(latitude, longitude);
//        Log.e("onLocationChanged", latitude+" "+longitude);

        if (myMarker.equals("man")) {
            myselfMarker = new MarkerOptions().position(latLng).title("Me").snippet(MapsActivity.beepeerName).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher_man)).anchor(0.5f,0.5f);
            marker = mMap.addMarker(myselfMarker);

        }
        else if(myMarker.equals("woman")){
            myselfMarker = new MarkerOptions().position(latLng).title("Me").snippet(MapsActivity.beepeerName).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher_woman));
            marker = mMap.addMarker(myselfMarker);

        }
        else if (myMarker.equals("car")){
            myselfMarker = new MarkerOptions().position(latLng).title("Me").snippet(MapsActivity.beepeerName).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher_car));
            marker = mMap.addMarker(myselfMarker);

        }
        else if (myMarker.equals("cng")){
            myselfMarker = new MarkerOptions().position(latLng).title("Me").snippet(MapsActivity.beepeerName).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher_cng));
            marker = mMap.addMarker(myselfMarker);

        }
        else {
            myselfMarker = new MarkerOptions().position(latLng).title("Me").snippet(MapsActivity.beepeerName).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher_rick));
            marker = mMap.addMarker(myselfMarker);

        }
        if(!(NearByPlacesFragment.selectedItemLatitude != 0.0 && NearByPlacesFragment.selectedItemLongitude != 0.0)){
            if(!hasNew){
                Log.e("markerChangedOnLocation","true");
                // Showing the current location in Google Map
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

                // Zoom in the Google Map
                mMap.animateCamera(CameraUpdateFactory.zoomTo(17));
            }
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public class LoadNearBy extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity().getApplicationContext());
            pDialog.setTitle("Loading...");
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String type = params[0];
            Log.e("typeInDoin", type);
            Places places;

            query.append("https://maps.googleapis.com/maps/api/place/nearbysearch/xml?");
            query.append("location=" +  latitude + "," + longitude + "&");
            query.append("radius=" + RADIUS_METER + "&");
            query.append("types=" + types + "&");
            query.append("sensor=true&"); //Must be true if queried from a device with GPS
            query.append("key=" + APIKEY);

            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response;
            String responseString = null;
            try {
                response = httpclient.execute(new HttpGet(String.valueOf(query)));
                StatusLine statusLine = response.getStatusLine();
                if(statusLine.getStatusCode() == HttpStatus.SC_OK){
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    response.getEntity().writeTo(out);
                    out.close();
                    responseString = out.toString();
                } else {
                    //Closes the connection.
                    response.getEntity().getContent().close();
                    throw new IOException(statusLine.getReasonPhrase());
                }
            } catch (ClientProtocolException e) {
                Log.e("ERROR", e.getMessage());
            } catch (IOException e) {
                Log.e("ERROR", e.getMessage());
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            pDialog.dismiss();
            nearbyPlaces = new ArrayList<NearBy>();

            try {

                Document xmlResult = loadXMLFromString(result);
                NodeList nodeList =  xmlResult.getElementsByTagName("result");

                for(int i = 0, length = nodeList.getLength(); i < length; i++) {

                    Node node = nodeList.item(i);

                    if(node.getNodeType() == Node.ELEMENT_NODE) {
                        Element nodeElement = (Element) node;
                        NearBy place = new NearBy();

                        Node name = nodeElement.getElementsByTagName("name").item(0);
                        Node vicinity = nodeElement.getElementsByTagName("vicinity").item(0);
                        Node rating = nodeElement.getElementsByTagName("rating").item(0);
                        Node reference = nodeElement.getElementsByTagName("reference").item(0);
                        Node id = nodeElement.getElementsByTagName("id").item(0);
                        Node geometryElement = nodeElement.getElementsByTagName("geometry").item(0);

                        NodeList locationElement = geometryElement.getChildNodes();

                        Element latLngElem = (Element) locationElement.item(1);

                        Node lat = latLngElem.getElementsByTagName("lat").item(0);
                        Node lng = latLngElem.getElementsByTagName("lng").item(0);

                        float[] geometry =  {Float.valueOf(lat.getTextContent()),
                                Float.valueOf(lng.getTextContent())};

                        int typeCount = nodeElement.getElementsByTagName("type").getLength();

                        String[] types = new String[typeCount];

                        for(int j = 0; j < typeCount; j++) {
                            types[j] = nodeElement.getElementsByTagName("type").item(j).getTextContent();
                        }

                        place.setVicinity(vicinity.getTextContent());
                        place.setId(id.getTextContent());
                        place.setName(name.getTextContent());

                        if(null == rating) {
                            place.setRating(0.0f);
                        } else {
                            place.setRating(Float.valueOf(rating.getTextContent()));
                        }

                        place.setReference(reference.getTextContent());
                        place.setGeometry(geometry);
                        place.setTypes(types);
//                        mNearbyPlaces.add(place);
                        nearbyPlaces.add(place);
                        Log.e("NearByPaces", place.getName()+" "+place.getVicinity());
                    }
                }
                placeAdapter = new PlaceAdapter(getActivity().getApplicationContext(), R.layout.httptestrow, nearbyPlaces);
                DialogFragment dialogFragment = NearByPlacesFragment.newInstance();
                dialogFragment.setCancelable(true);
//                dialogFragment.getActivity().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                mBearingProvider.stop();
                dialogFragment.show(getActivity().getFragmentManager(), "Near by place");
            } catch (Exception e) {
//                Log.e("ERROR", e.);
                e.printStackTrace();
            }
        }
    }

    public static Document loadXMLFromString(String xml) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        InputSource is = new InputSource(new StringReader(xml));

        return builder.parse(is);
    }


    private void bringTOFront() {
        rel1 = (RelativeLayout) view.findViewById(R.id.rel1);
        rel1.bringToFront();

    }

    public void showSettingsAlert(String provider) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity().getApplicationContext());

        alertDialog.setTitle(provider + " SETTINGS");

        alertDialog
                .setMessage(provider + " is not enabled! Want to go to settings menu?");

        alertDialog.setPositiveButton("Settings",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        getActivity().getApplicationContext().startActivity(intent);
                    }
                });

        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_refresh:
                clearMap();
                setUpCamera();
                markerSetUp(getBeebServiceData());
                Toast.makeText(getActivity().getApplicationContext(), "Data is refreshed", Toast.LENGTH_SHORT).show();
                return true;
            default:
                break;
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onCreate(MapsActivity.bundle);
        super.onResume();

//        initMobileService();
        MapsActivity.bottomLayout.setVisibility(View.VISIBLE);
        setUpMapIfNeeded();
        clearMap();
        delayProvider();
        setUpCamera();
        markerSetUp(getBeebServiceData());
        locationData();
        fabIconChange();
//        captureScreen();
    }

    private void setUpCamera() {
//        Log.e("setUpCamera", "true");
        if(hasNew){
            Log.e("hasNew","true");
            cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(newLat, newLon))
                    .zoom(zoom)
                    .bearing(bearing)
                    .tilt(90)
                    .build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//            new MarkerOptions().position(new LatLng(latitude, longitude)).title("Marker")
// .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_me))
            MarkerOptions msgMarker = new MarkerOptions();
            msgMarker.position(new LatLng(newLat, newLon));
            msgMarker.title("Message");
            msgMarker.snippet(newMSG);
            msgMarker.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_msg));
            mMap.addMarker(msgMarker).showInfoWindow();
            mMarkers.put("Message", msgMarker);
        }
        else if(NearByPlacesFragment.selectedItemLatitude != 0.0 && NearByPlacesFragment.selectedItemLongitude != 0.0){
        Log.e("nearbyInHome", NearByPlacesFragment.selectedItemLatitude+" "+NearByPlacesFragment.selectedItemLongitude);
            cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(NearByPlacesFragment.selectedItemLatitude, NearByPlacesFragment.selectedItemLongitude))
                    .zoom(17)
                    .bearing(bearing)
                    .build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            MarkerOptions nearByplaceMarker = new MarkerOptions();
            nearByplaceMarker.position(new LatLng(NearByPlacesFragment.selectedItemLatitude, NearByPlacesFragment.selectedItemLongitude));
            nearByplaceMarker.title(NearByPlacesFragment.nearbyPlaceName);
            nearByplaceMarker.snippet(NearByPlacesFragment.nearbyPlaceLocation);
            Log.e("place name: ", NearByPlacesFragment.nearbyPlaceName + "");
            nearByplaceMarker.icon(nearByPlaceIcon);
            mMap.addMarker(nearByplaceMarker).showInfoWindow();

            LatLng origin = new LatLng(latitude, longitude);
            LatLng dest = new LatLng(NearByPlacesFragment.selectedItemLatitude, NearByPlacesFragment.selectedItemLongitude);
            String url = getDirectionsUrl(origin, dest);

            DownloadTask downloadTask = new DownloadTask();

            // Start downloading json data from Google Directions API
            downloadTask.execute(url);
            mMarkers.put(NearByPlacesFragment.nearbyPlaceName, nearByplaceMarker);
//            NearByPlacesFragment.selectedItemLatitude = 0;
//            NearByPlacesFragment.selectedItemLongitude = 0;
        }
        else {
//            Log.e("noNew","true");
            getLatLong(); //this may cause effect for accelerometer
            cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(latitude, longitude)) // Sets the center of the map to
                    .zoom(zoom)                   // Sets the zoom
                    .bearing(bearing) // Sets the orientation of the camera to east
                    .tilt(90)    // Sets the tilt of the camera to 30 degrees
                    .build();    // Creates a CameraPosition from the builder
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    private void clearMap() {
        mMap.clear();
    }

    private void fabIconChange() {
        SharedPreferences values = getActivity().getSharedPreferences("app", Context.MODE_PRIVATE);
        String theme = values.getString("color", "");

        if(theme.equals("blue")){
            hospital.setImageDrawable(getResources().getDrawable(R.mipmap.ic_autumn_16));
            school.setImageDrawable(getResources().getDrawable(R.mipmap.ic_autumn_11));
            restaurant.setImageDrawable(getResources().getDrawable(R.mipmap.ic_autumn_9));
//            bank.setImageDrawable(getResources().getDrawable(R.mipmap.ic_autumn_7));
            atm.setImageDrawable(getResources().getDrawable(R.mipmap.ic_autumn_10));
            shopping.setImageDrawable(getResources().getDrawable(R.mipmap.ic_autumn_2));
            hotel.setImageDrawable(getResources().getDrawable(R.mipmap.ic_autumn_8));

            water.setImageDrawable(getResources().getDrawable(R.mipmap.ic_autumn_5));
            police.setImageDrawable(getResources().getDrawable(R.mipmap.ic_autumn_12));
            road_accident.setImageDrawable(getResources().getDrawable(R.mipmap.ic_autumn_4));
            construction.setImageDrawable(getResources().getDrawable(R.mipmap.ic_autumn_14));
            road_block.setImageDrawable(getResources().getDrawable(R.mipmap.ic_autumn_15));
        }
        else  if(theme.equals("yellow")){
            hospital.setImageDrawable(getResources().getDrawable(R.mipmap.ic_summer_16));
            school.setImageDrawable(getResources().getDrawable(R.mipmap.ic_summer_11));
            restaurant.setImageDrawable(getResources().getDrawable(R.mipmap.ic_summer_9));
//            bank.setImageDrawable(getResources().getDrawable(R.mipmap.ic_summer_7));
            atm.setImageDrawable(getResources().getDrawable(R.mipmap.ic_summer_10));
            shopping.setImageDrawable(getResources().getDrawable(R.mipmap.ic_summer_2));
            hotel.setImageDrawable(getResources().getDrawable(R.mipmap.ic_summer_8));

            water.setImageDrawable(getResources().getDrawable(R.mipmap.ic_summer_5));
            police.setImageDrawable(getResources().getDrawable(R.mipmap.ic_summer_12));
            road_accident.setImageDrawable(getResources().getDrawable(R.mipmap.ic_summer_4));
            construction.setImageDrawable(getResources().getDrawable(R.mipmap.ic_summer_14));
            road_block.setImageDrawable(getResources().getDrawable(R.mipmap.ic_summer_15));
        }
        else if(theme.equals("red")){
            hospital.setImageDrawable(getResources().getDrawable(R.mipmap.ic_spring_16));
            school.setImageDrawable(getResources().getDrawable(R.mipmap.ic_spring_11));
            restaurant.setImageDrawable(getResources().getDrawable(R.mipmap.ic_spring_9));
//            bank.setImageDrawable(getResources().getDrawable(R.mipmap.ic_spring_7));
            atm.setImageDrawable(getResources().getDrawable(R.mipmap.ic_spring_10));
            shopping.setImageDrawable(getResources().getDrawable(R.mipmap.ic_spring_2));
            hotel.setImageDrawable(getResources().getDrawable(R.mipmap.ic_spring_8));

            water.setImageDrawable(getResources().getDrawable(R.mipmap.ic_spring_5));
            police.setImageDrawable(getResources().getDrawable(R.mipmap.ic_spring_12));
            road_accident.setImageDrawable(getResources().getDrawable(R.mipmap.ic_spring_4));
            construction.setImageDrawable(getResources().getDrawable(R.mipmap.ic_spring_14));
            road_block.setImageDrawable(getResources().getDrawable(R.mipmap.ic_spring_15));
        }
        else {
            hospital.setImageDrawable(getResources().getDrawable(R.mipmap.ic_winter_16));
            school.setImageDrawable(getResources().getDrawable(R.mipmap.ic_winter_11));
            restaurant.setImageDrawable(getResources().getDrawable(R.mipmap.ic_winter_9));
//            bank.setImageDrawable(getResources().getDrawable(R.mipmap.ic_winter_7));
            atm.setImageDrawable(getResources().getDrawable(R.mipmap.ic_winter_10));
            shopping.setImageDrawable(getResources().getDrawable(R.mipmap.ic_winter_2));
            hotel.setImageDrawable(getResources().getDrawable(R.mipmap.ic_winter_8));

            water.setImageDrawable(getResources().getDrawable(R.mipmap.ic_winter_5));
            police.setImageDrawable(getResources().getDrawable(R.mipmap.ic_winter_12));
            road_accident.setImageDrawable(getResources().getDrawable(R.mipmap.ic_winter_4));
            construction.setImageDrawable(getResources().getDrawable(R.mipmap.ic_winter_14));
            road_block.setImageDrawable(getResources().getDrawable(R.mipmap.ic_winter_15));
        }
    }

    private void snapShot() {
        mMap.snapshot(new GoogleMap.SnapshotReadyCallback() {
            @Override
            public void onSnapshotReady(Bitmap bitmap) {

            }
        });
    }

    public static List<Beep> getBeebServiceData() {


        new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    results = mBeepTable.select("areaStatusId", "longitude", "latitude").execute().get();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
        return results;
    }

    private void markerSetUp(List<Beep> aVoid) {

        if (aVoid != null) {
            for (Beep result : aVoid) {
                MarkerOptions extraServiceMarkerOptions;
                areaId = Integer.parseInt(result.getmAreaStatusId());
                areaName = result.getmAreaName();
                markLatitude = Double.parseDouble(result.getmLatitude());
                markLongitude = Double.parseDouble(result.getmLongitude());

                //                mMarkersHashMap = new HashMap<Marker, MyMarker>();
                if (areaId == 0) {
                    mMap.addMarker(new MarkerOptions().position(new LatLng(markLatitude, markLongitude)).title("Beepeer").snippet("Easy").icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_green_with_arrow))).showInfoWindow();
//                    mMap.addCircle(new CircleOptions().center(new LatLng(markLatitude, markLongitude)).radius(10f).fillColor(getResources().getColor(R.color.green)).strokeColor(getResources().getColor(R.color.greenDark)));
                    Log.e("Map", "Started map5");
                    //                    mMap.setInfoWindowAdapter(new MarkerInfoWindowAdapter());
                } else if (areaId == 1) {
                    mMap.addMarker(new MarkerOptions().position(new LatLng(markLatitude, markLongitude)).title("Beepeer").snippet("Medium").icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_yellow_with_arrow))).showInfoWindow();
//                    mMap.addCircle(new CircleOptions().center(new LatLng(markLatitude, markLongitude)).radius(10f).fillColor(getResources().getColor(R.color.yellow)).strokeColor(getResources().getColor(R.color.yellowDrak)));
                    //                    mMap.setInfoWindowAdapter(new MarkerInfoWindowAdapter());
                } else if (areaId == 2) {
                    mMap.addMarker(new MarkerOptions().position(new LatLng(markLatitude, markLongitude)).title("Beepeer").snippet("Busy").icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_red_with_arrow))).showInfoWindow();
//                    mMap.addCircle(new CircleOptions().center(new LatLng(markLatitude, markLongitude)).radius(10f).fillColor(getResources().getColor(R.color.red)).strokeColor(getResources().getColor(R.color.redDrak)));
                } else if (areaId == 3) {
                    Log.e("Map", "Started map6");
                }
                else if (areaId == 4) {
                    Log.e("Map", "Started map6");
                    extraServiceMarkerOptions = new MarkerOptions();
                    extraServiceMarkerOptions.position(new LatLng(markLatitude, markLongitude));
                    extraServiceMarkerOptions.title("Road Accident");
                    extraServiceMarkerOptions.snippet(areaName);
                    extraServiceMarkerOptions.icon(road_accidentIcon);
                    mMap.addMarker(extraServiceMarkerOptions).showInfoWindow();
                }
                else if (areaId == 5) {
                    Log.e("Map", "Started map6");
                    extraServiceMarkerOptions = new MarkerOptions();
                    extraServiceMarkerOptions.position(new LatLng(markLatitude, markLongitude));
                    extraServiceMarkerOptions.title("Construction");
                    extraServiceMarkerOptions.snippet(areaName);
                    extraServiceMarkerOptions.icon(constructionIcon);
                    mMap.addMarker(extraServiceMarkerOptions).showInfoWindow();
                }
                else if (areaId == 6) {
                    Log.e("Map", "Started map6");
                    extraServiceMarkerOptions = new MarkerOptions();
                    extraServiceMarkerOptions.position(new LatLng(markLatitude, markLongitude));
                    extraServiceMarkerOptions.title("Traffic Police");
                    extraServiceMarkerOptions.snippet(areaName);
                    extraServiceMarkerOptions.icon(policeIcon);
                    mMap.addMarker(extraServiceMarkerOptions).showInfoWindow();
                }
                else if (areaId == 7) {
                    Log.e("Map", "Started map6");
                    extraServiceMarkerOptions = new MarkerOptions();
                    extraServiceMarkerOptions.position(new LatLng(markLatitude, markLongitude));
                    extraServiceMarkerOptions.title("Water Log");
                    extraServiceMarkerOptions.snippet(areaName);
                    extraServiceMarkerOptions.icon(waterIcon);
                    mMap.addMarker(extraServiceMarkerOptions).showInfoWindow();
                }
            }
        }
        else {
//            Toast.makeText(getActivity().getApplicationContext(), "Data Loading error...", Toast.LENGTH_SHORT).show();
        }

        if (myMarker.equals("man")) {
            myselfMarker = new MarkerOptions().position(new LatLng(latitude, longitude)).title("Me").snippet(MapsActivity.beepeerName).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher_man)).anchor(0.5f,0.5f);
            marker = mMap.addMarker(myselfMarker);
//            marker.showInfoWindow();
            mMarkers.put("Me", myselfMarker);
        }
        else if(myMarker.equals("woman")){
            myselfMarker = new MarkerOptions().position(new LatLng(latitude, longitude)).title("Me").snippet(MapsActivity.beepeerName).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher_woman));
            marker = mMap.addMarker(myselfMarker);
//            marker.showInfoWindow();
            mMarkers.put("Me", myselfMarker);
        }
        else if (myMarker.equals("car")){
            myselfMarker = new MarkerOptions().position(new LatLng(latitude, longitude)).title("Me").snippet(MapsActivity.beepeerName).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher_car));
            marker = mMap.addMarker(myselfMarker);
//            marker.showInfoWindow();
            mMarkers.put("Me", myselfMarker);
        }
        else if (myMarker.equals("cng")){
            myselfMarker = new MarkerOptions().position(new LatLng(latitude, longitude)).title("Me").snippet(MapsActivity.beepeerName).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher_cng));
            marker = mMap.addMarker(myselfMarker);
//            marker.showInfoWindow();
            mMarkers.put("Me", myselfMarker);
        }
        else {
            myselfMarker = new MarkerOptions().position(new LatLng(latitude, longitude)).title("Me").snippet(MapsActivity.beepeerName).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher_rick));
            marker = mMap.addMarker(myselfMarker);
//            marker.showInfoWindow();
            mMarkers.put("Me", myselfMarker);
        }
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) MapsActivity.fragmentManager
                    .findFragmentById(R.id.map))
                    .getMap();
            mMap.setBuildingsEnabled(true);
            if (mMap != null) {
                setUpMap();
            }
        } else
            setUpMap();
    }

    @Override
    public void onPause() {
        super.onPause();
//        mSensorManager.unregisterListener(this);
        mBearingProvider.stop();
        if(locationManager != null){
//            locationManager.removeUpdates(LocationService);
        }
//        sensorManager.unregisterListener(this);

        Log.e("onPause", "onPause HomeFragment");
    }

    private void setUpMap() {
        getLatLong();

        mMap = ((SupportMapFragment) MapsActivity.fragmentManager
                .findFragmentById(R.id.map)).getMap();

        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setZoomGesturesEnabled(true);
        //Map toolbar
        mMap.getUiSettings().setMapToolbarEnabled(true);
        mMap.getUiSettings().setScrollGesturesEnabled(true);
        mMap.getUiSettings().setTiltGesturesEnabled(true);
        mMap.getUiSettings().setRotateGesturesEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(true);

        mMap.setLocationSource(new CurrentLocationProvider(getActivity()));
//        mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title("Marker").icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_me)));

//        Show App bar
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                ((ActionBarActivity) getActivity()).getSupportActionBar().show();
                mBearingProvider.stop();
//                MapsActivity.relativeLayout.setVisibility(View.INVISIBLE);
            }
        });

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if(marker.getTitle().equals("Message")){
                    hasNew = false;
                    Toast.makeText(getActivity().getApplicationContext(), newMSG, Toast.LENGTH_SHORT).show();
                    dialog = new Dialog(getActivity(), R.style.Dialog);
                    dialog.setContentView(R.layout.map_chat);
                    outterRelativeLayout = (RelativeLayout) dialog.findViewById(R.id.relativeLayoutMapChatOutter);
                    innerRelativeLayout = (RelativeLayout) dialog.findViewById(R.id.relativeLayoutMapChatInner);
                    normalView = dialog.findViewById(R.id.normalView);
                    linearLayoutTitle = (LinearLayout)dialog.findViewById(R.id.linearLayoutTitle);
                    textViewTitle = (TextView)dialog.findViewById(R.id.title);
                    editedAge = (EditText) dialog.findViewById(R.id.editedAge);
                    checkThemeColor();
//                    TextView tvBeepeerName = (TextView) dialog.findViewById(R.id.tvBeepeerName);
//                    tvBeepeerName.setText(senderBeepeerName+": ");
                    TextView message = (TextView)dialog.findViewById(R.id.tvMessage);
                    message.setText(newMSG);
//        editedAge.setHint("Write message...");
                    btnOK = (Button) dialog.findViewById(R.id.btnOK);
                    btnOK.setText("SEND");
                    dialog.show();
                    btnOK.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(getActivity().getApplicationContext(), "Your message sent successfully", Toast.LENGTH_SHORT).show();
                            String msg = String.valueOf(editedAge.getText());
                            mapChat.setmAreaName(MapsActivity.cityName);
                            mapChat.setmBeepeer(beepeerID);
                            mapChat.setmLatitude(String.valueOf(latitude));
                            mapChat.setmLongitude(String.valueOf(longitude));
                            mapChat.setmMessage(msg);
                            mapChat.setmBeepeerName(MapsActivity.beepeerName);

                            Log.e("Home Fragment", beepeerID);
                            // Insert the new item
                            new AsyncTask<Void, Void, Void>() {
                                @Override
                                protected Void doInBackground(Void... params) {
                                    try {
                                        mMapChatTable.insert(mapChat).get();
                                    } catch (Exception e) {
                                    }

                                    return null;
                                }
                            }.execute();

//                            dialog.dismiss();
                            editedAge.setText("");
                            dialog.dismiss();
                        }
                    });
                }
                if(marker.getTitle().equals("Warning")){
                    marker.showInfoWindow();
                }
                else if(marker.getTitle().equals("Beepeer")){
                    marker.showInfoWindow();
                }
                else if(marker.getTitle().equals("Message")){
                    marker.showInfoWindow();
                }
                else if(marker.getTitle().equals("Road Accident")){
                    marker.showInfoWindow();
                }
                else if(marker.getTitle().equals("Construction")){
                    marker.showInfoWindow();
                }
                else if(marker.getTitle().equals("Traffic Police")){
                    marker.showInfoWindow();
                }
                else if(marker.getTitle().equals("Water Log")){
                    marker.showInfoWindow();
                }
                else if(marker.getTitle().equals(NearByPlacesFragment.nearbyPlaceName)){
                    marker.showInfoWindow();
                }
                Log.e("Message-HomeFrgmnt", newMSG+"");
                return true;
            }
        });

        //            final List<Beep> results = mBeepTable.execute().get();
    }

    private void getLatLong() {
        Location nwLocation = LocationService
                .getLocation(LocationManager.NETWORK_PROVIDER);

        if (nwLocation != null) {
            latitude = nwLocation.getLatitude();
            longitude = nwLocation.getLongitude();
            myLoc = new Location("MyLocation");
            myLoc.setLatitude(latitude);
            myLoc.setLongitude(longitude);

        } else {
            showSettingsAlert("NETWORK");
        }
    }

    private String saveBitmap(final Bitmap bitmap) {
        final String[] filePath = new String[1];
        GoogleMap.SnapshotReadyCallback callback = new GoogleMap.SnapshotReadyCallback() {

            @Override
            public void onSnapshotReady(Bitmap snapshot) {
                filePath[0] = Environment.getExternalStorageDirectory()
                        + File.separator + "screenshot.png";
                File imagePath = new File(filePath[0]);
                FileOutputStream fos;
                try {
                    fos = new FileOutputStream(imagePath);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                    fos.flush();
                    fos.close();
                } catch (FileNotFoundException e) {
                    Log.e("GREC", e.getMessage(), e);
                } catch (IOException e) {
                    Log.e("GREC", e.getMessage(), e);
                }
            }
        };

        mMap.snapshot(callback);

        return filePath[0];
    }

    private void insertBeep(Beep item) {
        mClient.getTable(Beep.class).insert(item, new TableOperationCallback<Beep>() {
            public void onCompleted(Beep entity, Exception exception, ServiceFilterResponse response) {
                if (exception == null) {
                    // Insert succeeded
                } else {
                    // Insert failed
                }
            }
        });
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        long timestamp = System.currentTimeMillis();

        LocationManager locManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        criteria.setPowerRequirement(Criteria.POWER_MEDIUM);
        criteria.setAccuracy(Criteria.ACCURACY_FINE);

        provider = locManager.getBestProvider(criteria, true);
        Location location = locManager.getLastKnownLocation(provider);

        double latitude = 0;
        double longitude = 0;
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            Log.e("onSensorChanged", latitude+" "+longitude);
        }
    }

    private void updateCamera(float bearing, CameraPosition cameraPosition) {
        CameraPosition oldPos = cameraPosition;

        if((oldPos.target.latitude != 0.0)||(oldPos.target.longitude != 0.0)){
            CameraPosition pos = CameraPosition.builder(oldPos).zoom(zoom).bearing(bearing)
                    .build();
            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(pos));
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }


    private class ProgressFilter implements ServiceFilter {

        @Override
        public ListenableFuture<ServiceFilterResponse> handleRequest(ServiceFilterRequest request, NextServiceFilterCallback nextServiceFilterCallback) {

            final SettableFuture<ServiceFilterResponse> resultFuture = SettableFuture.create();


            getActivity().runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (mProgressBar != null) mProgressBar.setVisibility(ProgressBar.VISIBLE);
                }
            });

            ListenableFuture<ServiceFilterResponse> future = nextServiceFilterCallback.onNext(request);

            Futures.addCallback(future, new FutureCallback<ServiceFilterResponse>() {
                @Override
                public void onFailure(Throwable e) {
                    resultFuture.setException(e);
                }

                @Override
                public void onSuccess(ServiceFilterResponse response) {
                    getActivity().runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            if (mProgressBar != null) mProgressBar.setVisibility(ProgressBar.GONE);
                        }
                    });

                    resultFuture.set(response);
                }
            });

            return resultFuture;
        }
    }

    private String getDirectionsUrl(LatLng origin,LatLng dest){

        // Origin of route
        String str_origin = "origin="+origin.latitude+","+origin.longitude;

        // Destination of route
        String str_dest = "destination="+dest.latitude+","+dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "mode=\"WALKING\"";

        // Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+mode+"&"+sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;

        return url;
    }

    /** A method to download json data from url */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb  = new StringBuffer();

            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
//            Log.d("Exception while downloading url", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try{
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    /** A class to parse the Google Places in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
            String distance = "";
            String duration = "";

            if(result.size()<1){
//                Toast.makeText(getBaseContext(), "No Points", Toast.LENGTH_SHORT).show();
                return;
            }

            // Traversing through all the routes
            for(int i=0;i<result.size();i++){
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for(int j=0;j<path.size();j++){
                    HashMap<String,String> point = path.get(j);

                    if(j==0){    // Get distance from the list
                        distance = (String)point.get("distance");
                        continue;
                    }else if(j==1){ // Get duration from the list
                        duration = (String)point.get("duration");
                        continue;
                    }

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(16);
                lineOptions.color(getResources().getColor(R.color.direction));
            }

            // Drawing polyline in the Google Map for the i-th route
            mMap.addPolyline(lineOptions);
        }
    }

    private void inviteFriends(){
        appLinkUrl = "https://play.google.com/store/apps/details?id=com.beepeer.bigs&ah=WxDX-Z_sYJuUD58H7O4C4WCaCOM";
//        previewImageUrl = "https://www.mydomain.com/my_invite_image.jpg";
        if (AppInviteDialog.canShow()) {
            AppInviteContent content = new AppInviteContent.Builder()
                    .setApplinkUrl(appLinkUrl)
                    .build();
            AppInviteDialog.show(getActivity(), content);
        }
    }

    private void checkThemeColor() {
        String color = app.getString("color", "");
        if(color.equals("red")){
            outterRelativeLayout.setBackgroundColor(getResources().getColor(R.color.spring_light));
            innerRelativeLayout.setBackgroundColor(getResources().getColor(R.color.spring));
            normalView.setBackgroundColor(getResources().getColor(R.color.spring));
            linearLayoutTitle.setBackgroundColor(getResources().getColor(R.color.spring));
            textViewTitle.setBackgroundColor(getResources().getColor(R.color.spring));
        }
        else if(color.equals("yellow")){
            outterRelativeLayout.setBackgroundColor(getResources().getColor(R.color.summer_light));
            innerRelativeLayout.setBackgroundColor(getResources().getColor(R.color.summer));
            normalView.setBackgroundColor(getResources().getColor(R.color.summer));
            linearLayoutTitle.setBackgroundColor(getResources().getColor(R.color.summer));
            textViewTitle.setBackgroundColor(getResources().getColor(R.color.summer));
        }
        else if(color.equals("blue")){
            outterRelativeLayout.setBackgroundColor(getResources().getColor(R.color.autumn_light));
            innerRelativeLayout.setBackgroundColor(getResources().getColor(R.color.autumn));
            normalView.setBackgroundColor(getResources().getColor(R.color.autumn));
            linearLayoutTitle.setBackgroundColor(getResources().getColor(R.color.autumn));
            textViewTitle.setBackgroundColor(getResources().getColor(R.color.autumn));
        }
        else {
            outterRelativeLayout.setBackgroundColor(getResources().getColor(R.color.winter_light));
            innerRelativeLayout.setBackgroundColor(getResources().getColor(R.color.winter));
            normalView.setBackgroundColor(getResources().getColor(R.color.winter));
            linearLayoutTitle.setBackgroundColor(getResources().getColor(R.color.winter));
            textViewTitle.setBackgroundColor(getResources().getColor(R.color.winter));
        }
    }

    public static Bitmap captureMapScreen() {
        Log.e("captureMapScreen", "captureMapScreen");
        final GoogleMap.SnapshotReadyCallback callback = new GoogleMap.SnapshotReadyCallback() {

            @Override
            public void onSnapshotReady(Bitmap snapshot) {
                try {
                    view.setDrawingCacheEnabled(true);
                    backBitmap = view.getDrawingCache();
                    bmOverlay = Bitmap.createBitmap(
                            backBitmap.getWidth(), backBitmap.getHeight(),
                            backBitmap.getConfig());
                    Canvas canvas = new Canvas(bmOverlay);
                    canvas.drawBitmap(snapshot, new Matrix(), null);
                    canvas.drawBitmap(backBitmap, 0, 0, null);
                    path = Environment.getExternalStorageDirectory()+ "/MapScreenShot"
                            + System.currentTimeMillis() + ".png";
                    FileOutputStream out = new FileOutputStream(path);
                    File imgFile = new  File(path);
                    sharedBitmap = BitmapFactory.decodeFile(path);
                    bmOverlay.compress(Bitmap.CompressFormat.PNG, 90, out);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        mMap.snapshot(callback);
        return bmOverlay;
    }
}
