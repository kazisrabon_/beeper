/*
 * Copyright (c) to srabon
 * IIT, University of Dhaka
 * kaziiit@gmail.com
 */

package com.beepeer.bigs;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.microsoft.windowsazure.notifications.NotificationsHandler;

public class MyHandler extends NotificationsHandler {
    public static final int NOTIFICATION_ID = 1;
    NotificationCompat.Builder builder;
    private Context ctx;
    private NotificationManager mNotificationManager;

    @Override
    public void onRegistered(Context context, final String gcmRegistrationId) {
        super.onRegistered(context, gcmRegistrationId);

        new AsyncTask<Void, Void, Void>() {

            protected Void doInBackground(Void... params) {
                try {
                    MapsActivity.mClient.getPush().register(gcmRegistrationId, null);
                    return null;
                } catch (Exception e) {
                    // handle error
                }
                return null;
            }
        }.execute();
    }

    @Override
    public void onReceive(Context context, Bundle bundle) {
        ctx = context;
        String nhMessage = bundle.getString("message");
        String nhLat = bundle.getString("latitude");
        String nhLon = bundle.getString("longitude");
        String nhBeepeer = bundle.getString("beeper");
        String nhReceiver = bundle.getString("receiver");
        String nhBeepeerName = bundle.getString("beepeerName");
        Log.e("MyHandler", nhMessage + " " +nhBeepeerName);
        if (nhReceiver == null) {
            if(checkDistance(Double.parseDouble(nhLat), Double.parseDouble(nhLon))){
                HomeFragment.hasNew = true;
                HomeFragment.newLat = Double.parseDouble(nhLat);
                HomeFragment.newLon = Double.parseDouble(nhLon);
                HomeFragment.newMSG = nhMessage;
                HomeFragment.senderBeepeerName = nhBeepeerName;
                if(!nhBeepeer.equals(MapsActivity.beepeerID)){
                    sendNotification(nhMessage);
                }
            }
        }
        else if(nhReceiver.equals(MapsActivity.beepeerID)){
            HomeFragment.hasNew = true;
            HomeFragment.newLat = Double.parseDouble(nhLat);
            HomeFragment.newLon = Double.parseDouble(nhLon);
            HomeFragment.newMSG = nhMessage;
            HomeFragment.senderBeepeerName = nhBeepeerName;
            sendNotification(nhMessage);
        }

        Log.e("MyHandler", nhBeepeer + " " + nhLat + " " + nhLon + " "+nhMessage+" "+nhReceiver);

//        sendNotification(nhMessage);
    }

    private boolean checkDistance(double chatLat, double chatLon) {
        LocationService nwLocation = new LocationService(ctx);
        Location myLocation = nwLocation.getLocation(LocationManager.NETWORK_PROVIDER);
        Location chatLocation = new Location("chat location");
        chatLocation.setLatitude(chatLat);
        chatLocation.setLongitude(chatLon);
        float distance = myLocation.distanceTo(chatLocation) / 1000;

        if(distance < 1){
            return true;
        }
        else {
            return false;
        }

    }

    private void sendNotification(String msg) {
        mNotificationManager = (NotificationManager)
                ctx.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = PendingIntent.getActivity(ctx, 0,
                new Intent(ctx, Splash.class), 0);
        long[] vibrate = { 0, 100, 200, 300 };

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(ctx)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("Beepeer")
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setContentText(msg)
                        .setVibrate(vibrate)
                        .setSound(Uri.parse("android.resource://"
                                + ctx.getPackageName() + "/" + R.raw.notification));

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
}
