/*
 * Copyright (c) to srabon
 * IIT, University of Dhaka
 * kaziiit@gmail.com
 */

package com.beepeer.bigs;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.beepeer.bigs.model.MapChat;
import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;

import java.util.concurrent.ExecutionException;

public class Chat extends Fragment {

    View view;
    EditText editTextChat;
    Button buttonAdd;
    String msg = new String();
    MapChat mapChat;
    private MobileServiceClient mClient;
    private MobileServiceTable<MapChat> mMapChatTable;
    private String receiver;
    SharedPreferences sharedPreferences;
    private String beepeerID;
//    public static
//    public static String

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.chat, container, false);
        } catch (InflateException e) {
        /* profile is already there, just return view as it is */
        }
        sharedPreferences = getActivity().getSharedPreferences(getResources().getString(R.string.id), Context.MODE_PRIVATE);
        beepeerID = sharedPreferences.getString("beepeer", "");
        Log.e("beeper", beepeerID);
        init();
        addItem();

        return view;
    }

    private void addItem() {
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClient == null) {
                    return;
                }
                msg = editTextChat.getText().toString().trim();
                mapChat = new MapChat();
                mapChat.setmAreaName(BeepService.cityName);
                mapChat.setmBeepeer(beepeerID);
                mapChat.setmLatitude(BeepService.latitude + "");
                mapChat.setmLongitude(BeepService.longitude + "");
                mapChat.setmMessage(msg);
                mapChat.setmReceiver(receiver);
                editTextChat.setText("");
                Log.e(">>>Chat<<", BeepService.cityName + " " + MainActivity.id + " " + msg);
                new saveInDB().execute();
            }
        });
    }

    private void init() {
        this.mClient = MainActivity.mClient;
        mMapChatTable = mClient.getTable(MapChat.class);
        editTextChat = (EditText) view.findViewById(R.id.etMsg);
        buttonAdd = (Button) view.findViewById(R.id.buttonAdd);
        Bundle bundle = this.getArguments();
        receiver = bundle.getString("receiverID");
    }

    private class saveInDB extends AsyncTask<Void, Void, Void> {


        @Override
        protected Void doInBackground(Void... params) {
            try {
                mMapChatTable.insert(mapChat).get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

            return null;
        }
    }
}
