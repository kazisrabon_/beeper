/*
 * Copyright (c) to srabon
 * IIT, University of Dhaka
 * kaziiit@gmail.com
 */

package com.beepeer.bigs.learn;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.util.Base64;
import android.util.Log;

import com.beepeer.bigs.MainActivity;
import com.beepeer.bigs.R;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Wiki extends ActionBarActivity {
    public static int position;
//    ImageButton ibNext, ibPrevious;
//    ImageView ivCapture;
    SharedPreferences values;
    SharedPreferences.Editor editor;
    private int value;
    private CustomPagerAdapter mCustomPagerAdapter;
    private ViewPager mViewPager;
    private CirclePageIndicator mIndicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_learn);

        printKeyHash();
        values = getSharedPreferences("app", Context.MODE_PRIVATE);
        value = values.getInt("wiki", 0);
        editor = values.edit();
        position = value;

        Log.e("Wiki", position + "");
        init();
        if(position == -1){
            startMain();
        }else {
//            zero();
            initPagerAdapter();
        }
    }

    private void initPagerAdapter() {
        position = 0;
        editor.putInt("wiki", position);
        editor.commit();
        mCustomPagerAdapter = new CustomPagerAdapter(Wiki.this);

        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mCustomPagerAdapter);
        mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
        mIndicator.setViewPager(mViewPager);
    }

    private void init() {
//        ibNext = (ImageButton)findViewById(R.id.ibRight);
//        ibPrevious = (ImageButton)findViewById(R.id.ibLeft);
//        ivCapture = (ImageView)findViewById(R.id.ivCapture);
    }

    private void zero(){
        position = 0;
//        ibNext.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                one();
//            }
//        });
//        ivCapture.setImageDrawable(getResources().getDrawable(R.drawable.zero));
    }

    private void one(){
        position = 1;
//        ibPrevious.setVisibility(View.VISIBLE);
//        ibPrevious.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                zero();
//            }
//        });
////        ibNext.setVisibility(View.VISIBLE);
//        ibNext.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                two();
//            }
//        });
//        ivCapture.setImageDrawable(getResources().getDrawable(R.drawable.one));
    }

    private void two(){
        position = 2;
//        ibPrevious.setVisibility(View.INVISIBLE);
//        ibPrevious.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                one();
//            }
//        });
////        ibNext.setVisibility(View.VISIBLE);
//        ibNext.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                three();
//            }
//        });
//        ivCapture.setImageDrawable(getResources().getDrawable(R.drawable.two));
    }

    private void three(){
        position = 3;
////        ibPrevious.setVisibility(View.VISIBLE);
//        ibPrevious.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                two();
//            }
//        });
////        ibNext.setVisibility(View.VISIBLE);
//        ibNext.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                four();
//            }
//        });
//        ivCapture.setImageDrawable(getResources().getDrawable(R.drawable.three));
    }

    private void four(){
        position = 4;
////        ibPrevious.setVisibility(View.VISIBLE);
//        ibPrevious.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                three();
//            }
//        });
////        ibNext.setVisibility(View.VISIBLE);
//        ibNext.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                position = -1;
//                startMain();
//            }
//        });
//        ivCapture.setImageDrawable(getResources().getDrawable(R.drawable.four));
    }

    private void five(){
        position = 5;
////        ibPrevious.setVisibility(View.VISIBLE);
//        ibPrevious.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                four();
//            }
//        });
////        ibNext.setVisibility(View.VISIBLE);
//        ibNext.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                six();
//            }
//        });
//        ivCapture.setImageDrawable(getResources().getDrawable(R.drawable.five));
    }

    private void six(){
        position = 6;
////        ibPrevious.setVisibility(View.VISIBLE);
//        ibPrevious.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                five();
//            }
//        });
////        ibNext.setVisibility(View.VISIBLE);
//        ibNext.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                position = -1;
//                startMain();
//            }
//        });
//        ivCapture.setImageDrawable(getResources().getDrawable(R.drawable.six));
    }

    private void startMain() {
        position = -1;
        editor.putInt("wiki", position);
        editor.commit();
        Intent mainIntent = new Intent(Wiki.this, MainActivity.class);
        startActivity(mainIntent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    private void printKeyHash(){
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.beepeer.bigs",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("KeyHash:", e.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.d("KeyHash:", e.toString());
        }
    }
}
