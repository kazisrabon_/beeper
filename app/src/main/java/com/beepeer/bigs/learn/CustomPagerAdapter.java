/*
 * Copyright (c) to srabon
 * IIT, University of Dhaka
 * kaziiit@gmail.com
 */

package com.beepeer.bigs.learn;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.beepeer.bigs.MainActivity;
import com.beepeer.bigs.R;

public class CustomPagerAdapter extends PagerAdapter {
    private final SharedPreferences values;
    Context mContext;
    LayoutInflater mLayoutInflater;
    int[] mResources = {
            R.drawable.zero,
            R.drawable.one,
            R.drawable.two,
            R.drawable.three,
            R.drawable.four,
    };

    public CustomPagerAdapter(Context context) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        values = mContext.getSharedPreferences("app", Context.MODE_PRIVATE);
    }

    @Override
    public int getCount() {
        return mResources.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
        imageView.setImageResource(mResources[position]);

        Button button = (Button) itemView.findViewById(R.id.button);
        if(position == 4){
            button.setVisibility(View.VISIBLE);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    values.edit().putInt("wiki", -1).commit();
                    Intent mainIntent = new Intent(mContext, MainActivity.class);
                    mContext.startActivity(mainIntent);
                }
            });
        }
        else {
            button.setVisibility(View.VISIBLE);
            button.setText("SKIP");
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    values.edit().putInt("wiki", -1).commit();
                    Intent mainIntent = new Intent(mContext, MainActivity.class);
                    mContext.startActivity(mainIntent);
                }
            });
        }


        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}
