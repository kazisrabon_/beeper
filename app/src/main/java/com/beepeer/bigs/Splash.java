/*
 * Copyright (c) to srabon
 * IIT, University of Dhaka
 * kaziiit@gmail.com
 */

package com.beepeer.bigs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;

import com.beepeer.bigs.learn.Wiki;


public class Splash extends ActionBarActivity {
    private static int SPLASH_TIME_OUT = 5000;
//    public static MobileServiceClient mClient;
//    public static MobileServiceTable<Beep> mBeepTable;
//    public static MobileServiceTable<MapChat> mMapChatTable;
//    public static MobileServiceTable<User> mUserTable;
//    public static List<Beep> results;
    private MediaPlayer mp;
    private SharedPreferences sharedpreferences;
    private String prefs;
    private LocationManager manager;
//    TextView networkStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        sharedpreferences = getSharedPreferences(getResources().getString(R.string.id), Context.MODE_PRIVATE);
        prefs = sharedpreferences.getString(getResources().getString(R.string.beepeerID), "");
        mp = MediaPlayer.create(getBaseContext(), R.raw.luncher); /*Gets your sound file from res/raw/sound.ogg */
        mp.start();
        manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );
        if ( !manager.isProviderEnabled( LocationManager.NETWORK_PROVIDER ) ) {

            buildAlertMessageNoGps();
        }
        else {
            if(!haveNetworkConnection()){
                showAlertDialoge();
            }
            else{
                delayUi();
            }
        }
    }

    private void delayUi() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 5s = 5000ms
                mp.stop();
                startActivity(new Intent(Splash.this, Wiki.class));
            }
        }, 5000);
    }

    private void showAlertDialoge() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage("Connect to Internet or quit")
                .setCancelable(false)
                .setPositiveButton("Connect to WIFI", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                    }
                })
                .setNegativeButton("Quit App", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        Splash.this.finish();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    public void statusCheck()
    {





    }
    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your Location seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog,  final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                        Splash.this.finish();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();

    }
}
