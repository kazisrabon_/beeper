/*
 * Copyright (c) to srabon
 * IIT, University of Dhaka
 * kaziiit@gmail.com
 */

package com.beepeer.bigs;

import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.beepeer.bigs.model.NearBy;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

public class NearByPlacesFragment extends DialogFragment {
    View view;
    private ListView listView;
    public static double selectedItemLatitude = 0;
    public static double selectedItemLongitude = 0;
    public static String nearbyPlaceName ="";
    public static String nearbyPlaceLocation ="";
    private SharedPreferences app;
    private String themeColor;
    private LinearLayout titleLayout;
    private TextView title;

    static NearByPlacesFragment newInstance(){
        return new NearByPlacesFragment();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.httptestlist, container, false);
            titleLayout = (LinearLayout)view.findViewById(R.id.linearLayoutTitle);
            title = (TextView)view.findViewById(R.id.title);
            title.setText(HomeFragment.types.toUpperCase());
        } catch (InflateException e) {
        /* profile is already there, just return view as it is */
        }
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        app = getActivity().getSharedPreferences("app", Context.MODE_PRIVATE);
        themeColor = app.getString("color", "");
        init();
        return view;
    }

    private void init() {
        listView = (ListView)view.findViewById(R.id.httptestlist_listview);
        changeListColorUsingTheme();
        listView.setAdapter(HomeFragment.placeAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(HomeFragment.types.equals("hospital")){
                    HomeFragment.nearByPlaceIcon = BitmapDescriptorFactory.fromResource(R.mipmap.ic_winter_16);
                }
                else if (HomeFragment.types.equals("school")){
                    HomeFragment.nearByPlaceIcon = BitmapDescriptorFactory.fromResource(R.mipmap.ic_winter_11);
                }
                else if (HomeFragment.types.equals("restaurant")){
                    HomeFragment.nearByPlaceIcon = BitmapDescriptorFactory.fromResource(R.mipmap.ic_winter_9);
                }
                else if (HomeFragment.types.equals("bank")){
                    HomeFragment.nearByPlaceIcon = BitmapDescriptorFactory.fromResource(R.mipmap.ic_winter_7);
                }
                else if (HomeFragment.types.equals("atm")){
                    HomeFragment.nearByPlaceIcon = BitmapDescriptorFactory.fromResource(R.mipmap.ic_winter_10);
                }
                else if (HomeFragment.types.equals("shopping_mall")){
                    HomeFragment.nearByPlaceIcon = BitmapDescriptorFactory.fromResource(R.mipmap.ic_winter_2);
                }
                else if (HomeFragment.types.equals("food")){
                    HomeFragment.nearByPlaceIcon = BitmapDescriptorFactory.fromResource(R.mipmap.ic_winter_9);
                }


                NearBy nearBy = HomeFragment.nearbyPlaces.get(position);
                nearbyPlaceName = nearBy.getName();
                nearbyPlaceLocation = nearBy.getVicinity();
                Toast.makeText(getActivity().getApplicationContext(), nearbyPlaceName, Toast.LENGTH_SHORT).show();
                float[] place = nearBy.getGeometry();
                selectedItemLatitude = place[0];
                selectedItemLongitude = place[1];
                debug(position, nearBy, place, selectedItemLatitude, selectedItemLongitude);
                Fragment homeFragment = new HomeFragment();
                getDialog().dismiss();
                getActivity().getFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, homeFragment)
                        .commit();
            }
        });
    }

    private void changeListColorUsingTheme() {
        if(themeColor.equals("red")){
            view.findViewById(R.id.relativeLayoutMapChatOutter).setBackgroundColor(getResources().getColor(R.color.spring_light));
            listView.setBackgroundColor(getResources().getColor(R.color.spring_light));
            titleLayout.setBackgroundColor(getResources().getColor(R.color.spring));
            title.setBackgroundColor(getResources().getColor(R.color.spring_light));
        }
        else if(themeColor.equals("yellow")){
            view.findViewById(R.id.relativeLayoutMapChatOutter).setBackgroundColor(getResources().getColor(R.color.summer_light));
            listView.setBackgroundColor(getResources().getColor(R.color.summer_light));
            titleLayout.setBackgroundColor(getResources().getColor(R.color.summer));
            title.setBackgroundColor(getResources().getColor(R.color.summer_light));
        }
        else if(themeColor.equals("blue")){
            view.findViewById(R.id.relativeLayoutMapChatOutter).setBackgroundColor(getResources().getColor(R.color.autumn_light));
            listView.setBackgroundColor(getResources().getColor(R.color.autumn_light));
            titleLayout.setBackgroundColor(getResources().getColor(R.color.autumn));
            title.setBackgroundColor(getResources().getColor(R.color.autumn_light));
        }
        else {
            view.findViewById(R.id.relativeLayoutMapChatOutter).setBackgroundColor(getResources().getColor(R.color.winter_light));
            listView.setBackgroundColor(getResources().getColor(R.color.winter_light));
            titleLayout.setBackgroundColor(getResources().getColor(R.color.winter));
            title.setBackgroundColor(getResources().getColor(R.color.winter_light));
        }
    }

    private void debug(int position, NearBy nearBy, float[] place, double selectedItemLatitude, double selectedItemLongitude) {
        Log.e("NearbyFragment", "pos:"+position+" name:"+nearBy.getName()+
                " place[0]"+place[0]+" place[1]"+place[1]+" lat:"+selectedItemLatitude+" lon:"+selectedItemLongitude);
    }
}
