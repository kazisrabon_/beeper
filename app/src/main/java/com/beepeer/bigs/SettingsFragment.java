/*
 * Copyright (c) to srabon
 * IIT, University of Dhaka
 * kaziiit@gmail.com
 */

package com.beepeer.bigs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.beepeer.bigs.adapter.ContactAdapter;
import com.beepeer.bigs.model.Contact;

import java.util.ArrayList;
import java.util.List;

public class SettingsFragment extends Fragment implements View.OnClickListener {

    private View view;
    SharedPreferences values;
    SharedPreferences.Editor editor;
    private String hasNotification;
    private TextView textViewTheme, textViewMarker, textViewNotification, getTextViewNotificationSound, textViewSOS;
    private String sosNumber = "";
    List<String> phones = new ArrayList<String>();
    List<String> names = new ArrayList<String>();
    String name, phone, id;
    private Contact contact;
    private List<Contact> contacts;
    private Dialog dialogContact;
    private ListView listViewContacts;
    private ContactAdapter contactAdapter;
    private Contact selectedContact;
    private String themeColor;
    private CheckBox chksummer, chkautumn, chkspring, chkwinter, chkman, chkwoman, chkcng, chkcar, chkrick, chkNY, chkNN, chkSY, chkSN;
    private LinearLayout llAutumn, llSummer, llSpring, llWinter, llMan, llWoman, llCar, llCng, llRick, llNY, llNN, llSY, llSN;
    private ProgressDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.settings_fragment, container, false);
//            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (InflateException e) {
        /* map is already there, just return view as it is */
        }

        init();

        return view;
    }

    private void init() {
        values = getActivity().getSharedPreferences("app", Context.MODE_PRIVATE);
        hasNotification = values.getString("notification", "");
        sosNumber = values.getString("sos", "");
        editor = values.edit();
        hideAll();
//        setYesNoColor();
        chksummer = (CheckBox) view.findViewById(R.id.checkboxSummer);
        chkautumn = (CheckBox) view.findViewById(R.id.checkboxAutumn);
        chkspring = (CheckBox) view.findViewById(R.id.checkboxSpring);
        chkwinter = (CheckBox) view.findViewById(R.id.checkboxWinter);
        chkman = (CheckBox) view.findViewById(R.id.checkboxMan);
        chkwoman = (CheckBox) view.findViewById(R.id.checkboxWoman);
        chkcng = (CheckBox) view.findViewById(R.id.checkboxCng);
        chkcar = (CheckBox) view.findViewById(R.id.checkboxCar);
        chkrick = (CheckBox) view.findViewById(R.id.checkboxRick);
        chkNY = (CheckBox) view.findViewById(R.id.checkboxNY);
        chkNN = (CheckBox) view.findViewById(R.id.checkboxNN);
        chkSY = (CheckBox) view.findViewById(R.id.checkboxSY);
        chkSN = (CheckBox) view.findViewById(R.id.checkboxSN);

        llAutumn = (LinearLayout) view.findViewById(R.id.llAutumn);
        llSummer = (LinearLayout) view.findViewById(R.id.llSummer);
        llSpring = (LinearLayout) view.findViewById(R.id.llSpring);
        llWinter = (LinearLayout) view.findViewById(R.id.llWinter);

        llMan = (LinearLayout) view.findViewById(R.id.llMan);
        llWoman = (LinearLayout) view.findViewById(R.id.llWoman);
        llCar = (LinearLayout) view.findViewById(R.id.llCar);
        llCng = (LinearLayout) view.findViewById(R.id.llCng);
        llRick = (LinearLayout) view.findViewById(R.id.llRick);

        llNY = (LinearLayout) view.findViewById(R.id.llNY);
        llNN = (LinearLayout) view.findViewById(R.id.llNN);

        llSY = (LinearLayout) view.findViewById(R.id.llSY);
        llSN = (LinearLayout) view.findViewById(R.id.llSN);

        setCheckUncheck(chksummer, chkautumn, chkspring, chkwinter, chkman, chkwoman, chkcng, chkcar, chkrick, chkNY, chkNN, chkSY, chkSN);

        chksummer.setOnClickListener(this);
        chkautumn.setOnClickListener(this);
        chkspring.setOnClickListener(this);
        chkwinter.setOnClickListener(this);
        chkman.setOnClickListener(this);
        chkwoman.setOnClickListener(this);
        chkcng.setOnClickListener(this);
        chkcar.setOnClickListener(this);
        chkrick.setOnClickListener(this);
        chkNY.setOnClickListener(this);
        chkNN.setOnClickListener(this);
        chkSY.setOnClickListener(this);
        chkSN.setOnClickListener(this);

        llAutumn.setOnClickListener(this);
        llSummer.setOnClickListener(this);
        llSpring.setOnClickListener(this);
        llWinter.setOnClickListener(this);
        llMan.setOnClickListener(this);
        llWoman.setOnClickListener(this);
        llCar.setOnClickListener(this);
        llCng.setOnClickListener(this);
        llRick.setOnClickListener(this);
        llNY.setOnClickListener(this);
        llNN.setOnClickListener(this);
        llSY.setOnClickListener(this);
        llSN.setOnClickListener(this);

        view.findViewById(R.id.textviewTheme).setOnClickListener(this);
        view.findViewById(R.id.textviewMarker).setOnClickListener(this);
        view.findViewById(R.id.textviewNotification).setOnClickListener(this);
        view.findViewById(R.id.tvNotificationSound).setOnClickListener(this);
        view.findViewById(R.id.textviewSOS).setOnClickListener(this);
        view.findViewById(R.id.redtheme).setOnClickListener(this);
        view.findViewById(R.id.yellowtheme).setOnClickListener(this);
        view.findViewById(R.id.greentheme).setOnClickListener(this);
        view.findViewById(R.id.ourtheme).setOnClickListener(this);
        view.findViewById(R.id.markerMan).setOnClickListener(this);
        view.findViewById(R.id.markerWoman).setOnClickListener(this);
        view.findViewById(R.id.markerCar).setOnClickListener(this);
        view.findViewById(R.id.markerCng).setOnClickListener(this);
        view.findViewById(R.id.markerRick).setOnClickListener(this);
        view.findViewById(R.id.notificationGreen).setOnClickListener(this);
        view.findViewById(R.id.notificationRed).setOnClickListener(this);
        view.findViewById(R.id.notificationSoundGreen).setOnClickListener(this);
        view.findViewById(R.id.notificationSoundRed).setOnClickListener(this);
//        changeColorUsingTheme();
        invisibleNotificationSound();
    }

    private void setCheckUncheck(CheckBox chksummer, CheckBox chkautumn, CheckBox chkspring,
                                 CheckBox chkwinter, CheckBox chkman, CheckBox chkwoman,
                                 CheckBox chkcng, CheckBox chkcar, CheckBox chkrick, CheckBox chkNY,
                                 CheckBox chkNN, CheckBox chkSY, CheckBox chkSN) {
        String themeColor = values.getString("color", "");
        String marker = values.getString("marker", "");
        String notification = values.getString("notification", "");
        String sound = values.getString("sound", "");

        if(notification.equals("yes")){
            nyChecked();
        }
        else if(notification.equals("no")){
            nnChecked();
        }
        else {
            nUnchecked();
        }

        if(sound.equals("yes")){
            syChecked();
        }
        else if(sound.equals("no")){
            snChecked();
        }
        else {
            sUnchecked();
        }

        if(marker.equals("man")){
            manChecked();
        }
        else if(marker.equals("woman")) {
            womanChecked();
        }
        else if(marker.equals("cng")){
            cngChecked();
        }
        else if(marker.equals("car")){
            carChecked();
        }
        else if(marker.equals("rick")){
            rickChecked();
        }

        if(themeColor.equals("blue")){
            autumnChecked();
        }
        else if(themeColor.equals("yellow")){
            summerChecked();
        }
        else if(themeColor.equals("red")){
            springChecked();
        }
        else if(themeColor.equals("our")){
            winterChecked();
        }

    }

    private void sUnchecked() {
        chkSY.setChecked(false);
        chkSN.setChecked(false);
    }

    private void syChecked() {
        chkSY.setChecked(true);
        chkSN.setChecked(false);
    }

    private void snChecked() {
        chkSY.setChecked(false);
        chkSN.setChecked(true);
    }

    private void nUnchecked() {
        chkNY.setChecked(false);
        chkNN.setChecked(false);
    }

    private void nyChecked() {
        chkNY.setChecked(true);
        chkNN.setChecked(false);
    }

    private void nnChecked() {
        chkNY.setChecked(false);
        chkNN.setChecked(true);
    }

//    private void setYesNoColor() {
//        View notiGreen = view.findViewById(R.id.notificationGreen);
//        View notiRed = view.findViewById(R.id.notificationRed);
//        View soundGreen = view.findViewById(R.id.notificationSoundGreen);
//        View soundRed = view.findViewById(R.id.notificationSoundRed);
//        String notification = values.getString("notification", "");
//        String sound = values.getString("sound", "");
//        String themeColor = values.getString("color", "");
//        if(themeColor.equals("red")){
//            if(notification.equals("yes")){
//                notiGreen.setBackgroundColor(getResources().getColor(R.color.pressed));
//            }
//            if(notification.equals("no")){
//                notiRed.setBackgroundColor(getResources().getColor(R.color.pressed));
//            }
//            if(sound.equals("yes")){
//                soundGreen.setBackgroundColor(getResources().getColor(R.color.pressed));
//            }
//            if(sound.equals("no")){
//                soundRed.setBackgroundColor(getResources().getColor(R.color.pressed));
//            }
//        }
//        else if(themeColor.equals("yellow")){
//            if(notification.equals("yes")){
//                notiGreen.setBackgroundColor(getResources().getColor(R.color.pressed));
//            }
//            if(notification.equals("no")){
//                notiRed.setBackgroundColor(getResources().getColor(R.color.pressed));
//            }
//            if(sound.equals("yes")){
//                soundGreen.setBackgroundColor(getResources().getColor(R.color.pressed));
//            }
//            if(sound.equals("no")){
//                soundRed.setBackgroundColor(getResources().getColor(R.color.pressed));
//            }
//
//        }
//        else if(themeColor.equals("blue")){
//            if(notification.equals("yes")){
//                notiGreen.setBackgroundColor(getResources().getColor(R.color.pressed));
//            }
//            if(notification.equals("no")){
//                notiRed.setBackgroundColor(getResources().getColor(R.color.pressed));
//            }
//            if(sound.equals("yes")){
//                soundGreen.setBackgroundColor(getResources().getColor(R.color.pressed));
//            }
//            if(sound.equals("no")){
//                soundRed.setBackgroundColor(getResources().getColor(R.color.pressed));
//            }
//        }
//        else{
//            if(notification.equals("yes")){
//                notiGreen.setBackgroundColor(getResources().getColor(R.color.pressed));
//            }
//            if(notification.equals("no")){
//                notiRed.setBackgroundColor(getResources().getColor(R.color.pressed));
//            }
//            if(sound.equals("yes")){
//                soundGreen.setBackgroundColor(getResources().getColor(R.color.pressed));
//            }
//            if(sound.equals("no")){
//                soundRed.setBackgroundColor(getResources().getColor(R.color.pressed));
//            }
//        }
//    }

    private void invisibleNotificationSound() {
        if(hasNotification.equals("yes")){
            view.findViewById(R.id.tvNotificationSound).setVisibility(View.VISIBLE);
            view.findViewById(R.id.linearLayoutNotificationSound).setVisibility(View.VISIBLE);
        }else {
            view.findViewById(R.id.tvNotificationSound).setVisibility(View.GONE);
            view.findViewById(R.id.linearLayoutNotificationSound).setVisibility(View.GONE);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.textviewTheme:
                if(view.findViewById(R.id.linearLayoutTheme).getVisibility() == View.VISIBLE) {
                    view.findViewById(R.id.linearLayoutTheme).setVisibility(View.GONE);
                }
                else{
                    view.findViewById(R.id.linearLayoutTheme).setVisibility(View.VISIBLE);
                    changeColorOfText(view.findViewById(R.id.textviewTheme));
                }
                view.findViewById(R.id.linearLayoutMarker).setVisibility(View.GONE);
                view.findViewById(R.id.linearLayoutNotification).setVisibility(View.GONE);
                view.findViewById(R.id.linearLayoutNotificationSound).setVisibility(View.GONE);
                break;

            case R.id.textviewMarker:
                if(view.findViewById(R.id.linearLayoutMarker).getVisibility() == View.VISIBLE) {
                    view.findViewById(R.id.linearLayoutMarker).setVisibility(View.GONE);
                }
                else{
                    view.findViewById(R.id.linearLayoutMarker).setVisibility(View.VISIBLE);
                    changeColorOfText(view.findViewById(R.id.textviewMarker));
                }
                view.findViewById(R.id.linearLayoutTheme).setVisibility(View.GONE);
                view.findViewById(R.id.linearLayoutNotification).setVisibility(View.GONE);
                view.findViewById(R.id.linearLayoutNotificationSound).setVisibility(View.GONE);
                break;

            case R.id.textviewNotification:
                if(view.findViewById(R.id.linearLayoutNotification).getVisibility() == View.VISIBLE) {
                    view.findViewById(R.id.linearLayoutNotification).setVisibility(View.GONE);
                }
                else{
                    view.findViewById(R.id.linearLayoutNotification).setVisibility(View.VISIBLE);
                    changeColorOfText(view.findViewById(R.id.textviewNotification));
                }
                view.findViewById(R.id.linearLayoutTheme).setVisibility(View.GONE);
                view.findViewById(R.id.linearLayoutMarker).setVisibility(View.GONE);
                view.findViewById(R.id.linearLayoutNotificationSound).setVisibility(View.GONE);
                break;

            case R.id.tvNotificationSound:
                if(view.findViewById(R.id.linearLayoutNotificationSound).getVisibility() == View.VISIBLE) {
                    view.findViewById(R.id.linearLayoutNotificationSound).setVisibility(View.GONE);
                }
                else{
                    view.findViewById(R.id.linearLayoutNotificationSound).setVisibility(View.VISIBLE);
                    changeColorOfText(view.findViewById(R.id.tvNotificationSound));
                }
                view.findViewById(R.id.linearLayoutTheme).setVisibility(View.GONE);
                view.findViewById(R.id.linearLayoutMarker).setVisibility(View.GONE);
                view.findViewById(R.id.linearLayoutNotification).setVisibility(View.GONE);
                break;

            case R.id.textviewSOS:
                changeColorOfText(view.findViewById(R.id.textviewSOS));
                view.findViewById(R.id.linearLayoutNotificationSound).setVisibility(View.GONE);
                view.findViewById(R.id.linearLayoutTheme).setVisibility(View.GONE);
                view.findViewById(R.id.linearLayoutMarker).setVisibility(View.GONE);
                view.findViewById(R.id.linearLayoutNotification).setVisibility(View.GONE);

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());   //never use application context to show dialog
                LayoutInflater inflater=getActivity().getLayoutInflater();
                final View layout=inflater.inflate(R.layout.sos,null);
                builder.setView(layout);
                builder.setTitle("SOS Call Setting");
                final EditText editTextSOSNumber = (EditText)layout.findViewById(R.id.editTextSOSNumber);
                if(!sosNumber.equals("")){
                    editTextSOSNumber.setText(sosNumber);
                }
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        sosNumber = editTextSOSNumber.getText().toString().trim();
                        editor.putString("sos", sosNumber);
                        editor.commit();
                        dialog.cancel();
                        view.findViewById(R.id.textviewSOS).setBackgroundColor(getResources().getColor(R.color.white));
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        view.findViewById(R.id.textviewSOS).setBackgroundColor(getResources().getColor(R.color.white));
                    }
                });
                builder.setNeutralButton("Phonebook", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        showPhoneNumber();
                        if(!sosNumber.equals("")){
                            editTextSOSNumber.setText(sosNumber);
                        }
                    }
                });
                builder.show();
                break;

            case R.id.greentheme:
                //autumn
                autumnChecked();
                Log.e("greenSettings", "Green is selected as theme");
                Toast.makeText(getActivity().getApplicationContext(), "Autumn is selected as theme", Toast.LENGTH_SHORT).show();
                MapsActivity.toolbar.setBackgroundColor(getResources().getColor(R.color.autumn_header));
                MapsActivity.bottomLayout.setBackgroundColor(getResources().getColor(R.color.autumn_header));
                editor.putString("color", "blue");
                editor.commit();
//                changeColorUsingTheme();
                break;

            case R.id.yellowtheme:
                //summer
                summerChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Summer is selected as theme", Toast.LENGTH_SHORT).show();
                MapsActivity.toolbar.setBackgroundColor(getResources().getColor(R.color.summer_header));
                MapsActivity.bottomLayout.setBackgroundColor(getResources().getColor(R.color.summer_header));
                editor.putString("color", "yellow");
//                changeColorUsingTheme();
                editor.commit();
                break;

            case R.id.redtheme:
                //spring
                springChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Spring is selected as theme", Toast.LENGTH_SHORT).show();
                MapsActivity.toolbar.setBackgroundColor(getResources().getColor(R.color.spring_header));
                MapsActivity.bottomLayout.setBackgroundColor(getResources().getColor(R.color.spring_header));
                editor.putString("color", "red");
                editor.commit();
//                changeColorUsingTheme();
                break;

            case R.id.ourtheme:
                //winter
                winterChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Winter is selected as theme", Toast.LENGTH_SHORT).show();
                MapsActivity.toolbar.setBackgroundColor(getResources().getColor(R.color.winter_header));
                MapsActivity.bottomLayout.setBackgroundColor(getResources().getColor(R.color.winter_header));
                editor.putString("color", "our");
                editor.commit();
//                changeColorUsingTheme();
                break;

            case R.id.markerMan:
                manChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Marker Changed", Toast.LENGTH_SHORT).show();
                editor.putString("marker", "man");
                editor.commit();
                break;

            case R.id.markerWoman:
                womanChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Marker Changed", Toast.LENGTH_SHORT).show();
                editor.putString("marker", "woman");
                editor.commit();
                break;

            case R.id.markerCar:
                carChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Marker Changed", Toast.LENGTH_SHORT).show();
                editor.putString("marker", "car");
                editor.commit();
                break;

            case R.id.markerCng:
                cngChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Marker Changed", Toast.LENGTH_SHORT).show();
                editor.putString("marker", "cng");
                editor.commit();
                break;

            case R.id.markerRick:
                rickChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Marker Changed", Toast.LENGTH_SHORT).show();
                editor.putString("marker", "rick");
                editor.commit();
                break;

            case R.id.notificationGreen:
                nyChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Your notification is turned on", Toast.LENGTH_SHORT).show();
                editor.putString("notification", "yes");
                editor.commit();
                view.findViewById(R.id.tvNotificationSound).setVisibility(View.VISIBLE);
//                setYesNoColor();
//                view.findViewById(R.id.linearLayoutNotificationSound).setVisibility(View.VISIBLE);
//                view.findViewById(R.id.viewNotificationSound).setVisibility(View.VISIBLE);
                break;

            case R.id.notificationRed:
                nnChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Your notification is turned off", Toast.LENGTH_SHORT).show();
                editor.putString("notification", "no");
                editor.commit();
                view.findViewById(R.id.tvNotificationSound).setVisibility(View.GONE);
                view.findViewById(R.id.linearLayoutNotificationSound).setVisibility(View.GONE);
//                setYesNoColor();
                break;

            case R.id.notificationSoundGreen:
                syChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Your notification sound is turned on", Toast.LENGTH_SHORT).show();
                editor.putString("sound", "yes");
                editor.commit();
//                setYesNoColor();
                break;

            case R.id.notificationSoundRed:
                snChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Your notification sound is turned off", Toast.LENGTH_SHORT).show();
                editor.putString("sound", "no");
                editor.commit();
//                setYesNoColor();
                break;

            case R.id.checkboxSummer:
                summerChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Summer is selected as theme", Toast.LENGTH_SHORT).show();
                MapsActivity.toolbar.setBackgroundColor(getResources().getColor(R.color.summer_header));
                MapsActivity.bottomLayout.setBackgroundColor(getResources().getColor(R.color.summer_header));
                editor.putString("color", "yellow");
                editor.commit();
                break;

            case R.id.checkboxAutumn:
                autumnChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Autumn is selected as theme", Toast.LENGTH_SHORT).show();
                MapsActivity.toolbar.setBackgroundColor(getResources().getColor(R.color.autumn_header));
                MapsActivity.bottomLayout.setBackgroundColor(getResources().getColor(R.color.autumn_header));
                editor.putString("color", "blue");
                editor.commit();
                break;

            case R.id.checkboxWinter:
                winterChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Winter is selected as theme", Toast.LENGTH_SHORT).show();
                MapsActivity.toolbar.setBackgroundColor(getResources().getColor(R.color.winter_header));
                MapsActivity.bottomLayout.setBackgroundColor(getResources().getColor(R.color.winter_header));
                editor.putString("color", "our");
                editor.commit();
                break;

            case R.id.checkboxSpring:
                springChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Spring is selected as theme", Toast.LENGTH_SHORT).show();
                MapsActivity.toolbar.setBackgroundColor(getResources().getColor(R.color.spring_header));
                MapsActivity.bottomLayout.setBackgroundColor(getResources().getColor(R.color.spring_header));
                editor.putString("color", "red");
                editor.commit();
                break;

            case R.id.checkboxMan:
                manChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Marker Changed", Toast.LENGTH_SHORT).show();
                editor.putString("marker", "man");
                editor.commit();
                break;

            case R.id.checkboxWoman:
                womanChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Marker Changed", Toast.LENGTH_SHORT).show();
                editor.putString("marker", "woman");
                editor.commit();
                break;

            case R.id.checkboxCng:
                cngChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Marker Changed", Toast.LENGTH_SHORT).show();
                editor.putString("marker", "cng");
                editor.commit();
                break;

            case R.id.checkboxCar:
                carChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Marker Changed", Toast.LENGTH_SHORT).show();
                editor.putString("marker", "car");
                editor.commit();
                break;

            case R.id.checkboxRick:
                rickChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Marker Changed", Toast.LENGTH_SHORT).show();
                editor.putString("marker", "rick");
                editor.commit();
                break;

            case R.id.llNY:
                nyChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Your notification is turned on", Toast.LENGTH_SHORT).show();
                editor.putString("notification", "yes");
                editor.commit();
                view.findViewById(R.id.tvNotificationSound).setVisibility(View.VISIBLE);
//                setYesNoColor();
//                view.findViewById(R.id.linearLayoutNotificationSound).setVisibility(View.VISIBLE);
//                view.findViewById(R.id.viewNotificationSound).setVisibility(View.VISIBLE);
                break;

            case R.id.llNN:
                nnChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Your notification is turned off", Toast.LENGTH_SHORT).show();
                editor.putString("notification", "no");
                editor.commit();
                view.findViewById(R.id.tvNotificationSound).setVisibility(View.GONE);
                view.findViewById(R.id.linearLayoutNotificationSound).setVisibility(View.GONE);
//                setYesNoColor();
                break;

            case R.id.llSY:
                syChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Your notification sound is turned on", Toast.LENGTH_SHORT).show();
                editor.putString("sound", "yes");
                editor.commit();
//                setYesNoColor();
                break;

            case R.id.llSN:
                snChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Your notification sound is turned off", Toast.LENGTH_SHORT).show();
                editor.putString("sound", "no");
                editor.commit();
//                setYesNoColor();
                break;

            case R.id.llSummer:
                summerChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Summer is selected as theme", Toast.LENGTH_SHORT).show();
                MapsActivity.toolbar.setBackgroundColor(getResources().getColor(R.color.summer_header));
                MapsActivity.bottomLayout.setBackgroundColor(getResources().getColor(R.color.summer_header));
                editor.putString("color", "yellow");
                editor.commit();
                break;

            case R.id.llAutumn:
                autumnChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Autumn is selected as theme", Toast.LENGTH_SHORT).show();
                MapsActivity.toolbar.setBackgroundColor(getResources().getColor(R.color.autumn_header));
                MapsActivity.bottomLayout.setBackgroundColor(getResources().getColor(R.color.autumn_header));
                editor.putString("color", "blue");
                editor.commit();
                break;

            case R.id.llWinter:
                winterChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Winter is selected as theme", Toast.LENGTH_SHORT).show();
                MapsActivity.toolbar.setBackgroundColor(getResources().getColor(R.color.winter_header));
                MapsActivity.bottomLayout.setBackgroundColor(getResources().getColor(R.color.winter_header));
                editor.putString("color", "our");
                editor.commit();
                break;

            case R.id.llSpring:
                springChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Spring is selected as theme", Toast.LENGTH_SHORT).show();
                MapsActivity.toolbar.setBackgroundColor(getResources().getColor(R.color.spring_header));
                MapsActivity.bottomLayout.setBackgroundColor(getResources().getColor(R.color.spring_header));
                editor.putString("color", "red");
                editor.commit();
                break;

            case R.id.llMan:
                manChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Marker Changed", Toast.LENGTH_SHORT).show();
                editor.putString("marker", "man");
                editor.commit();
                break;

            case R.id.llWoman:
                womanChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Marker Changed", Toast.LENGTH_SHORT).show();
                editor.putString("marker", "woman");
                editor.commit();
                break;

            case R.id.llCng:
                cngChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Marker Changed", Toast.LENGTH_SHORT).show();
                editor.putString("marker", "cng");
                editor.commit();
                break;

            case R.id.llCar:
                carChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Marker Changed", Toast.LENGTH_SHORT).show();
                editor.putString("marker", "car");
                editor.commit();
                break;

            case R.id.llRick:
                rickChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Marker Changed", Toast.LENGTH_SHORT).show();
                editor.putString("marker", "rick");
                editor.commit();
                break;

            case R.id.checkboxNY:
                nyChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Your notification is turned on", Toast.LENGTH_SHORT).show();
                editor.putString("notification", "yes");
                editor.commit();
                view.findViewById(R.id.tvNotificationSound).setVisibility(View.VISIBLE);
                break;

            case R.id.checkboxNN:
                nnChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Your notification is turned off", Toast.LENGTH_SHORT).show();
                editor.putString("notification", "no");
                editor.commit();
                view.findViewById(R.id.tvNotificationSound).setVisibility(View.GONE);
                view.findViewById(R.id.linearLayoutNotificationSound).setVisibility(View.GONE);
                break;

            case R.id.checkboxSY:
                syChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Your notification sound is turned on", Toast.LENGTH_SHORT).show();
                editor.putString("sound", "yes");
                editor.commit();
                break;

            case R.id.checkboxSN:
                snChecked();
                Toast.makeText(getActivity().getApplicationContext(), "Your notification sound is turned off", Toast.LENGTH_SHORT).show();
                editor.putString("sound", "no");
                editor.commit();
                break;
        }
    }

    private void rickChecked() {
        chkman.setChecked(false);
        chkwoman.setChecked(false);
        chkcng.setChecked(false);
        chkcar.setChecked(false);
        chkrick.setChecked(true);
    }

    private void carChecked() {
        chkman.setChecked(false);
        chkwoman.setChecked(false);
        chkcng.setChecked(false);
        chkcar.setChecked(true);
        chkrick.setChecked(false);
    }

    private void cngChecked() {
        chkman.setChecked(false);
        chkwoman.setChecked(false);
        chkcng.setChecked(true);
        chkcar.setChecked(false);
        chkrick.setChecked(false);
    }

    private void womanChecked() {
        chkman.setChecked(false);
        chkwoman.setChecked(true);
        chkcng.setChecked(false);
        chkcar.setChecked(false);
        chkrick.setChecked(false);
    }

    private void manChecked() {
        chkman.setChecked(true);
        chkwoman.setChecked(false);
        chkcng.setChecked(false);
        chkcar.setChecked(false);
        chkrick.setChecked(false);
    }

    private void springChecked() {
        chkautumn.setChecked(false);
        chksummer.setChecked(false);
        chkspring.setChecked(true);
        chkwinter.setChecked(false);
    }

    private void winterChecked() {
        chkautumn.setChecked(false);
        chksummer.setChecked(false);
        chkspring.setChecked(false);
        chkwinter.setChecked(true);
    }

    private void autumnChecked() {
        chkautumn.setChecked(true);
        chksummer.setChecked(false);
        chkspring.setChecked(false);
        chkwinter.setChecked(false);
    }

    private void summerChecked() {
        chkautumn.setChecked(false);
        chksummer.setChecked(true);
        chkspring.setChecked(false);
        chkwinter.setChecked(false);
    }

    private void changeColorOfText(View view) {
        themeColor = values.getString("color", "");
        if(themeColor.equals("red")){
            view.setBackgroundColor(getResources().getColor(R.color.spring_light));
        }
        else if(themeColor.equals("yellow")){
            view.setBackgroundColor(getResources().getColor(R.color.summer_light));

        }
        else if(themeColor.equals("blue")){
            view.setBackgroundColor(getResources().getColor(R.color.autumn_light));
        }
        else {
            view.setBackgroundColor(getResources().getColor(R.color.winter_light));
        }
    }

//    private void changeColorUsingTheme() {
//        themeColor = values.getString("color", "");
//        if(themeColor.equals("red")){
//            view.findViewById(R.id.textviewMarker).setBackgroundColor(getResources().getColor(R.color.spring_light));
//            view.findViewById(R.id.tvNotificationSound).setBackgroundColor(getResources().getColor(R.color.spring_light));
//        }
//        else if(themeColor.equals("yellow")){
//            view.findViewById(R.id.textviewMarker).setBackgroundColor(getResources().getColor(R.color.summer_light));
//            view.findViewById(R.id.tvNotificationSound).setBackgroundColor(getResources().getColor(R.color.summer_light));
//        }
//        else if(themeColor.equals("blue")){
//            view.findViewById(R.id.textviewMarker).setBackgroundColor(getResources().getColor(R.color.autumn_light));
//            view.findViewById(R.id.tvNotificationSound).setBackgroundColor(getResources().getColor(R.color.autumn_light));
//        }
//        else {
//            view.findViewById(R.id.textviewMarker).setBackgroundColor(getResources().getColor(R.color.winter_light));
//            view.findViewById(R.id.tvNotificationSound).setBackgroundColor(getResources().getColor(R.color.winter_light));
//        }
//    }

    private void showPhoneNumber() {
        progressDialog = ProgressDialog.show(getActivity(), "Please wait", "Loading...", true);
        dialogContact = new Dialog(getActivity());
        dialogContact.setContentView(R.layout.friends);
        dialogContact.setTitle("Phone Number");
        dialogContact.setCanceledOnTouchOutside(true);
        listViewContacts = (ListView) dialogContact.findViewById(R.id.friends);
        readContacts();
        generateList();
    }

    public void hideAll(){
        view.findViewById(R.id.linearLayoutTheme).setVisibility(View.GONE);
        view.findViewById(R.id.linearLayoutMarker).setVisibility(View.GONE);
        view.findViewById(R.id.linearLayoutNotification).setVisibility(View.GONE);
        view.findViewById(R.id.linearLayoutNotificationSound).setVisibility(View.GONE);
    }

    public void readContacts() {
        phones.clear();
        names.clear();
        contacts = new ArrayList<Contact>();
        contacts.clear();
        ContentResolver cr = getActivity().getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);

        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
//                    Log.e("Friends" , name + " " + id);
                    // get the phone number
                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        phone = pCur.getString(
                                pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        phone = phone.replace("-", "");
                        phone = phone.replace(" ", "");
                        phone = phone.replace("+88", "");
//                        phones.add(phone);
//                        names.add(name);
                    }
                    Log.e("name&number", name +" "+ phone);
                    contact = new Contact(name, phone);
                    contacts.add(contact);
                    pCur.close();
                }
            }
        }
        contactAdapter = new ContactAdapter(getActivity(), contacts);
        listViewContacts.setAdapter(contactAdapter);
    }

    private void generateList() {
//        context = getApplicationContext();
        progressDialog.dismiss();
        dialogContact.show();
        listViewContacts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dialogContact.dismiss();
                selectedContact = contacts.get(position);
                sosNumber = selectedContact.phone;
                contacts.clear();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        MapsActivity.bottomLayout.setVisibility(View.GONE);
    }
}