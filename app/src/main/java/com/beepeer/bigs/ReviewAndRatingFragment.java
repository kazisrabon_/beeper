/*
 * Copyright (c) to srabon
 * IIT, University of Dhaka
 * kaziiit@gmail.com
 */

package com.beepeer.bigs;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class ReviewAndRatingFragment extends Fragment {

    private View view;
    private Button buttonUrlLink;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.review_rating_fragment, container, false);
            buttonUrlLink = (Button)view.findViewById(R.id.linkToPlayStore);
            buttonUrlLink.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    Intent viewIntent =
                            new Intent("android.intent.action.VIEW",
                                    Uri.parse("https://play.google.com/store/apps/details?id=com.beepeer.bigs&ah=WxDX-Z_sYJuUD58H7O4C4WCaCOM"));
                    startActivity(viewIntent);
                }
            });
        } catch (InflateException e) {
        /* map is already there, just return view as it is */
        }
        return view;
    }
}
