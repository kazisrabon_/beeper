/*
 * Copyright (c) to srabon
 * IIT, University of Dhaka
 * kaziiit@gmail.com
 */

package com.beepeer.bigs;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.beepeer.bigs.model.User;
import com.beepeer.bigs.utils.NetworkUtil;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.ProfileTracker;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.PersonBuffer;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;
import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.MobileServiceList;
import com.microsoft.windowsazure.mobileservices.http.NextServiceFilterCallback;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilter;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilterRequest;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilterResponse;
import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.table.TableOperationCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static com.microsoft.windowsazure.mobileservices.table.query.QueryOperations.val;

public class MainActivity extends ActionBarActivity implements View.OnClickListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        ResultCallback<People.LoadPeopleResult> {

    private static final int RC_SIGN_IN = 0;
    // Logcat tag
    private static final String TAG = "MainActivity";
    // Profile pic image size in pixels
    private static final int PROFILE_PIC_SIZE = 400;
    // Google client to interact with Google API
    public static GoogleApiClient mGoogleApiClient;

    public static boolean isGoogleSignIn = false;
    public static boolean isFacebookSignIn = false;
    public static boolean isUserLoggedIn = false;
    private MobileServiceList<User> result;
    public static MobileServiceClient mClient;
    public static MobileServiceTable<User> mUserTable;
    //    public static MobileServiceTable<Beep> mBeepTable;
    public static List<User> results;
    public static String id = "";
    TextView networkStatus;
    SharedPreferences sharedpreferences;
//    ProgressBar mProgressBar;
    private LoginButton loginBtn;   //facebook
    private CallbackManager callbackManager;
    private com.facebook.Profile fbProfile;
    private boolean mIntentInProgress;
    private boolean mSignInClicked;
    private ConnectionResult mConnectionResult;
    private SignInButton btnSignIn;
    private EditText etEmail;
    private Button loginButton;     //our's
    private Button btnLinkToRegister;
    private Intent intent;
    public static boolean google;
    private TextView loginErrorMsg;
    private ImageView imgCoverPic;
    private ImageView imgProfilePic;
    public static Drawable drawable;
    public static Drawable coverPic;
    public static String personName = "", email = "" , googlePlusId = "", facebookId = "";
    private String age = "";
    private String phoneNumber = "000";
    private ProfileTracker profileTracker;
    private String gender;
    private SharedPreferences.Editor editor;
    public static String userName ="", url = "", cover = "";
    public static String facebookProfileImage = "";
    private SharedPreferences picSF;
    private SharedPreferences.Editor picEditor;
    private Button btnSignOut;
    private String prefs;
    private ProgressDialog progressDialog;
    private SharedPreferences profile;
    private LocationManager manager;

    public MainActivity() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_main);

        sharedpreferences = getSharedPreferences(getResources().getString(R.string.id), Context.MODE_PRIVATE);
        picSF = getSharedPreferences("pictures", Context.MODE_PRIVATE);
        profile = getSharedPreferences("app", Context.MODE_PRIVATE);
        picEditor = picSF.edit();
        networkStatus = (TextView) findViewById(R.id.networkStatus);
        loginErrorMsg = (TextView) findViewById(R.id.login_error);
        imgProfilePic = (ImageView) findViewById(R.id.imgProfilePic);
        imgCoverPic = (ImageView) findViewById(R.id.imgCoverPic);
        intent = new Intent(MainActivity.this, MapsActivity.class);
        prefs = sharedpreferences.getString(getResources().getString(R.string.beepeerID), "");
        try {
            mClient = new MobileServiceClient(
                    "https://beepeer.azure-mobile.net/",
                    "TBpzCbTsUcdYjPiGBdZunJfjRnhEiR93",
                    this).withFilter(new ProgressFilter());
            mUserTable = mClient.getTable(User.class);
//            mBeepTable =  mClient.getTable(Beep.class);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        Log.e("Prefs", prefs);

        etEmail = (EditText) findViewById(R.id.email);
        final User item = new User();
        loginButton = (Button) findViewById(R.id.btnLogin);
        btnLinkToRegister = (Button) findViewById(R.id.btnLinkToRegisterScreen);

//        facebook
        loginBtn = (LoginButton) findViewById(R.id.login_button);
        loginBtn.setReadPermissions(Arrays.asList("public_profile, email, user_birthday"));
        loginBtn.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code

                loginResult.getAccessToken().getUserId();
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                try {
                                    Log.e("facebook4", object.getJSONObject("picture")
                                            .getJSONObject("data")
                                            .getString("url"));
//                                    sendJSON(object);
//                                    url = object.getString("url");
//                                    url = object.getJSONObject("picture").getJSONObject("data").getString("url");
//                                    cover = object.getJSONObject("cover").getString("source");

                                    final User user= new User();
                                    user.setmName(object.getString("name"));
                                    user.setmFacebookId(object.getString("id"));
                                    addUser(user);

//                                    facebookProfileImage = object.getString("");
//                                    Log.e("facebook4", "User Info: " + personName +  "Id: "+ facebookId);
                                }catch (Exception ex){}
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,link,picture{url},cover");
                request.setParameters(parameters);
                request.executeAsync();
//                Log.e("facebook4", "User Info: " + personName +  "Id: "+ facebookId);
//                addUser();

            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });

        if (NetworkUtil.getConnectivityStatus(getBaseContext()) == 0)
            networkStatus.setText(NetworkUtil.getConnectivityStatusString(getBaseContext()));

//        mProgressBar = (ProgressBar) findViewById(R.id.loadingProgressBarMain);
//        mProgressBar.setVisibility(ProgressBar.GONE);

        loginButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                HomeFragment.hasNew = false;
                checkUser();
//                String prefs = sharedpreferences.getString(getResources().getString(R.string.id), "");
//                Log.e("Prefs", prefs);
            }
        });

        // Link to Register Screen
        btnLinkToRegister.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this,
                        RegisterActivity.class);
                manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );
                if ( !manager.isProviderEnabled( LocationManager.NETWORK_PROVIDER ) ) {

                    startActivity(i);
                }
                else {
                    startService(new Intent(MainActivity.this, BeepService.class));
                    startActivity(i);
                }

//                finish();
            }
        });

//        validator = new Validator(this);

//        google sign in
        googleSignIn();

        if (!prefs.equals("")) {
            startService(new Intent(MainActivity.this, BeepService.class));
            startActivity(intent);
        }
    }

    private void sendJSON(JSONObject object) {
        try {
            picEditor.putString("profile",object.getString("url"));
            picEditor.putString("profile",object.getString("cover"));
            picEditor.commit();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void googleSignIn() {
        btnSignIn = (SignInButton) findViewById(R.id.btn_sign_in);
        btnSignOut = (Button) findViewById(R.id.btn_sign_out);
        btnSignIn.setOnClickListener(this);
        btnSignOut.setOnClickListener(this);
//        mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .addConnectionCallbacks(this)
//                .addOnConnectionFailedListener(this)
//                .addApi(Plus.API)
//                .addScope(Plus.SCOPE_PLUS_LOGIN)
//                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .addScope(Plus.SCOPE_PLUS_PROFILE)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
            if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    //    check user for system
    private void checkUser() {
        if (mClient == null) {
            isUserLoggedIn = false;
            isFacebookSignIn = false;
            isGoogleSignIn = false;
            return;
        }else{
            isUserLoggedIn = true;
            isFacebookSignIn = false;
            isGoogleSignIn = false;
            new StoreData().execute();
        }
    }

    @Override
    public void onConnected(Bundle arg0) {
        mSignInClicked = false;
        Toast.makeText(this, "User is connected!", Toast.LENGTH_LONG).show();
        isGoogleSignIn = true;
        updateUI(isGoogleSignIn);
//        mGoogleApiClient.connect();

//        if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
//            Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
//            String personName = currentPerson.getDisplayName();
//            String personPhotoUrl = currentPerson.getImage().getUrl();
//            String personGooglePlusProfile = currentPerson.getUrl();
//            Log.e("Google", personName);
//        }


        // Get user's information
        getProfileInformation();

        // Update the UI after signin
//        updateUI(true);
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
        isGoogleSignIn = false;
        Log.e("MainActivity", "onConnectionSuspended");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_sign_in:
                // Signin button clicked
                signInWithGplus();
                break;

            case R.id.btn_sign_out:
                // Signout button clicked
                signOutFromGplus();
                break;
        }
    }

    /**
     * Sign-in into google
     */
    private void signInWithGplus() {
        if (!mGoogleApiClient.isConnecting()) {
            mSignInClicked = true;
            Log.e("MainActivity", "signInWithGplus");
            resolveSignInError();
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (!result.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this,
                    0).show();
            return;
        }

        if (!mIntentInProgress) {
            // Store the ConnectionResult for later usage
            mConnectionResult = result;

            if (mSignInClicked) {
                // The user has already clicked 'sign-in' so we attempt to
                // resolve all
                // errors until the user is signed in, or they cancel.
                resolveSignInError();
            }
        }

    }

    /**
     * Method to resolve any signin errors
     */
    private void resolveSignInError() {
        Log.e("MainActivity", mConnectionResult.hasResolution()+"  "+mConnectionResult);
        if (mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }

    /**
     * Fetching user's information name, email, profile pic
     */
    private void getProfileInformation() {
        try {

            if (isGoogleSignIn) {
                Person currentPerson = Plus.PeopleApi
                        .getCurrentPerson(mGoogleApiClient);
                Log.e("name : ", currentPerson.getDisplayName());
                final User user= new User();
                user.setmName(currentPerson.getDisplayName());
                user.setmGooglePlusId(currentPerson.getId());
                user.setmAge(String.valueOf(currentPerson.getAgeRange().getMin()));
                user.setmEmail(Plus.AccountApi.getAccountName(mGoogleApiClient));
                addUser(user);
//                personName = currentPerson.getDisplayName();
//                String personPhotoUrl = currentPerson.getImage().getUrl();
//                String personCoverPic = currentPerson.getCover().getCoverPhoto().getUrl();
//                String personGooglePlusProfile = currentPerson.getUrl();
//                email = Plus.AccountApi.getAccountName(mGoogleApiClient);
//                age = String.valueOf(currentPerson.getAgeRange().getMin());
//
//                Log.e(TAG, "Name: " + personName + ", plusProfile: "
//                        + personGooglePlusProfile + ", email: " + email
//                        + ", Image: " + personPhotoUrl);
//
//                personPhotoUrl = personPhotoUrl.substring(0,
//                        personPhotoUrl.length() - 2)
//                        + PROFILE_PIC_SIZE;

//                new LoadProfileImage().execute(personPhotoUrl);
//                new LoadCoverImage().execute(personCoverPic);

            } else {
                Toast.makeText(MainActivity.this,
                        "Person information is null", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {

        callbackManager.onActivityResult(requestCode, responseCode, intent);

        if (requestCode == RC_SIGN_IN) {
            if (responseCode != RESULT_OK) {
                mSignInClicked = false;
            }

            mIntentInProgress = false;

            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        }
    }

    /**
     * Sign-out from google
     */
    private void signOutFromGplus() {
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
            mGoogleApiClient.connect();
            isGoogleSignIn = false;
            updateUI(isGoogleSignIn);
        }
    }

    /**
     * Revoking access from google
     */
    private void revokeGplusAccess() {
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            Plus.AccountApi.revokeAccessAndDisconnect(mGoogleApiClient)
                    .setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status arg0) {
                            Log.e(TAG, "User access revoked!");
                            mGoogleApiClient.connect();
                            isGoogleSignIn = false;
                        }

                    });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        uiHelper.onResume();
        //initializeMap();
        sharedpreferences = getSharedPreferences(getResources().getString(R.string.id), Context.MODE_PRIVATE);
        prefs = sharedpreferences.getString(getResources().getString(R.string.beepeerID), "");
        if (!prefs.equals("")) {
//            startService(new Intent(MainActivity.this, BeepService.class));
            startActivity(intent);
        }
        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }

    @Override
    public void onPause() {
        super.onPause();
//        uiHelper.onPause();
        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
//        finish();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        uiHelper.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle savedState) {
        super.onSaveInstanceState(savedState);
//        uiHelper.onSaveInstanceState(savedState);
    }

    @Override
    public void onResult(People.LoadPeopleResult peopleData) {
        if (peopleData.getStatus().getStatusCode() == CommonStatusCodes.SUCCESS) {
            PersonBuffer personBuffer = peopleData.getPersonBuffer();
            try {
                int count = personBuffer.getCount();
                for (int i = 0; i < count; i++) {
                    Log.d(TAG, "Display name: " + personBuffer.get(i).getDisplayName());
                }
            } finally {
                personBuffer.close();
            }
        } else {
            Log.e(TAG, "Error requesting people data: " + peopleData.getStatus());
        }
    }

    private class LoadProfileImage extends AsyncTask<String, Void, Bitmap> {


        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
//            bmImage.setImageBitmap(result);
            drawable = new BitmapDrawable(getResources(), result);
        }
    }

    private class LoadCoverImage extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... urls) {
            Bitmap mIconProfile = null;
            String urldisplay = urls[0];
            try {
                InputStream inputStream = new java.net.URL(urldisplay).openStream();
                mIconProfile = BitmapFactory.decodeStream(inputStream);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }

            return mIconProfile;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
//            imageViewCover.setImageBitmap(bitmap);
            coverPic = new BitmapDrawable(getResources(), bitmap);
            isGoogleSignIn = true;
//            addUser(user);
//            new StoreData().execute();
        }
    }

    public void addUser(User user) {
        if (mClient == null) {
            Log.e("mClient", "mClient is null");
            return;
        }
        if (hasFBId(user)) {
            loginErrorMsg.setVisibility(View.VISIBLE);
            loginErrorMsg.setText("This Facebook account is already used!!!");
            return;
        }

        isFacebookSignIn = true;
        isGoogleSignIn = false;
        isUserLoggedIn = false;

        // Create a new item
        final User item = user;

        Log.e("reg data", item.getmName()+" "+item.getmFacebookId());

        // Insert the new item
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
//                Log.e("reg data", personName+" "+email+" "+phoneNumber+" "+age+" "+facebookId+" "+googlePlusId);
                mClient.getTable(User.class).insert(item, new TableOperationCallback<User>() {
                    public void onCompleted(User entity, Exception exception, ServiceFilterResponse response) {
                        if (exception == null) {
//                            startActivity(intent);
                        } else {
                            Log.e("insert error", "insert error");
//                            revokeGplusAccess();
                        }
                    }
                });
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                new StoreData().execute(item);
            }
        }.execute();
    }

    private boolean hasFBId(User user) {
        final Boolean[] aBoolean = new Boolean[1];
        aBoolean[0] = false;
        final User user1 = user;
        new AsyncTask<Void, Void, Void>(){

            @Override
            protected Void doInBackground(Void... params) {

                try {
                    MobileServiceList<User> fbResults = mUserTable.where().field("facebookId").eq(val(user1.getmFacebookId())).execute().get();
                    if(fbResults.size() > 1){
                        aBoolean[0] = true;
                    }
                    else {
                        aBoolean[0] = false;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

                return null;
            }

        }.execute();
        return aBoolean[0];
    }

    public class StoreData extends AsyncTask<User, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(User... params) {
            try {

//                String stringEmail, mail;
                if(isUserLoggedIn){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            email = etEmail.getText().toString();
                        }
                    });
                    result = mUserTable.where().field("email").eq(val(email)).execute().get();
                    results = result;
                }
                else if(isGoogleSignIn){
                    User user = params[0];
                    result = mUserTable.where().field("googlePlusId").eq(val(user.getmGooglePlusId())).execute().get();
                    results = result;
                }
                else if(isFacebookSignIn){
                    User user = params[0];
                    Log.e("isfacebook", "isfacebook");
                    result = mUserTable.where().field("facebookId").eq(val(user.getmFacebookId())).execute().get();
                    results = result;
                }


                if (result.isEmpty()) {
                    Log.e("Result is Null", result.toString());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loginErrorMsg.setVisibility(View.VISIBLE);
                            loginErrorMsg.setText("No User Found");
                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loginErrorMsg.setVisibility(View.GONE);
                        }
                    });
                    User newUsers = result.get(0);
                    id = newUsers.getmId();
                    email = newUsers.getmEmail();
                    facebookId = newUsers.getmFacebookId();
                    userName = newUsers.getmName();
                    googlePlusId = newUsers.getmGooglePlusId();
                    Log.e("Result", id);
//                        id = result.toString();
                    saveDataInSF(userName, id, email, facebookId, googlePlusId, newUsers.getmAge(),
                            newUsers.getmMsisdn(), newUsers.getmCountry(), "");
                    startApp();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return null;
        }

    }

    private void startApp() {
        startService(new Intent(MainActivity.this, BeepService.class));
        startActivity(intent);
    }

    private void updateUI(boolean isSignedIn) {
        if (isSignedIn) {
            btnSignIn.setVisibility(View.GONE);
            btnSignOut.setVisibility(View.VISIBLE);
//            btnRevokeAccess.setVisibility(View.VISIBLE);
//            llProfileLayout.setVisibility(View.VISIBLE);
//            btnStart.setVisibility(View.VISIBLE);
//            startActivity(new Intent(this, MainActivity.class));
        } else {
            btnSignIn.setVisibility(View.VISIBLE);
            btnSignOut.setVisibility(View.GONE);
//            btnRevokeAccess.setVisibility(View.GONE);
//            llProfileLayout.setVisibility(View.GONE);
//            btnStart.setVisibility(View.GONE);
        }
    }

    private void saveDataInSF(String userName, String id, String email, String facebookId,
                              String googlePlusId, String age, String phone, String location, String gender) {
        editor = sharedpreferences.edit();
        editor.clear();
        editor.commit();
        editor.putString(getResources().getString(R.string.mail), email);
        editor.putString(getResources().getString(R.string.beepeerID), id);
        editor.putString(getResources().getString(R.string.beepeerName), userName);
        editor.putString(getResources().getString(R.string.facebookId), facebookId);
        editor.putString(getResources().getString(R.string.googlePlusId), googlePlusId);
        editor.commit();

        profile.edit().putString("email", email).commit();
        profile.edit().putString(getResources().getString(R.string.beepeerID), id).commit();
        profile.edit().putString("name", userName).commit();
        profile.edit().putString("age", age).commit();
        profile.edit().putString("phone", phone).commit();
        profile.edit().putString("gender", gender).commit();
        profile.edit().putString("location", location).commit();

        Log.e("Maps Activity",id);
    }

    private class ProgressFilter implements ServiceFilter {

        @Override
        public ListenableFuture<ServiceFilterResponse> handleRequest(ServiceFilterRequest request, NextServiceFilterCallback nextServiceFilterCallback) {

            final SettableFuture<ServiceFilterResponse> resultFuture = SettableFuture.create();


            runOnUiThread(new Runnable() {

                @Override
                public void run() {
//                    if (mProgressBar != null) mProgressBar.setVisibility(ProgressBar.VISIBLE);
                    progressDialog = ProgressDialog.show(MainActivity.this, "Please wait", "Loading...", true);
                }
            });

            ListenableFuture<ServiceFilterResponse> future = nextServiceFilterCallback.onNext(request);

            Futures.addCallback(future, new FutureCallback<ServiceFilterResponse>() {
                @Override
                public void onFailure(Throwable e) {
                    resultFuture.setException(e);
                }

                @Override
                public void onSuccess(ServiceFilterResponse response) {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
//                            if (mProgressBar != null) mProgressBar.setVisibility(ProgressBar.GONE);
                            progressDialog.dismiss();
                        }
                    });

                    resultFuture.set(response);
                }
            });

            return resultFuture;
        }
    }

}