/*
 * Copyright (c) to srabon
 * IIT, University of Dhaka
 * kaziiit@gmail.com
 */

package com.beepeer.bigs.model;

public class Contact {

    public String name, phone;

    public Contact(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }
}
