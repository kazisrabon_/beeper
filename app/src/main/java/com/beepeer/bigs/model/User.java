/*
 * Copyright (c) to srabon
 * IIT, University of Dhaka
 * kaziiit@gmail.com
 */

package com.beepeer.bigs.model;

public class User {
    @com.google.gson.annotations.SerializedName("id")
    public String mId;

    @com.google.gson.annotations.SerializedName("facebookId")
    public String mFacebookId;

    @com.google.gson.annotations.SerializedName("googlePlusId")
    public String mGooglePlusId;

    @com.google.gson.annotations.SerializedName("language")
    public String mLanguage;

    @com.google.gson.annotations.SerializedName("country")
    public String mCountry;

    @com.google.gson.annotations.SerializedName("age")
    public String mAge;

    @com.google.gson.annotations.SerializedName("msisdn")
    public String mMsisdn;

    @com.google.gson.annotations.SerializedName("email")
    public String mEmail;

    @com.google.gson.annotations.SerializedName("name")
    public String mName;

    public User() {
    }

    @Override
    public String toString() {
        return getmId();
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getmFacebookId() {
        return mFacebookId;
    }

    public void setmFacebookId(String mFacebookId) {
        this.mFacebookId = mFacebookId;
    }

    public String getmGooglePlusId() {
        return mGooglePlusId;
    }

    public void setmGooglePlusId(String mGooglePlusId) {
        this.mGooglePlusId = mGooglePlusId;
    }

    public String getmLanguage() {
        return mLanguage;
    }

    public void setmLanguage(String mLanguage) {
        this.mLanguage = mLanguage;
    }

    public String getmCountry() {
        return mCountry;
    }

    public void setmCountry(String mCountry) {
        this.mCountry = mCountry;
    }

    public String getmAge() {
        return mAge;
    }

    public void setmAge(String mAge) {
        this.mAge = mAge;
    }

    public String getmMsisdn() {
        return mMsisdn;
    }

    public void setmMsisdn(String mMsisdn) {
        this.mMsisdn = mMsisdn;
    }

    public String getmEmail() {
        return mEmail;
    }

    public void setmEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof User && ((User) o).mId == mId;
    }
}
