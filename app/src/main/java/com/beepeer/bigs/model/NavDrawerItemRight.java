/*
 * Copyright (c) to srabon
 * IIT, University of Dhaka
 * kaziiit@gmail.com
 */

package com.beepeer.bigs.model;

public class NavDrawerItemRight {

    private int icon;
    public NavDrawerItemRight(){}

    public NavDrawerItemRight(int icon){
        this.icon = icon;
    }

    public int getIcon(){
        return this.icon;
    }

    public void setIcon(int icon){
        this.icon = icon;
    }

}
