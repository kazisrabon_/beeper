/*
 * Copyright (c) to srabon
 * IIT, University of Dhaka
 * kaziiit@gmail.com
 */

package com.beepeer.bigs.model;

public class Beep {
    @com.google.gson.annotations.SerializedName("id")
    public String mId;

    @com.google.gson.annotations.SerializedName("beeper")
    public String mBeeper;

    @com.google.gson.annotations.SerializedName("areaStatusId")
    public String mAreaStatusId;

    @com.google.gson.annotations.SerializedName("areaName")
    public String mAreaName;

    @com.google.gson.annotations.SerializedName("longitude")
    public String mLongitude;

    @com.google.gson.annotations.SerializedName("latitude")
    public String mLatitude;


    public Beep() {
    }

    public Beep(String mBeeper, String mAreaStatusId, String mAreaName, String mLongitude, String mLatitude) {
        this.mBeeper = mBeeper;
        this.mAreaStatusId = mAreaStatusId;
        this.mAreaName = mAreaName;
        this.mLongitude = mLongitude;
        this.mLatitude = mLatitude;
    }

    @Override
    public String toString() {
        return getmId();
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getmBeeper() {
        return mBeeper;
    }

    public void setmBeeper(String mBeeper) {
        this.mBeeper = mBeeper;
    }

    public String getmAreaStatusId() {
        return mAreaStatusId;
    }

    public void setmAreaStatusId(String mAreaStatusId) {
        this.mAreaStatusId = mAreaStatusId;
    }

    public String getmAreaName() {
        return mAreaName;
    }

    public void setmAreaName(String mAreaName) {
        this.mAreaName = mAreaName;
    }

    public String getmLongitude() {
        return mLongitude;
    }

    public void setmLongitude(String mLongitude) {
        this.mLongitude = mLongitude;
    }

    public String getmLatitude() {
        return mLatitude;
    }

    public void setmLatitude(String mLatitude) {
        this.mLatitude = mLatitude;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Beep && ((Beep) o).mId == mId;
    }
}
