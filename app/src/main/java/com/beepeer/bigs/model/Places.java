/*
 * Copyright (c) to srabon
 * IIT, University of Dhaka
 * kaziiit@gmail.com
 */

package com.beepeer.bigs.model;

public class Places {

    @com.google.gson.annotations.SerializedName("lat")
    private Double gLat;

    @com.google.gson.annotations.SerializedName("lng")
    private Double gLon;

    @com.google.gson.annotations.SerializedName("id")
    private String gID;

    @com.google.gson.annotations.SerializedName("vicinity")
    private String gVicinity;

    @com.google.gson.annotations.SerializedName("name")
    private String gName;

    public Double getgLat() {
        return gLat;
    }

    public void setgLat(Double gLat) {
        this.gLat = gLat;
    }

    public Double getgLon() {
        return gLon;
    }

    public void setgLon(Double gLon) {
        this.gLon = gLon;
    }

    public String getgID() {
        return gID;
    }

    public void setgID(String gID) {
        this.gID = gID;
    }

    public String getgVicinity() {
        return gVicinity;
    }

    public void setgVicinity(String gVicinity) {
        this.gVicinity = gVicinity;
    }

    public String getgName() {
        return gName;
    }

    public void setgName(String gName) {
        this.gName = gName;
    }
}
