/*
 * Copyright (c) to srabon
 * IIT, University of Dhaka
 * kaziiit@gmail.com
 */

package com.beepeer.bigs.model;

public class MapChat {

    @com.google.gson.annotations.SerializedName("id")
    public String mId;

    @com.google.gson.annotations.SerializedName("message")
    public String mMessage;

    @com.google.gson.annotations.SerializedName("latitude")
    public String mLatitude;

    @com.google.gson.annotations.SerializedName("longitude")
    public String mLongitude;

    @com.google.gson.annotations.SerializedName("areaName")
    public String mAreaName;

    @com.google.gson.annotations.SerializedName("beeper")
    public String mBeepeer;

    @com.google.gson.annotations.SerializedName("receiver")
    public String mReceiver;

    @com.google.gson.annotations.SerializedName("beepeerName")
    public String mBeepeerName;

    public String getmBeepeerName() {
        return mBeepeerName;
    }

    public void setmBeepeerName(String mBeepeerName) {
        this.mBeepeerName = mBeepeerName;
    }



    public MapChat() {
    }

    public String getmMessage() {
        return mMessage;
    }

    public void setmMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    public String getmLatitude() {
        return mLatitude;
    }

    public void setmLatitude(String mLatitude) {
        this.mLatitude = mLatitude;
    }

    public String getmLongitude() {
        return mLongitude;
    }

    public void setmLongitude(String mLongitude) {
        this.mLongitude = mLongitude;
    }

    public String getmAreaName() {
        return mAreaName;
    }

    public void setmAreaName(String mAreaName) {
        this.mAreaName = mAreaName;
    }

    public String getmBeepeer() {
        return mBeepeer;
    }

    public void setmBeepeer(String mBeepeer) {
        this.mBeepeer = mBeepeer;
    }

    public String getmReceiver() {
        return mReceiver;
    }

    public void setmReceiver(String mReceiver) {
        this.mReceiver = mReceiver;
    }

    public boolean equals(Object o) {
        return o instanceof MapChat && ((MapChat) o).mId == mId;
    }
}
