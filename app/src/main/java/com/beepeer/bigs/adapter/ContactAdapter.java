/*
 * Copyright (c) to srabon
 * IIT, University of Dhaka
 * kaziiit@gmail.com
 */

package com.beepeer.bigs.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.beepeer.bigs.R;
import com.beepeer.bigs.model.Contact;

import java.util.List;

public class ContactAdapter extends ArrayAdapter<Contact> {
    private LayoutInflater inflater;
    private Context context;
    private List<Contact> contacts;
    private TextView number, name;
    private ImageView call, message;

    public ContactAdapter(Context context, List<Contact> users) {
        super(context, 0, users);
        this.context = context;
        this.contacts = users;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final Contact contact = contacts.get(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (inflater == null) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.contacts, null);
            number = (TextView) convertView.findViewById(R.id.tvNumber);
            name = (TextView) convertView.findViewById(R.id.tvName);
            call = (ImageView) convertView.findViewById(R.id.call);
            message = (ImageView) convertView.findViewById(R.id.msg);
        }
        // Lookup view for data population

        // Populate the data into the template view using the data object
        Log.e("phone&Name", contact.name + " " + contact.phone);
        name.setText(contact.name);
        number.setText(contact.phone);
//        call.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent callIntent = new Intent(Intent.ACTION_CALL);
//                callIntent.setData(Uri.parse(contact.phone));
//                startActivity(callIntent);
//            }
//        });
        // Return the completed view to render on screen
        return convertView;
    }
}
