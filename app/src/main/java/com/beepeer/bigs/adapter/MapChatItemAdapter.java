/*
 * Copyright (c) to srabon
 * IIT, University of Dhaka
 * kaziiit@gmail.com
 */
package com.beepeer.bigs.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;

import com.beepeer.bigs.R;
import com.beepeer.bigs.model.MapChat;

public class MapChatItemAdapter extends ArrayAdapter<MapChat> {
    Context mContext;
    int mLayoutResourceId;
    public MapChatItemAdapter(Context context, int layoutResourceId) {
        super(context, layoutResourceId);

        mContext = context;
        mLayoutResourceId = layoutResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        final MapChat currentItem = getItem(position);

        if (row == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext.getApplicationContext());
            row = inflater.inflate(mLayoutResourceId, parent, false);
        }

        row.setTag(currentItem);
        final CheckBox checkBox = (CheckBox) row.findViewById(R.id.checkToDoItem);
        checkBox.setText(currentItem.getmMessage());
        checkBox.setChecked(false);
        checkBox.setEnabled(true);

        checkBox.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
//                if (checkBox.isChecked()) {
//                    checkBox.setEnabled(false);
//                    if (mContext instanceof MapsActivity) {
//                        MapsActivity activity = (MapsActivity) mContext;
//                        activity.checkItem(currentItem);
//                    }
//                }
            }
        });

        return row;
    }

}