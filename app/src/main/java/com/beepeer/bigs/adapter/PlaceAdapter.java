/*
 * Copyright (c) to srabon
 * IIT, University of Dhaka
 * kaziiit@gmail.com
 */

package com.beepeer.bigs.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.beepeer.bigs.R;
import com.beepeer.bigs.model.NearBy;

import java.util.ArrayList;

public class PlaceAdapter extends ArrayAdapter<NearBy> {
    public Context context;
    public int layoutResourceId;
    public ArrayList<NearBy> places;

    public PlaceAdapter(Context context, int layoutResourceId, ArrayList<NearBy> places) {
        super(context, layoutResourceId, places);
        this.layoutResourceId = layoutResourceId;
        this.places = places;
        this.context = context;
    }

    @Override
    public View getView(int rowIndex, View convertView, ViewGroup parent) {
        View row = convertView;
        row = null;
        if(row == null) {
            LayoutInflater layout = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layout.inflate(R.layout.httptestrow, null);
        }
        NearBy place = places.get(rowIndex);
        if(place != null) {
            TextView name = (TextView) row.findViewById(R.id.htttptestrow_name);
            TextView vicinity = (TextView) row.findViewById(R.id.httptestrow_vicinity);
            if(name != null) {
                name.setText(place.getName());
            }
            if(vicinity != null) {
                vicinity.setText(place.getVicinity());
            }
        }
        return row;
    }
}
